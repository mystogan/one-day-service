<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prinaja extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		date_default_timezone_set("Asia/Bangkok");
	}

	public function index(){
		$id_surat = $this->input->post('id_surat');
		$kode = $this->input->post('kode_surat');
		$nim = $this->input->post('nim');
		$surat = "surat".$kode;
		$day = date("d");
		$month = date("m");
		$year = date("Y");
		$datas = 1;
		$datas = $this->getProfil($nim);

		$dataAjuan = $this->Home_model->getAjuan($id_surat);
		$mhsdataAjuan = $this->Home_model->getmhsajuan($id_surat);
		if ($dataAjuan[0]['kode_surat'] > 7) {
			$surat = "surat".$dataAjuan[0]['kode_surat'].$dataAjuan[0]['kategori'];
		}
		$datass = $this->Home_model->getNomor($datas['kodefakultas']);
		$datafakultas = $this->getfakultas($datas['kodefakultas']);
		
		$dekan = $this->getdekan($datafakultas['dekan'],$datas['kodefakultas']);
		$wadek1 = $this->getdekan($datafakultas['wadek1'],$datas['kodefakultas']);
		$wadek2 = $this->getdekan($datafakultas['wadek2'],$datas['kodefakultas']);
		$wadek3 = $this->getdekan($datafakultas['wadek3'],$datas['kodefakultas']);
		$data = array(
				 'nim' => $datas['nim'] ,
				 'nama' => $datas['nama'] ,
				 'kodeunit' => $datas['kodeunit'] ,
				 'semestermhs' => $datas['semester'] ,
				 'alamat' => $datas['alamat'] ,
				 'kota' => $datas['kota'] ,
				 'prov' => $datas['prov'] ,
				 'tgllahir' => $datas['tgllahir'] ,
				 'tmplahir' => $datas['tmplahir'] ,
				 'prodi' => $datas['prodi'] ,
				 'ipk' => $datas['ipk'] ,
				 'kodefakultas' => $datas['kodefakultas'] ,
				 'fakultas' => $datafakultas['fakultas'] ,
				 'hari' => $day ,
				 'nobulan' => $month ,
				 'bulan' => $bulan ,
				 'nomor' => $datass[0]['nomor'] ,
				 'tahun' => $year,
				 'dekan' => $dekan,
				 'wadek1' => $wadek1,
				 'wadek2' => $wadek2,
				 'wadek3' => $wadek3,
				 'nodekan' => $nodekan,
				 'singkatan' => $datass[0]['singkatan'],
				 'pengajuan' => $dataAjuan,
				 'mhsdataAjuan' => $mhsdataAjuan
			);
			$dataUpdate = array(
					 'status' => '88'
				);
			$this->Home_model->hapus($id_surat,$dataUpdate);
			$this->load->view('print/'.$surat,$data);
	//	echo "print/$surat$data";
		
  }

  public function previewSurat(){
	$id_surat = $this->input->post('id_surat');
		$kode = $this->input->post('kode_surat');
		$nim = $this->input->post('nim');
		$surat = "surat".$kode;
		$day = date("d");
		$month = date("m");
		$year = date("Y");
		$datas = 1;
		$datas = $this->getProfil($nim);

		$dataAjuan = $this->Home_model->getAjuan($id_surat);
		$mhsdataAjuan = $this->Home_model->getmhsajuan($id_surat);
		if ($dataAjuan[0]['kode_surat'] > 7) {
			$surat = "surat".$dataAjuan[0]['kode_surat'].$dataAjuan[0]['kategori'];
		}
		$datass = $this->Home_model->getNomor($datas['kodefakultas']);
		$datafakultas = $this->getfakultas($datas['kodefakultas']);
		
		$dekan = $this->getdekan($datafakultas['dekan'],$datas['kodefakultas']);
		$wadek1 = $this->getdekan($datafakultas['wadek1'],$datas['kodefakultas']);
		$wadek2 = $this->getdekan($datafakultas['wadek2'],$datas['kodefakultas']);
		$wadek3 = $this->getdekan($datafakultas['wadek3'],$datas['kodefakultas']);
		$data = array(
				 'nim' => $datas['nim'] ,
				 'nama' => $datas['nama'] ,
				 'kodeunit' => $datas['kodeunit'] ,
				 'semestermhs' => $datas['semester'] ,
				 'alamat' => $datas['alamat'] ,
				 'kota' => $datas['kota'] ,
				 'prov' => $datas['prov'] ,
				 'tgllahir' => $datas['tgllahir'] ,
				 'tmplahir' => $datas['tmplahir'] ,
				 'prodi' => $datas['prodi'] ,
				 'ipk' => $datas['ipk'] ,
				 'kodefakultas' => $datas['kodefakultas'] ,
				 'fakultas' => $datafakultas['fakultas'] ,
				 'hari' => $day ,
				 'nobulan' => $month ,
				 'bulan' => $bulan ,
				 'nomor' => $datass[0]['nomor'] ,
				 'tahun' => $year,
				 'dekan' => $dekan,
				 'wadek1' => $wadek1,
				 'wadek2' => $wadek2,
				 'wadek3' => $wadek3,
				 'nodekan' => $nodekan,
				 'singkatan' => $datass[0]['singkatan'],
				 'pengajuan' => $dataAjuan,
				 'mhsdataAjuan' => $mhsdataAjuan
			);
			$dataUpdate = array(
					 'status' => '88'
				);
				$this->load->view('print/'.$surat,$data);
  }
  
  public function valid() {
      $id_surat = $this->input->post('id');
      $dataUpdate = array(
        'status' => '1'
      );
      $this->Home_model->hapus($id_surat,$dataUpdate);
  }


		public function getpangkat($nip){
			$this->load->model('Simpeg_model');
			$pangkat = $this->Simpeg_model->getpangkat($nip);
			return ($pangkat);
		}
		public function getfakultas($prodi = "Z"){
			// echo 'get profil mas ';
			$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password='ampelakademik!3'");
			//$result = pg_prepare($conn, "my_query", 'SELECT * FROM gate.sc_user WHERE username = $1 and password = $2
			$result = pg_prepare($conn, "my_query2", "select * from gate.ms_unit where kodeunit = $1");
			$result = pg_execute($conn, "my_query2", array($prodi));
			while ($row = pg_fetch_assoc($result)){
				$data = array(
						 'kodefakultas' => $row['kodeunit'] ,
						 'fakultas' => $row['namaunit'] ,
						 'dekan' => $row['ketua'] ,
						 'wadek1' => $row['pembantu1'] ,
						 'wadek2' => $row['pembantu2'] ,
						 'wadek3' => $row['pembantu3']
					);
			}
			return $data;
		}
		public function getdekan($nip,$kode){
			// echo 'get profil mas ';
			$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password='ampelakademik!3'");
			//$result = pg_prepare($conn, "my_query", 'SELECT * FROM gate.sc_user WHERE username = $1 and password = $2
			$result = pg_prepare($conn, "my_query3", "SELECT akademik.f_namalengkap(a.gelardepan,a.nama,a.gelarbelakang) AS hasil , b.namaunit , nip
																								FROM kepegawaian.ms_pegawai a , gate.ms_unit b
																								WHERE nip = $1
																								and b.kodeunit = $2;");
			$result = pg_execute($conn, "my_query3", array($nip,$kode));
			while ($row = pg_fetch_assoc($result)){
				$data = array(
						 'nama' => $row['hasil'] ,
						 'fakultas' => $row['namaunit'] ,
						 'nip' => $row['nip']
					);
			}
			return $data;
		}

	public function getProfil($nim){
		  // echo 'get profil mas '.$nim;
		$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password='ampelakademik!3'");
		//$result = pg_prepare($conn, "my_query", 'SELECT * FROM gate.sc_user WHERE username = $1 and password = $2
		$result = pg_prepare($conn, "my_query", "select a.nim,a.nama,a.semestermhs,a.kodeunit , a.alamat,a.tmplahir , a.tgllahir , a.ipk , b.namaunit ,b.kodeunitparent,
													c.namakota,d.namapropinsi
													from akademik.ms_mahasiswa a
													left join gate.ms_unit b on a.kodeunit = b.kodeunit
													left join akademik.ms_kota c on c.kodekota = a.kodekota
													left join akademik.ms_propinsi d on c.kodepropinsi = d.kodepropinsi
													where a.nim = $1");
		$result = pg_execute($conn, "my_query", array($nim));
		while ($row = pg_fetch_assoc($result)){
			$semestermhs = $row['semestermhs'];
			$alamat = $row['alamat'];
			$tgllahir = $row['tgllahir'];
			$tmplahir = $row['tmplahir'];
			$ipk = $row['ipk'];
			$data = array(
					 'nim' => $row['nim'] ,
					 'nama' => $row['nama'] ,
					 'kodeunit' => $row['kodeunit'] ,
					 'prodi' => $row['namaunit'] ,
					 'kodefakultas' => $row['kodeunitparent'] ,
					 'semester' => $semestermhs ,
					 'alamat' => $alamat ,
					 'kota' => $row['namakota'] ,
					 'prov' => $row['namapropinsi'] ,
					 'tgllahir' => $tgllahir ,
					 'tmplahir' => $tmplahir ,
					 'ipk' => $ipk
				);
		}
		return $data;
	}

}
