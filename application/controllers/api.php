<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// use Restserver\Libraries\REST_Controller;
class Api extends CI_Controller {
  
	public function __construct() {
    header('Content-type: application/json');
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Allow-Methods: GET, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Rest_api');
  }
  
  public function fak($kodefak) {
    $fak = $this->Rest_api->getFak($kodefak);
    echo json_encode($fak);
  }
  
  public function matkul($nim) {
    $matkul = $this->Rest_api->getAllMatkul($nim);
    echo json_encode($matkul);
  }
  
  public function matkulcek($nim) {
    $matkulcek = $this->Rest_api->getAllMatkulCek($nim);
    echo json_encode($matkulcek);
  }
  
  public function allset($kodefak, $periode) {
    $allset = $this->Rest_api->getAllSet($kodefak, $periode);
    echo json_encode($allset);
  }
  
  public function addnilai($kodemk, $nmmk, $nipdosen, $dos, $kodeunit, $periode, $jwb, $jwbtext, $nim) {
    $namamk = urldecode($nmmk);
    $dosen = urldecode($dos);
    $jawab = urldecode($jwb);
    $jawabtext = urldecode($jwbtext);
    $addnilai = $this->Rest_api->addNilai($kodemk, $namamk, $nipdosen, $dosen, $kodeunit, $periode, $jawab, $jawabtext, $nim);
    echo json_encode(['suskes']);
  }
  
  public function kodeunit($kodefak) {
    $kodeunit = $this->Rest_api->getAllKodeunit($kodefak);
    echo json_encode($kodeunit);
  }
  
  public function dosen($kodeunit, $periode) {
    $dosen = $this->Rest_api->getAllDosen($kodeunit, $periode);
    echo json_encode($dosen);
  }
  
  public function allfak($kodefak, $periode) {
    $allfak = $this->Rest_api->getAllFak($kodefak, $periode);
    echo json_encode($allfak);
  }
  
  public function nilai($nip, $kodeunit, $periode) {
    $nilai = $this->Rest_api->getAllNilai($nip, $kodeunit, $periode);
    echo json_encode($nilai);
  }
  
  public function set($kodefak, $periode) {
    $allset = $this->Rest_api->getAllSetting($kodefak, $periode);
    echo json_encode($allset);
  }
  
  public function setting($periode, $kodefak, $sdate, $edate) {
    $setting = $this->Rest_api->addSetting($periode, $kodefak, $sdate, $edate);
    echo json_encode(['suskes']);
  }
  
  public function upset($id, $periode, $sdate, $edate) {
    $upset = $this->Rest_api->editSetting($id, $periode, $sdate, $edate);
    echo json_encode(['suskes']);
  }
  
  public function delset($id) {
    $delset = $this->Rest_api->delSetting($id);
    echo json_encode(['suskes']);
  }
}