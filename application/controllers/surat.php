<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Surat extends CI_Controller {
	function __construct(){
		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->model('Siakad_model');
		$this->load->helper(array('form','url','file','download', 'cookie'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		SESSION_START();
	}

	public function index(){

	}

	public function kaprod($nim) {
		return $this->Home_model->getKaprod($nim);
	}

	public function getemail($nip) {
		return $this->Home_model->getEmail($nip);
	}

	public function jenis_surat_1(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		$get['data_siakad'] = $this->Siakad_model->getprofil($get['data']['nim']);
		 // print_r ($get['data_siakad']);
		$this->load->view('print/getsurat',$get);
	}
	public function add_surat_1(){
		date_default_timezone_set('Asia/Jakarta');
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$surat1_alamat = $this->input->post('surat1_alamat');
		$surat1_keperluan = $this->input->post('surat1_keperluan');
		$tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		$kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		$data_siakad['tmplahir'] = $kota_tmpt_mhs;
		$data_siakad['tgllahir'] = $tgl_lahir_mhs;
		$data_siakad['alamat'] = ucwords(strtolower($surat1_alamat));

		$this->Siakad_model->update_data_siakad($nim,$data_siakad);

		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 1;
		$data['nama'] = $nama;
		$data['mhs_alamat'] = $surat1_alamat;
		$data['srt1_keperluan'] = $surat1_keperluan;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		$data['status'] = 1;

		$cek = $this->Home_model->cekantri2($nim,1);
		if (count($cek) >= 2) {
			echo "<script>alert ('Maaf Surat Masih di Proses silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else if($cek[0]['status'] == 1){
			echo "<script>alert ('Maaf Saurat masih antri dan belum diprint, silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}


	}
	public function update_surat_1($cek_update_staff){
		$id_surat = $this->input->post('id');
		$mhs_alamat = $this->input->post('mhs_alamat');
		$dekanat = $this->input->post('dekanat');
		$surat1_keperluan = $this->input->post('surat1_keperluan');

		if($cek_update_staff != 1){
			$data['mhs_alamat'] = $mhs_alamat;
			$data['srt1_keperluan'] = $surat1_keperluan;
		}else {
			$data['mhs_alamat'] = $mhs_alamat;
			$data['srt1_keperluan'] = $surat1_keperluan;
		}
		$data['dekanat'] = $dekanat;

		$tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		$kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		$nim = $this->input->post('nim_mhs');
		$data_siakad['tmplahir'] = $kota_tmpt_mhs;
		$data_siakad['tgllahir'] = $tgl_lahir_mhs;
		$data_siakad['alamat'] = $data['mhs_alamat'];
		$this->Siakad_model->update_data_siakad($nim,$data_siakad);

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}
	public function jenis_surat_2(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		$get['data_siakad'] = $this->Siakad_model->getprofil($get['data']['nim']);
		$this->load->view('print/getsurat',$get);
		// echo $nim." ".$id." ".$id_surat;
	}
	public function add_surat_2(){
		date_default_timezone_set("Asia/Jakarta");
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$surat1_alamat = $this->input->post('surat1_alamat');
		$surat1_keperluan = $this->input->post('surat1_keperluan');

		$tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		$kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		$data_siakad['tmplahir'] = $kota_tmpt_mhs;
		$data_siakad['tgllahir'] = $tgl_lahir_mhs;
		$data_siakad['alamat'] = ucwords(strtolower($surat1_alamat));

		$this->Siakad_model->update_data_siakad($nim,$data_siakad);

		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 2;
		$data['nama'] = $nama;
		$data['mhs_alamat'] = $surat1_alamat;
		$data['srt1_keperluan'] = $surat1_keperluan;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		$data['status'] = 1;
		// print_r ($data);

		$cek = $this->Home_model->cekantri2($nim,2);
		if (count($cek) >= 2) {
			echo "<script>alert ('Maaf Surat Masih di Proses silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else if($cek[0]['status'] == 1){
			echo "<script>alert ('Maaf Saurat masih antri dan belum diprint, silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}


	}
	public function update_surat_2($cek_update_staff){
		$id_surat = $this->input->post('id');
		$mhs_alamat = $this->input->post('mhs_alamat');
		$dekanat = $this->input->post('dekanat');

		if($cek_update_staff != 1){
			$data['mhs_alamat'] = $mhs_alamat;
		}else {
			$data['mhs_alamat'] = $mhs_alamat;
		}
		$data['dekanat'] = $dekanat;

		$tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		$kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		$nim = $this->input->post('nim_mhs');
		$data_siakad['tmplahir'] = $kota_tmpt_mhs;
		$data_siakad['tgllahir'] = $tgl_lahir_mhs;
		$data_siakad['alamat'] = $data['mhs_alamat'];
		$this->Siakad_model->update_data_siakad($nim,$data_siakad);

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}
	public function jenis_surat_3(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		 // print_r ($get);
		$this->load->view('print/getsurat',$get);
		// echo $nim." ".$id." ".$id_surat;
	}
	public function add_surat_3(){
		date_default_timezone_set("Asia/Jakarta");
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$fakultas = $this->session->userdata('fakultas');
		$srt3_instansi = $this->input->post('srt3_instansi');
		$srt3_alamat = $this->input->post('srt3_alamat');
		$srt3_kabupaten = $this->input->post('srt3_kabupaten');
		$srt3_judul = $this->input->post('srt3_judul');
		$srt3_tgl = $this->input->post('srt3_tgl');
		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 3;
		$data['nama'] = $nama;
		$data['ket_instansi'] = $srt3_instansi;
		$data['alamat_instansi'] = $srt3_alamat;
		$data['kab_penelitian'] = $srt3_kabupaten;
		$data['judul_penelitian'] = $srt3_judul;
		$data['tgl_penelitian'] = $srt3_tgl;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		if ($fakultas == 'Fak. Sains dan Teknologi'){
			$data['status'] = 88;
			
			//ini kodingan phpmailer jangan dihapus
			$lempar['nim'] = $nim;
			$nip = $this->kaprod($nim);
			$lempar['email'] = $this->getemail($nip);
			$lempar['user'] = $nama;
			$this->load->view('mail/index.php', $lempar);

		} elseif($fakultas == 'Fak. Dakwah dan Komunikasi'){ 
			$data['status'] = 88;
			$lempar['nim'] = $nim;
			$nip = $this->Home_model->getsekprod($nim);
			$lempar['email'] = $this->getemail($nip);
			$lempar['user'] = $nama;
			$this->load->view('mail/dakwah.php', $lempar);
		} elseif($fakultas == 'Fak. Adab dan Humaniora'){ 
			$data['status'] = 88;
			$lempar['nim'] = $nim;
			$nip = $this->Home_model->getsekprod($nim);
			$lempar['email'] = $this->getemail($nip);
			$lempar['user'] = $nama;
			$this->load->view('mail/adab.php', $lempar);
		} else {
			$data['status'] = 1;
		}
		// print_r ($data);

		$cek = $this->Home_model->cekantri2($nim,3);
		if (count($cek) >= 2) {
			echo "<script>alert ('Maaf Surat Masih di Proses silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else if($cek[0]['status'] == 1){
			echo "<script>alert ('Maaf Saurat masih antri dan belum diprint, silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}


	}
	public function update_surat_3($cek_update_staff){
		$id_surat = $this->input->post('id');
		$srt3_instansi = $this->input->post('srt3_instansi');
		$srt3_alamat = $this->input->post('srt3_alamat');
		$srt3_kabupaten = $this->input->post('srt3_kabupaten');
		$srt3_judul = $this->input->post('srt3_judul');
		$srt3_tgl = $this->input->post('srt3_tgl');
		$dekanat = $this->input->post('dekanat');

		if($cek_update_staff != 1){
			$data['ket_instansi'] = $srt3_instansi;
			$data['alamat_instansi'] = $srt3_alamat;
			$data['kab_penelitian'] = $srt3_kabupaten;
			$data['judul_penelitian'] = $srt3_judul;
			$data['tgl_penelitian'] = $srt3_tgl;
		}else {
			$data['ket_instansi'] = $srt3_instansi;
			$data['alamat_instansi'] = $srt3_alamat;
			$data['kab_penelitian'] = $srt3_kabupaten;
			$data['judul_penelitian'] = $srt3_judul;
			$data['tgl_penelitian'] = $srt3_tgl;
		}


		$data['dekanat'] = $dekanat;

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}
	public function jenis_surat_4(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		 // print_r ($get);
		$this->load->view('print/getsurat',$get);
		// echo $nim." ".$id." ".$id_surat;
	}
	public function add_surat_4(){
		date_default_timezone_set("Asia/Jakarta");
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$fakultas = $this->session->userdata('fakultas');
		$srt3_instansi = $this->input->post('srt3_instansi');
		$srt3_alamat = $this->input->post('srt3_alamat');
		$srt3_kabupaten = $this->input->post('srt3_kabupaten');
		$srt3_judul = $this->input->post('srt3_judul');
		$srt3_tgl = $this->input->post('srt3_tgl');
		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 4;
		$data['nama'] = $nama;
		$data['ket_instansi'] = $srt3_instansi;
		$data['alamat_instansi'] = $srt3_alamat;
		$data['kab_penelitian'] = $srt3_kabupaten;
		$data['judul_penelitian'] = $srt3_judul;
		$data['tgl_penelitian'] = $srt3_tgl;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		if ($fakultas == 'Fak. Sains dan Teknologi'){
			$data['status'] = 88;
			
			//ini kodingan phpmailer jangan dihapus
			$lempar['nim'] = $nim;
			$nip = $this->kaprod($nim);
			$lempar['email'] = $this->getemail($nip);
			$lempar['user'] = $nama;
			$this->load->view('mail/index.php', $lempar);

		} elseif($fakultas == 'Fak. Dakwah dan Komunikasi'){ 
			$data['status'] = 88;
			$lempar['nim'] = $nim;
			$nip = $this->Home_model->getsekprod($nim);
			$lempar['email'] = $this->getemail($nip);
			$lempar['user'] = $nama;
			$this->load->view('mail/dakwah.php', $lempar);
		} elseif($fakultas == 'Fak. Adab dan Humaniora'){ 
			$data['status'] = 88;
			$lempar['nim'] = $nim;
			$nip = $this->Home_model->getsekprod($nim);
			$lempar['email'] = $this->getemail($nip);
			$lempar['user'] = $nama;
			$this->load->view('mail/adab.php', $lempar);
		} else {
			$data['status'] = 1;
		}
		// print_r ($data);

		$cek = $this->Home_model->cekantri2($nim,4);
		if (count($cek) >= 2) {
			echo "<script>alert ('Maaf Surat Masih di Proses silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else if($cek[0]['status'] == 1){
			echo "<script>alert ('Maaf Saurat masih antri dan belum diprint, silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}


	}
	public function update_surat_4($cek_update_staff){
		$id_surat = $this->input->post('id');
		$srt3_instansi = $this->input->post('srt3_instansi');
		$srt3_alamat = $this->input->post('srt3_alamat');
		$srt3_kabupaten = $this->input->post('srt3_kabupaten');
		$srt3_judul = $this->input->post('srt3_judul');
		$srt3_tgl = $this->input->post('srt3_tgl');
		$dekanat = $this->input->post('dekanat');

		if($cek_update_staff != 1){
			$data['ket_instansi'] = $srt3_instansi;
			// $data['alamat_instansi'] = ucwords(strtolower($srt3_alamat));
			$data['alamat_instansi'] = $srt3_alamat;
			$data['kab_penelitian'] = $srt3_kabupaten;
			$data['judul_penelitian'] = $srt3_judul;
			$data['tgl_penelitian'] = $srt3_tgl;
		}else {
			$data['ket_instansi'] = $srt3_instansi;
			$data['alamat_instansi'] = $srt3_alamat;
			$data['kab_penelitian'] = $srt3_kabupaten;
			$data['judul_penelitian'] = $srt3_judul;
			$data['tgl_penelitian'] = $srt3_tgl;
		}

		$data['dekanat'] = $dekanat;

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}
	public function jenis_surat_5(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		$get['data_siakad'] = $this->Siakad_model->getprofil($get['data']['nim']);
		$this->load->view('print/getsurat',$get);
		// echo $nim." ".$id." ".$id_surat;
	}
	public function add_surat_5(){
		date_default_timezone_set("Asia/Jakarta");
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$fakultas = $this->session->userdata('fakultas');
		$surat5_alamat = $this->input->post('surat5_alamat');
		$mhs_alasan = $this->input->post('mhs_alasan');

		$tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		$kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		$data_siakad['tmplahir'] = $kota_tmpt_mhs;
		$data_siakad['tgllahir'] = $tgl_lahir_mhs;
		$data_siakad['alamat'] = ucwords(strtolower($surat5_alamat));

		$this->Siakad_model->update_data_siakad($nim,$data_siakad);

		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 5;
		$data['nama'] = $nama;
		$data['mhs_alamat'] = $surat5_alamat;
		$data['mhs_alasan'] = $mhs_alasan;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		if ($fakultas == 'Fak. Sains dan Teknologi'){
			$data['status'] = 88;
			
			//ini kodingan phpmailer jangan dihapus
			$lempar['nim'] = $nim;
			$nip = $this->kaprod($nim);
			$lempar['email'] = $this->getemail($nip);
			$lempar['user'] = $nama;
			$this->load->view('mail/index.php', $lempar);

		} else {
			$data['status'] = 1;
		}

		$cek = $this->Home_model->cekantri($nim,5);
		if($cek == 1){
			echo "<script>alert ('Maaf Anda Sudah Pesan !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$cek = $this->Home_model->cekcuti($nim);
			if ( count($cek) >= 2) {
					echo "<script>alert ('Anda Sudah Mengajukan Cuti 2 Kali, Silahkan ke bagian akademik untuk konfirmasi.');window.location.href = '".base_url()."home';</script>";
			}else {
				$this->Home_model->savePengajuan($data);
				header("location:".base_url()."home");
			}
		}


	}
	public function update_surat_5(){
		$id_surat = $this->input->post('id');
		$mhs_alamat = $this->input->post('mhs_alamat');
		$mhs_alasan = $this->input->post('mhs_alasan');
		$dekanat = $this->input->post('dekanat');
		$data['mhs_alamat'] = $mhs_alamat;
		$data['mhs_alasan'] = $mhs_alasan;
		$data['dekanat'] = $dekanat;

		$tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		$kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		$nim = $this->input->post('nim_mhs');
		$data_siakad['tmplahir'] = $kota_tmpt_mhs;
		$data_siakad['tgllahir'] = $tgl_lahir_mhs;
		$data_siakad['alamat'] = $data['mhs_alamat'];
		$this->Siakad_model->update_data_siakad($nim,$data_siakad);

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}
	public function jenis_surat_6(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		$get['data_siakad'] = $this->Siakad_model->getprofil($get['data']['nim']);
		$this->load->view('print/getsurat',$get);
		// echo $nim." ".$id." ".$id_surat;
	}
	public function add_surat_6(){
		date_default_timezone_set("Asia/Jakarta");
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$srt6_nama_ortu = $this->input->post('srt6_nama_ortu');
		$srt6_nip = $this->input->post('srt6_nip');
		$srt6_pangkat = $this->input->post('srt6_pangkat');
		$srt6_instansi = $this->input->post('srt6_instansi');
		$filep = $_FILES["filepen"]["name"];
		$file_kk = $_FILES["file_kk"]["name"];
		$filep_ktm = $_FILES["file_ktm"]["name"];

		// print_r ($filep_ktm);
		$namafile = $this->upload($filep,"filepen");
		$namafile_kk = $this->upload($file_kk,"file_kk");
		$namafile_ktm = $this->upload($filep_ktm,"file_ktm");

		$tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		$kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		$data_siakad['tmplahir'] = $kota_tmpt_mhs;
		$data_siakad['tgllahir'] = $tgl_lahir_mhs;
		$data_siakad['alamat'] = ucwords(strtolower($surat5_alamat));

		// $this->Siakad_model->update_data_siakad($nim,$data_siakad);

		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 6;
		$data['nama'] = $nama;
		$data['srt6_nama_ortu'] = $srt6_nama_ortu;
		$data['srt6_nip'] = $srt6_nip;
		$data['srt6_pangkat'] = ($srt6_pangkat);
		$data['srt6_instansi'] = $srt6_instansi;
		$data['file'] = $namafile;
		$data['file_kk'] = $namafile_kk;
		$data['file_ktm'] = $namafile_ktm;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		$data['status'] = 1;

		$cek = $this->Home_model->cekantri2($nim,6);
		if (count($cek) >= 2) {
			echo "<script>alert ('Maaf Surat Masih di Proses silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else if($cek[0]['status'] == 1){
			echo "<script>alert ('Maaf Saurat masih antri dan belum diprint, silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}
	}
	public function update_surat_6(){
		$id_surat = $this->input->post('id');
		$srt6_nama_ortu = $this->input->post('srt6_nama_ortu');
		$srt6_nip = $this->input->post('srt6_nip');
		$srt6_pangkat = $this->input->post('srt6_pangkat');
		$srt6_instansi = $this->input->post('srt6_instansi');
		$dekanat = $this->input->post('dekanat');
		$data['srt6_nama_ortu'] = $srt6_nama_ortu;
		$data['srt6_nip'] = $srt6_nip;
		$data['srt6_pangkat'] = ($srt6_pangkat);
		$data['srt6_instansi'] = $srt6_instansi;
		$data['dekanat'] = $dekanat;

		$tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		$kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		$nim = $this->input->post('nim_mhs');
		$data_siakad['tmplahir'] = $kota_tmpt_mhs;
		$data_siakad['tgllahir'] = $tgl_lahir_mhs;
		$this->Siakad_model->update_data_siakad($nim,$data_siakad);

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}

		function upload($simpan,$nama){
			$sub = substr($simpan,-5);
			$string = uniqid().$sub;

			$target_dir = "assets/foto/";
			$target_file = $target_dir . $string;
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

			if(isset($_POST["submit"])) {
				$check = getimagesize($_FILES[$nama]["tmp_name"]);
				if($check !== false) {
					$uploadOk = 1;
				} else {
					$uploadOk = 0;
				}
			}

			if($imageFileType != "pdf" && $imageFileType != "docx" && $imageFileType != "PDF" && $imageFileType != "DOCX"
		 			&& $imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" ) {
				echo "<script> alert ('Format Harus JPG, PNG, PDF atau docx !')</script>";
				$uploadOk = 0;
			}
			if ($uploadOk == 0) {
				echo "<script> alert ('Error Waktu Uploads !')</script>";
			} else {
				if (move_uploaded_file($_FILES[$nama]["tmp_name"], $target_file)) {
					$op = 1;
				} else {
					echo "<script> alert ('Error Waktu Upload,Silahkan upload ulang !')</script>";
					// echo "error ".$_FILES["file"]["error"];
					// echo "target ".$target_file;
					echo $nama;
				}

			}
			if ($op == 1) {
				return $string;
			}else {
				return "";
			}
	}
	public function jenis_surat_7(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		$get["provinsi"] = $this->Home_model->getProv();
		$this->load->view('print/getsurat',$get);
	}
	public function add_surat_7(){
		date_default_timezone_set("Asia/Jakarta");
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$fakultas = $this->session->userdata('fakultas');
		$fakultas = $this->session->userdata('fakultas');
		$instansi = $this->input->post('instansi');
		$alamat = $this->input->post('alamat');
		$mulai_tgl = $this->input->post('mulai_tgl');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$kabupaten = $this->input->post('kabupaten');
		$srt1_keperluan = $this->input->post('srt1_keperluan');
		$jenis_magang = $this->input->post('jenis_magang');
		$srt7_matkul = $this->input->post('srt7_matkul');
		$srt3_judul = $this->input->post('srt3_judul');
		$srt13_tujuan = $this->input->post('srt13_tujuan');
		$srt16_data = $this->input->post('srt16_data');
		$time_mulai = $this->input->post('time_mulai');
		$kode_surat = $this->input->post('kode_surat');
		$now = date("Y-m-d H:i:s");
		if ($fakultas == 'Fak. Sains dan Teknologi'){
			$data['status'] = 88;
			
			//ini kodingan phpmailer jangan dihapus
			$lempar['nim'] = $nim;
			$nip = $this->kaprod($nim);
			$lempar['email'] = $this->getemail($nip);
			$lempar['user'] = $nama;
			$this->load->view('mail/index.php', $lempar);

		} elseif($fakultas == 'Fak. Dakwah dan Komunikasi'){ 
			$data['status'] = 88;
			$lempar['nim'] = $nim;
			$nip = $this->Home_model->getsekprod($nim);
			$lempar['email'] = $this->getemail($nip);
			$lempar['user'] = $nama;
			$this->load->view('mail/dakwah.php', $lempar);
		} elseif($fakultas == 'Fak. Adab dan Humaniora'){ 
			$data['status'] = 88;
			$lempar['nim'] = $nim;
			$nip = $this->Home_model->getsekprod($nim);
			$lempar['email'] = $this->getemail($nip);
			$lempar['user'] = $nama;
			$this->load->view('mail/adab.php', $lempar);
		} else {
			$data['status'] = 1;
		}
		$data = array(
				 'nim' => $nim ,
				 'kode_surat' => $kode_surat ,
				 'tgl_penelitian' => $mulai_tgl." ".$time_mulai ,
				 'tgl_akhir' => $tgl_akhir ,
				 'ket_instansi' =>  $instansi,
				 'kab_penelitian' => $kabupaten,
				 'alamat_instansi' => $alamat,
				 'nama' => $nama ,
				 'prodi' => $prodi ,
				 'srt7_matkul' => $srt7_matkul,
				 'srt1_keperluan' => $srt1_keperluan,
				 'judul_penelitian' => $srt3_judul ,
				 'laboratorium' => $srt13_tujuan ,
				 'str15_data' => $srt16_data ,
				 'tgl_submit' => $now ,
				 'kategori' => $jenis_magang ,
				 'status' => $data['status']
			);
		$cek = $this->Home_model->cekantri($nim,$kode_surat);
		if($cek == 1){
				echo "<script>alert ('Maaf Anda Sudah Pesan !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$id = $this->Home_model->savePengajuanMagang($data);
			if($id == null){
				echo "<script>alert ('Maaf Belum Tersimpan Silahkan Ulangi Lagi !!!');window.location.href = '".base_url()."home';</script>";
			}else {
				$nim_mhs_akeh = $this->input->post('nim_mhs_akeh');
				$nama_mhs_akeh = $this->input->post('nama_mhs_akeh');
				$prodi_mhs_akeh = $this->input->post('prodi_mhs_akeh');
				$semester_mhs_akeh = $this->input->post('semester_mhs_akeh');
				for($i=0;$i<count($nim_mhs_akeh);$i++){
					$datas['id_pengajuan'] = $id;
					$datas['nim_mhs'] = $nim_mhs_akeh[$i];
					$datas['nama_mhs'] = $nama_mhs_akeh[$i];
					$datas['prodi_mhs'] = $prodi_mhs_akeh[$i];
					$datas['semester_mhs'] = $semester_mhs_akeh[$i];
					if ($nim_mhs_akeh[$i] != "") {
						$this->Home_model->saveMhsPengajuan($datas);
					}
				}

			}
			// print_r ($datas);
			header("location:".base_url()."home");
		}
	}
	public function update_surat_7($cek_update_staff){

		$id_surat = $this->input->post('id');
		$instansi = $this->input->post('instansi');
		$alamat = $this->input->post('alamat');
		$mulai_tgl = $this->input->post('mulai_tgl');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$srt7_matkul = $this->input->post('srt7_matkul');
		$dekanat = $this->input->post('dekanat');
		$time_mulai = $this->input->post('time_mulai');
		$kab_penelitian = $this->input->post('kab_penelitian');
		$srt3_judul = $this->input->post('srt3_judul');
		$srt13_tujuan = $this->input->post('srt13_tujuan');
		$srt16_data = $this->input->post('srt16_data');

		$data['tgl_penelitian'] = $mulai_tgl." ".$time_mulai;
		$data['tgl_akhir'] = $tgl_akhir;
		$data['dekanat'] = $dekanat;

		if($cek_update_staff != 1){
			$data['ket_instansi'] = $instansi;
			$data['alamat_instansi'] =  $alamat;
			$data['kab_penelitian'] = $kab_penelitian;
			$data['srt7_matkul'] = $srt7_matkul;
			$data['judul_penelitian'] = $srt3_judul;
			$data['laboratorium'] = $srt13_tujuan;
			$data['str15_data'] = $srt16_data;
		}else {
			$data['ket_instansi'] = $instansi;
			$data['alamat_instansi'] =  $alamat;
			$data['kab_penelitian'] = $kab_penelitian;
			$data['srt7_matkul'] = $srt7_matkul;
			$data['judul_penelitian'] = $srt3_judul;
			$data['laboratorium'] = $srt13_tujuan;
			$data['str15_data'] = $srt16_data;
		}



		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}


	// surat rekomendasi
	public function jenis_surat_11(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		$get['data_siakad'] = $this->Siakad_model->getprofil($get['data']['nim']);
		$this->load->view('print/getsurat',$get);
		// echo $nim." ".$id." ".$id_surat;
	}
	public function add_surat_11(){
		date_default_timezone_set("Asia/Jakarta");
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$surat1_alamat = $this->input->post('surat1_alamat');
		$surat1_keperluan = $this->input->post('surat1_keperluan');

		$tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		$kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		$data_siakad['tmplahir'] = $kota_tmpt_mhs;
		$data_siakad['tgllahir'] = $tgl_lahir_mhs;
		$data_siakad['alamat'] = ucwords(strtolower($surat1_alamat));

		$this->Siakad_model->update_data_siakad($nim,$data_siakad);

		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 11;
		$data['nama'] = $nama;
		$data['mhs_alamat'] = $surat1_alamat;
		$data['srt1_keperluan'] = $surat1_keperluan;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		$data['status'] = 1;
		// print_r ($data);

		$cek = $this->Home_model->cekantri2($nim,11);
		if (count($cek) >= 2) {
			echo "<script>alert ('Maaf Surat Masih di Proses silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else if($cek[0]['status'] == 1){
			echo "<script>alert ('Maaf Saurat masih antri dan belum diprint, silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}


	}
	public function update_surat_11($cek_update_staff){
		$id_surat = $this->input->post('id');
		$mhs_alamat = $this->input->post('mhs_alamat');
		$dekanat = $this->input->post('dekanat');
		$surat1_keperluan = $this->input->post('surat1_keperluan');

		if($cek_update_staff != 1){
			$data['mhs_alamat'] = $mhs_alamat;
			$data['srt1_keperluan'] = $surat1_keperluan;
		}else {
			$data['mhs_alamat'] = $mhs_alamat;
			$data['srt1_keperluan'] = $surat1_keperluan;
		}
		$data['dekanat'] = $dekanat;

		$tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		$kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		$nim = $this->input->post('nim_mhs');
		$data_siakad['tmplahir'] = $kota_tmpt_mhs;
		$data_siakad['tgllahir'] = $tgl_lahir_mhs;
		$data_siakad['alamat'] = $data['mhs_alamat'];
		$this->Siakad_model->update_data_siakad($nim,$data_siakad);

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}

	public function jenis_surat_12(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		$get['data_siakad'] = $this->Siakad_model->getprofil($get['data']['nim']);
		$this->load->view('print/getsurat',$get);
		// echo $nim." ".$id." ".$id_surat;
	}
	public function add_surat_12(){
		date_default_timezone_set("Asia/Jakarta");
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$surat1_alamat = $this->input->post('surat1_alamat');
		$surat1_keperluan = $this->input->post('surat1_keperluan');

		$tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		$kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		$data_siakad['tmplahir'] = $kota_tmpt_mhs;
		$data_siakad['tgllahir'] = $tgl_lahir_mhs;
		$data_siakad['alamat'] = ucwords(strtolower($surat1_alamat));

		$this->Siakad_model->update_data_siakad($nim,$data_siakad);

		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 12;
		$data['nama'] = $nama;
		$data['mhs_alamat'] = $surat1_alamat;
		$data['srt1_keperluan'] = $surat1_keperluan;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		$data['status'] = 1;
		// print_r ($data);

		$cek = $this->Home_model->cekantri2($nim,12);
		if (count($cek) >= 2) {
			echo "<script>alert ('Maaf Surat Masih di Proses silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else if($cek[0]['status'] == 1){
			echo "<script>alert ('Maaf Saurat masih antri dan belum diprint, silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}


	}
	public function update_surat_12($cek_update_staff){
		$id_surat = $this->input->post('id');
		$mhs_alamat = $this->input->post('mhs_alamat');
		$dekanat = $this->input->post('dekanat');
		$surat1_keperluan = $this->input->post('surat1_keperluan');

		if($cek_update_staff != 1){
			$data['mhs_alamat'] = $mhs_alamat;
			$data['srt1_keperluan'] = $surat1_keperluan;
		}else {
			$data['mhs_alamat'] = $mhs_alamat;
			$data['srt1_keperluan'] = $surat1_keperluan;
		}
		$data['dekanat'] = $dekanat;

		$tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		$kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		$nim = $this->input->post('nim_mhs');
		$data_siakad['tmplahir'] = $kota_tmpt_mhs;
		$data_siakad['tgllahir'] = $tgl_lahir_mhs;
		$data_siakad['alamat'] = $data['mhs_alamat'];
		$this->Siakad_model->update_data_siakad($nim,$data_siakad);

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}
	
	public function jenis_surat_13(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		$get['data_siakad'] = $this->Siakad_model->getprofil($get['data']['nim']);
		$this->load->view('print/getsurat',$get);
		// echo $nim." ".$id." ".$id_surat;
	}
	public function add_surat_13(){
		date_default_timezone_set("Asia/Jakarta");
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$fakultas = $this->session->userdata('fakultas');
		$srt13_instansi = $this->input->post('srt13_instansi');
		$srt13_alamat = $this->input->post('srt13_alamat');
		$srt13_kabupaten = $this->input->post('srt13_kabupaten');
		$srt13_tujuan = $this->input->post('srt13_tujuan');
		$srt13_keperluan = $this->input->post('srt13_keperluan');
		$srt13_matakuliah = $this->input->post('srt13_matakuliah');
		$srt13_judul = $this->input->post('srt13_judul');
		$srt13_tglawal = $this->input->post('srt13_tglawal');
		$srt13_tglakhir = $this->input->post('srt13_tglakhir');
		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 13;
		$data['nama'] = $nama;
		$data['ket_instansi'] = $srt13_instansi;
		$data['alamat_instansi'] = $srt13_alamat;
		$data['kab_penelitian'] = $srt13_kabupaten;
		$data['laboratorium'] = $srt13_tujuan;
		$data['srt1_keperluan'] = $srt13_keperluan;
		$data['srt7_matkul'] = $srt13_matakuliah;
		$data['judul_penelitian'] = $srt13_judul;
		$data['tgl_penelitian'] = $srt13_tglawal;
		$data['tgl_akhir'] = $srt13_tglakhir;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		if ($fakultas == 'Fak. Sains dan Teknologi'){
			$data['status'] = 88;
			
			//ini kodingan phpmailer jangan dihapus
			$lempar['nim'] = $nim;
			$nip = $this->kaprod($nim);
			$lempar['email'] = $this->getemail($nip);
			$lempar['user'] = $nama;
			$this->load->view('mail/index.php', $lempar);

		} else {
			$data['status'] = 1;
		}
		// print_r ($data);

		$cek = $this->Home_model->cekantri2($nim,13);
		if (count($cek) >= 2) {
			echo "<script>alert ('Maaf Surat Masih di Proses silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else if($cek[0]['status'] == 1){
			echo "<script>alert ('Maaf Saurat masih antri dan belum diprint, silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}


	}
	public function update_surat_13($cek_update_staff){
		$id_surat = $this->input->post('id');
		$srt13_instansi = $this->input->post('srt13_instansi');
		$srt13_alamat = $this->input->post('srt13_alamat');
		$srt13_kabupaten = $this->input->post('srt13_kabupaten');
		$srt13_tujuan = $this->input->post('srt13_tujuan');
		$srt13_keperluan = $this->input->post('srt13_keperluan');
		$srt13_matakuliah = $this->input->post('srt13_matakuliah');
		$srt13_judul = $this->input->post('srt13_judul');
		$srt13_tglawal = $this->input->post('srt13_tglawal');
		$srt13_tglakhir = $this->input->post('srt13_tglakhir');
		
		$dekanat = $this->input->post('dekanat');

		if($cek_update_staff != 1){
			$data['ket_instansi'] = $srt13_instansi;
			$data['alamat_instansi'] = $srt13_alamat;
			$data['kab_penelitian'] = $srt13_kabupaten;
			$data['laboratorium'] = $srt13_tujuan;
			$data['srt7_matkul'] = $srt13_matakuliah;
			$data['srt1_keperluan'] = $srt13_keperluan;
			$data['judul_penelitian'] = $srt13_judul;
			$data['tgl_penelitian'] = $srt13_tglawal;
			$data['tgl_akhir'] = $srt13_tglakhir;
		}else {
			$data['ket_instansi'] = $srt13_instansi;
			$data['alamat_instansi'] = $srt13_alamat;
			$data['kab_penelitian'] = $srt13_kabupaten;
			$data['laboratorium'] = $srt13_tujuan;
			$data['srt7_matkul'] = $srt13_matakuliah;
			$data['srt1_keperluan'] = $srt13_keperluan;
			$data['judul_penelitian'] = $srt13_judul;
			$data['tgl_penelitian'] = $srt13_tglawal;
			$data['tgl_akhir'] = $srt13_tglakhir;
		}

		$data['dekanat'] = $dekanat;

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}
	
	public function jenis_surat_15(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		$get['data_siakad'] = $this->Siakad_model->getprofil($get['data']['nim']);
		$this->load->view('print/getsurat',$get);
		// echo $nim." ".$id." ".$id_surat;
	}
	public function add_surat_15(){
		date_default_timezone_set("Asia/Jakarta");
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$fakultas = $this->session->userdata('fakultas');
		$srt15_instansi = $this->input->post('srt15_instansi');
		$srt15_alamat = $this->input->post('srt15_alamat');
		$srt15_kabupaten = $this->input->post('srt15_kabupaten');
		$srt15_tujuan = $this->input->post('srt15_tujuan');
		$srt15_matakuliah = $this->input->post('srt15_matakuliah');
		$srt15_data = $this->input->post('srt15_data');
		$srt15_judul = $this->input->post('srt15_judul');
		$srt15_tglawal = $this->input->post('srt15_tglawal');
		$srt15_tglakhir = $this->input->post('srt15_tglakhir');
		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 15;
		$data['nama'] = $nama;
		$data['ket_instansi'] = $srt15_instansi;
		$data['alamat_instansi'] = $srt15_alamat;
		$data['kab_penelitian'] = $srt15_kabupaten;
		$data['laboratorium'] = $srt15_tujuan;
		$data['srt7_matkul'] = $srt15_matakuliah;
		$data['str15_data'] = $srt15_data;
		$data['judul_penelitian'] = $srt15_judul;
		$data['tgl_penelitian'] = $srt15_tglawal;
		$data['tgl_akhir'] = $srt15_tglakhir;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		
		if ($fakultas == 'Fak. Sains dan Teknologi'){
			$data['status'] = 88;
			
			//ini kodingan phpmailer jangan dihapus
			$lempar['nim'] = $nim;
			$nip = $this->kaprod($nim);
			$lempar['email'] = $this->getemail($nip);
			$lempar['user'] = $nama;
			$this->load->view('mail/index.php', $lempar);
			
		} else {
			$data['status'] = 1;
		}
			
		// print_r ($data);

		$cek = $this->Home_model->cekantri2($nim,15);
		if (count($cek) >= 2) {
			echo "<script>alert ('Maaf Surat Masih di Proses silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else if($cek[0]['status'] == 1){
			echo "<script>alert ('Maaf Saurat masih antri dan belum diprint, silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}


	}
	public function update_surat_15($cek_update_staff){
		$id_surat = $this->input->post('id');
		$srt15_instansi = $this->input->post('srt15_instansi');
		$srt15_alamat = $this->input->post('srt15_alamat');
		$srt15_kabupaten = $this->input->post('srt15_kabupaten');
		$srt15_tujuan = $this->input->post('srt15_tujuan');
		$srt15_matakuliah = $this->input->post('srt15_matakuliah');
		$srt15_data = $this->input->post('srt15_data');
		$srt15_judul = $this->input->post('srt15_judul');
		$srt15_tglawal = $this->input->post('srt15_tglawal');
		$srt15_tglakhir = $this->input->post('srt15_tglakhir');
		$dekanat = $this->input->post('dekanat');

		if($cek_update_staff != 1){
			$data['ket_instansi'] = $srt15_instansi;
			$data['alamat_instansi'] = $srt15_alamat;
			$data['kab_penelitian'] = $srt15_kabupaten;
			$data['laboratorium'] = $srt15_tujuan;
			$data['srt7_matkul'] = $srt15_matakuliah;
			$data['str15_data'] = $srt15_data;
			$data['judul_penelitian'] = $srt15_judul;
			$data['tgl_penelitian'] = $srt15_tglawal;
			$data['tgl_akhir'] = $srt15_tglakhir;
		}else {
			$data['ket_instansi'] = $srt15_instansi;
			$data['alamat_instansi'] = $srt15_alamat;
			$data['kab_penelitian'] = $srt15_kabupaten;
			$data['laboratorium'] = $srt15_tujuan;
			$data['srt7_matkul'] = $srt15_matakuliah;
			$data['str15_data'] = $srt15_data;
			$data['judul_penelitian'] = $srt15_judul;
			$data['tgl_penelitian'] = $srt15_tglawal;
			$data['tgl_akhir'] = $srt15_tglakhir;
		}

		$data['dekanat'] = $dekanat;

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}
	public function jenis_surat_16(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		$get['data_siakad'] = $this->Siakad_model->getprofil($get['data']['nim']);
		$this->load->view('print/getsurat',$get);
		// echo $nim." ".$id." ".$id_surat;
	}
	public function add_surat_16(){
		date_default_timezone_set("Asia/Jakarta");
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$fakultas = $this->session->userdata('fakultas');
		$srt16_instansi = $this->input->post('srt16_instansi');
		$srt16_alamat = $this->input->post('srt16_alamat');
		$srt16_kabupaten = $this->input->post('srt16_kabupaten');
		$srt16_tujuan = $this->input->post('srt16_tujuan');
		$srt16_data = $this->input->post('srt16_data');
		$srt16_tglawal = $this->input->post('srt16_tglawal');
		$srt16_tglakhir = $this->input->post('srt16_tglakhir');
		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 16;
		$data['nama'] = $nama;
		$data['ket_instansi'] = $srt16_instansi;
		$data['alamat_instansi'] = $srt16_alamat;
		$data['kab_penelitian'] = $srt16_kabupaten;
		$data['laboratorium'] = $srt16_tujuan;
		$data['str15_data'] = $srt16_data;
		$data['tgl_penelitian'] = $srt16_tglawal;
		$data['tgl_akhir'] = $srt16_tglakhir;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		$data['status'] = 1;
			
		// print_r ($data);

		$cek = $this->Home_model->cekantri2($nim,16);
		if (count($cek) >= 2) {
			echo "<script>alert ('Maaf Surat Masih di Proses silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else if($cek[0]['status'] == 1){
			echo "<script>alert ('Maaf Saurat masih antri dan belum diprint, silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}
	}
	public function update_surat_16($cek_update_staff){
		$id_surat = $this->input->post('id');
		$srt16_instansi = $this->input->post('srt16_instansi');
		$srt16_alamat = $this->input->post('srt16_alamat');
		$srt16_kabupaten = $this->input->post('srt16_kabupaten');
		$srt16_tujuan = $this->input->post('srt16_tujuan');
		$srt16_data = $this->input->post('srt16_data');
		$srt16_tglawal = $this->input->post('srt16_tglawal');
		$srt16_tglakhir = $this->input->post('srt16_tglakhir');
		$dekanat = $this->input->post('dekanat');

		if($cek_update_staff != 1){
			$data['ket_instansi'] = $srt16_instansi;
			$data['alamat_instansi'] = $srt16_alamat;
			$data['kab_penelitian'] = $srt16_kabupaten;
			$data['laboratorium'] = $srt16_tujuan;
			$data['str15_data'] = $srt16_data;
			$data['tgl_penelitian'] = $srt16_tglawal;
			$data['tgl_akhir'] = $srt16_tglakhir;
		}else {
			$data['ket_instansi'] = $srt16_instansi;
			$data['alamat_instansi'] = $srt16_alamat;
			$data['kab_penelitian'] = $srt16_kabupaten;
			$data['laboratorium'] = $srt16_tujuan;
			$data['str15_data'] = $srt16_data;
			$data['tgl_penelitian'] = $srt16_tglawal;
			$data['tgl_akhir'] = $srt16_tglakhir;
		}

		$data['dekanat'] = $dekanat;

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}
	
	public function jenis_surat_18(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		$get['data_siakad'] = $this->Siakad_model->getprofil($get['data']['nim']);
		$this->load->view('print/getsurat',$get);
		// echo $nim." ".$id." ".$id_surat;
	}
	public function add_surat_18(){
		date_default_timezone_set("Asia/Jakarta");
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$fakultas = $this->session->userdata('fakultas');
		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 18;
		$data['nama'] = $nama;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		$data['status'] = 1;
			
		// print_r ($data);

		$cek = $this->Home_model->cekantri2($nim,18);
		if (count($cek) >= 2) {
			echo "<script>alert ('Maaf Surat Masih di Proses silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else if($cek[0]['status'] == 1){
			echo "<script>alert ('Maaf Saurat masih antri dan belum diprint, silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}
	}
	public function update_surat_18($cek_update_staff){
		$id_surat = $this->input->post('id');
		$nomor_surat = $this->input->post('nomor_surat');
		$srt16_tglawal = $this->input->post('srt16_tglawal');
		$dekanat = $this->input->post('dekanat');

		if($cek_update_staff != 1){
			$data['nomor_surat'] = $nomor_surat;
			$data['tgl_penelitian'] = $srt16_tglawal;
		}else {
			$data['nomor_surat'] = $nomor_surat;
			$data['tgl_penelitian'] = $srt16_tglawal;
		}

		$data['dekanat'] = $dekanat;

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}


	public function jenis_surat_20(){
		$id = $this->input->post('id');
		$get['data'] = $this->Home_model->getSurat($id);
		$get['data_siakad'] = $this->Siakad_model->getprofil($get['data']['nim']);
		$this->load->view('print/getsurat',$get);
		// echo $nim." ".$id." ".$id_surat;
	}

	public function add_surat_20(){
		date_default_timezone_set('Asia/Jakarta');
		// $nim = $this->session->userdata('nim');
		// $nama = $_SESSION['nama'];
		// $prodi = $_SESSION['prodi'];
		// $semester = $_SESSION['semester'];
		// $alamat = $_SESSION['alamat'];
		// $rt = $_SESSION['rt'];
		// $rw = $_SESSION['rw'];
		// $kelurahan = $_SESSION['kelurahan'];
		// $kecamatan = $_SESSION['kecamatan'];
		// $kota = $_SESSION['kota'];
		$nim = $this->session->userdata('nim');
		$nama = $_SESSION['nama'];
		$prodi = $_SESSION['prodi'];
		$fakultas = $this->session->userdata('fakultas');
		$now = date("Y-m-d H:i:s");
		$data['nim'] = $nim;
		$data['kode_surat'] = 20;
		$data['nama'] = $nama;
		$data['prodi'] = $prodi;
		$data['tgl_submit'] = $now;
		$data['status'] = 1;


		// $surat1_alamat = $this->input->post('surat1_alamat');
		// $surat1_keperluan = $this->input->post('surat1_keperluan');
		// $tgl_lahir_mhs = $this->input->post('tgl_lahir_mhs');
		// $kota_tmpt_mhs = $this->input->post('kota_tmpt_mhs');
		// $data_siakad['tmplahir'] = $kota_tmpt_mhs;
		// $data_siakad['tgllahir'] = $tgl_lahir_mhs;
		// $data_siakad['alamat'] = ucwords(strtolower($surat1_alamat));

		// $this->Siakad_model->update_data_siakad($nim,$data_siakad);

		// $now = date("Y-m-d H:i:s");
		// $data['nim'] = $nim;
		// $data['kode_surat'] = 20;
		// $data['nama'] = $nama;
		// $data['mhs_alamat'] = $surat1_alamat;
		// $data['srt1_keperluan'] = $surat1_keperluan;
		// $data['prodi'] = $prodi;
		// $data['tgl_submit'] = $now;
		// $data['status'] = 1;
		// $data['semester'] = $semester;
		// $data['alamat'] = $alamat;
		// $data['rt'] = $rt;
		// $data['rw'] = $rw;
		// $data['kelurahan'] = $kelurahan;
		// $data['kecamatan'] = $kecamatan;
		// $data['kota'] = $kota;




		// print_r ($data);
		$cek = $this->Home_model->cekantri2($nim,20);
		if (count($cek) >= 2) {
			echo "<script>alert ('Maaf Surat Masih di Proses silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else if($cek[0]['status'] == 1){
			echo "<script>alert ('Maaf Saurat masih antri dan belum diprint, silahkan cek status diaplikasi !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}
	}

	public function update_surat_20($cek_update_staff){
		$id_surat = $this->input->post('id');
		$nomor_surat = $this->input->post('nomor_surat');
		$srt16_tglawal = $this->input->post('srt16_tglawal');
		$dekanat = $this->input->post('dekanat');

		if($cek_update_staff != 1){
			$data['nomor_surat'] = $nomor_surat;
			$data['tgl_penelitian'] = $srt16_tglawal;
		}else {
			$data['nomor_surat'] = $nomor_surat;
			$data['tgl_penelitian'] = $srt16_tglawal;
		}

		$data['dekanat'] = $dekanat;

		$this->Home_model->hapus($id_surat,$data);
		header("location:".base_url()."admin");
	}


}
