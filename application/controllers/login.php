<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download', 'cookie'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	}
	public function index(){
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
		// echo "<script>alert('One Day Service Hanya Bisa Diakses Melalui http://ctrl.uinsby.ac.id'); location.href = 'http://ctrl.uinsby.ac.id/index';</script>";
		// header("location:http://ctrl.uinsby.ac.id/index");
	}

	public function sso(){
		$par = get_cookie('nip');
		$token = get_cookie('token');
		$param['par'] = $par;
		$param['token'] = $token;
		$this->load->view('sso.php',$param);
	}

	public function sso2(){
		$par = get_cookie('nip');
		$token = get_cookie('token');
		$param['par'] = $par;
		$param['token'] = $token;
		$this->load->view('sso2.php',$param);
	}

	public function cek1() { //untuk mhs
		$username = $this->input->post('username');
		$token = $this->input->post('token');

		if($token == get_cookie('token')) {
			$par = base64_decode(base64_decode($username));
			$datax = explode('|', $par);
			$nip = $datax[0];
			$tanggal = $datax[1];
			if($tanggal == date("Y-m-d")) {
				$data_siakad = $this->ceksiakad1($nip);
				// echo $data_siakad = 1;
				if($data_siakad == 0){
					echo "<script>alert ('Maaf Username/Password Salah !');window.location.href = '".base_url()."login';</script>";
				}else {
					echo "<script>window.location.href = '".base_url()."home';</script>";
				}
			}
		}
	}

	public function cek2() { //untuk dosen
		$username = $this->input->post('username');
		$token = $this->input->post('token');

		if($token == get_cookie('token')) {
			$par = base64_decode(base64_decode($username));
			$datax = explode('|', $par);
			$nip = $datax[0];
			$tanggal = $datax[1];

			//  echo date("Y-m-d");
			if($tanggal == date("Y-m-d")) {
				$this->session->set_userdata('username',$nip);
					$data_simpeg=$this->Home_model->login_simpeg($nip);
					// print_r($data_simpeg);

				$param =  $data_simpeg[0]->par;
				$fak =  $data_simpeg[0]->kode;
				if($param == 1){
					if($fak == 'Z') {
						$this->session->set_userdata('fak','dewo');
					} else {
						$this->session->set_userdata('fak',$fak);
					}
					echo "<script>window.location.href = '".base_url()."admin';</script>";
				} else {
					echo "<script>window.location.href = '".base_url()."login';</script>";
				}
			}
		}
	}

	public function cek(){
		// untuk memastikan bahwa halaman login adalah pada login ods
		$id = $this->input->post('id_login');
		if($id == 1 ){
				$user = $this->input->post('nim');
				$password = md5($this->input->post('password'));
				// cek di simpeg pegawai untuk login
				$data = $this->Home_model->login_tembus($user,$password);
				if($data == 0){
					$datax = $this->Home_model->login($user,$password);
					if ($datax==0) {
						$data_siakad = $this->ceksiakad($user,$password);
						if($data_siakad == 0){
							echo "<script>alert ('Maaf Username/Password Salah !');window.location.href = '".base_url()."login';</script>";
						}else {
							echo "<script>window.location.href = '".base_url()."home';</script>";
						}
					} else {
						$this->session->set_userdata('username',$user);
						$this->session->set_userdata('fak','dewo');
						echo "<script>window.location.href = '".base_url()."admin';</script>";
					}
					$data_siakad = $this->ceksiakad($user,$password);
					if($data_siakad == 0){
						echo "<script>alert ('Maaf Username/Password Salah !');window.location.href = '".base_url()."login';</script>";
					}else {
						echo "<script>window.location.href = '".base_url()."home';</script>";
					}

				}else if($data == 1){
					$this->session->set_userdata('username',$user);
					 $data_simpeg=$this->Home_model->login_simpeg($user);
					//print_r($data_simpeg);

					$param =  $data_simpeg[0]->par;
					$fak =  $data_simpeg[0]->kode;
					if($param == 1){
						$this->session->set_userdata('fak',$fak);
						echo "<script>window.location.href = '".base_url()."admin';</script>";
					}else{
						$data = $this->Home_model->login($user,$password);
						if ($data==0) {
							echo "<script>window.location.href = '".base_url()."login';</script>";
						} else {
							$this->session->set_userdata('fak','dewo');
							echo "<script>window.location.href = '".base_url()."admin';</script>";
						}
					}
				}else {
					// echo "ASdsa";
					$data = $this->Home_model->login($user,$password);
						if ($data==0) {
							echo "<script>window.location.href = '".base_url()."login';</script>";
						} else {
							$this->session->set_userdata('fak','dewo');
							echo "<script>window.location.href = '".base_url()."admin';</script>";
						}
					// echo "<script>window.location.href = '".base_url()."login';</script>";
				}
		}else {
			header("location:".base_url()."login");
		}
	}

	public function ceksiakad($username,$password){
		SESSION_START();
		$username = strtoupper($username);
		$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password=ampelakademik!3");
		$result = pg_prepare($conn, "my_query", "select a.nim, a.sex , c.nama_program_studi,a.nama , d.kodeunitparent
												from akademik.ms_mahasiswa a , gate.sc_user b , akademik.ak_prodi c , gate.ms_unit d
												where a.nim = b.username
												and a.kodeunit = c.kodeunit
												and d.kodeunit = a.kodeunit
												and b.username = $1
												and b.password = $2;");
		$result = pg_execute($conn, "my_query", array($username,$password));
		while ($row = pg_fetch_assoc($result)){
			 $user = $row['nim'];
			$sex = $row['sex'];
			 $prodi = $row['nama_program_studi'];
			 $nama = $row['nama'];
			 $kodefak = $row['kodeunitparent'];
		}
		if($user == null || $user == ""){
			 $datas = 0;
		}else {
			// kode fakultas lihat ditabel kodeunit pada database ods
			if ($kodefak == "G" || $kodefak =="I"|| $kodefak == "H" || $kodefak == "B" ||
				$kodefak == "A" || $kodefak == 'E' || $kodefak == 'J' || $kodefak == 'C' ||
				$kodefak == 'D' ) {
				$datas = 1;
				$this->session->set_userdata('nim',$user);
				$_SESSION['sex'] = $sex;
				$_SESSION['prodi'] = $prodi;
				$_SESSION['nama'] = $nama;

				// $this->session->set_userdata('sex',$sex);
				// $this->session->set_userdata('prodi',$prodi);
				// $this->session->set_userdata('nama',$nama);
				$this->session->set_userdata('kodefak',$kodefak);
			}else {
				echo "<script>alert ('Maaf anda bukan mahasiswa FEBI atau Fisip !');window.location.href = '".base_url()."login';</script>";
			}
		}
		return $datas;


	}

	public function ceksiakad1($username){
		SESSION_START();
		$username = strtoupper($username);
		 $conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password=ampelakademik!3");



		 $result = pg_prepare($conn, "my_query", "select a.nim, a.sex , c.nama_program_studi,a.nama , d.kodeunitparent, a.semestermhs, a.kodeunit
		 from akademik.ms_mahasiswa a , gate.sc_user b , akademik.ak_prodi c , gate.ms_unit d
		 where a.nim = b.username
		 and a.kodeunit = c.kodeunit
		 and d.kodeunit = a.kodeunit
		 and b.username = $1");


		 $result = pg_execute($conn, "my_query", array($username));
		while ($row = pg_fetch_assoc($result)){
			 $user = $row['nim'];
			 $sex = $row['sex'];
			 $prodi = $row['nama_program_studi'];
			 $nama = $row['nama'];
			 $kodefak = $row['kodeunitparent'];
			 $semester = $row['semestermhs'];
		}
		if($user == null || $user == ""){
			 $datas = 0;
		}else {
			if ($kodefak == "G" || $kodefak =="I"|| $kodefak == "H" || $kodefak == "B" || $kodefak == "A") {
				$datas = 1;
				$this->session->set_userdata('nim',$user);
				$_SESSION['sex'] = $sex;
				$_SESSION['prodi'] = $prodi;
				$_SESSION['nama'] = $nama;
				$_SESSION['semester'] = $semester;
				$this->session->set_userdata('kodefak',$kodefak);
			}else {
				echo "<script>alert ('Maaf anda bukan mahasiswa FEBI atau Fisip !');window.location.href = '".base_url()."login';</script>";
			}
		}
		return $datas;


	}



}
