<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_h extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download','cookie'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

		
		$tes = $_COOKIE['prod'];
		 if(empty($tes)){
			 header("location:".base_url()."login");
		 } 
		
		// $user = $this->session->userdata('username');
		// if($user == null){
			// header("location:".base_url()."login");
		// }

	}
	public function index(){
		 // $user = $this->session->userdata('username');
		 // $user = $this->session->userdata('198403072014031001');
		 // $dewo = $this->session->userdata('SINF');
		$user = get_cookie('nip');
		$dewo = get_cookie('prod');

		// $dewo = 'SINF';
		//  echo('hai');



		// $data['menunggu'] = $this->Home_model->getPrint(2);
		// $data['all'] = $this->Home_model->getPrint();
		
		// $data = $this->Home_model->getPrintKhusus(1);
		// print_r($data);
		$data['antri'] = $this->Home_model->getPrintKhusus(88,$dewo);
		$data['all'] = $this->Home_model->getPrintKhusus1(1,$dewo);
		
		$this->load->view('header');
		$this->load->view('formskaprodi',$data);
		$this->load->view('footer');
	}
	public function logout(){
		$this->session->unset_userdata(array('username' => ''));
		$this->session->unset_userdata(array('id' => ''));
		header("location:../admin");

	}
	public function gis(){
		$data = $this->Home_model->getGis();
		//print_r ($data);
		//echo "<br/>";
		//echo json_encode ($data);
		//header("location:../admin");
	}

}
