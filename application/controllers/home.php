<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		SESSION_START();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->model('Siakad_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

		// $tes = $_COOKIE['token'];
		//  if(empty($tes)){
		// 	 header("location:".base_url()."login");
		//  }


		$nim = $this->session->userdata('nim');
		 if($nim == null){
			 header("location:".base_url()."login");
		 }
	}

	public function index(){

		// echo $this->session->userdata('fakultas');
		$kodefak = $this->session->userdata('kodefak');
		$nim = $this->session->userdata('nim');
		// echo $nimm = $_COOKIE['nim'];


		$this->getFakultas($kodefak);
		$data['profil'] = $this->getProfil($nim);
		if($nim != ""){
			$data['profil'] = $this->Siakad_model->getprofil($nim);
		}
		// print_r ($data['profil']);
		$data["antria"] = $this->Home_model->getantri($nim);
		$data["provinsi"] = $this->Home_model->getProv();
		$data["surat"] = $this->Home_model->get_surat();
		$this->load->view('header');
		$this->load->view('form',$data);
		$this->load->view('footer');
	}

	public function getFakultas($kode){
		$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password='ampelakademik!3'");
		//$result = pg_prepare($conn, "my_query", 'SELECT * FROM gate.sc_user WHERE username = $1 and password = $2
		$result = pg_prepare($conn, "my_query", "select namaunit from gate.ms_unit where kodeunit = $1");
		$result = pg_execute($conn, "my_query", array($kode));
		while ($row = pg_fetch_assoc($result)){
			$fak = $row['namaunit'];
		}
		$this->session->set_userdata('fakultas',$fak);
		//return $fak;
	}

	public function getProfil($nim){
		$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password='ampelakademik!3'");
		$result = pg_prepare($conn, "my_query", "select a.nim,a.nama,a.semestermhs,a.kodeunit, a.noijasah, a.alamat,a.tmplahir , a.tgllahir , a.ipk , b.namaunit ,b.kodeunitparent,
												c.namakota,d.namapropinsi, a.noijasah
												from akademik.ms_mahasiswa a, gate.ms_unit b, akademik.ms_kota c , akademik.ms_propinsi d
												where a.kodeunit = b.kodeunit
												and c.kodekota = a.kodekota
												and c.kodepropinsi = d.kodepropinsi
												and nim = $1");
		$result = pg_execute($conn, "my_query", array($nim));
		while ($row = pg_fetch_assoc($result)){
			$semestermhs = $row['semestermhs'];
			$alamat = $row['alamat'];
			$tgllahir = $row['tgllahir'];
			$tmplahir = $row['tmplahir'];
			$ipk = $row['ipk'];
			$data = array(
					 'nim' => $row['nim'] ,
					 'nama' => $row['nama'] ,
					 'kodeunit' => $row['kodeunit'] ,
					 'prodi' => $row['namaunit'] ,
					 'kodefakultas' => $row['kodeunitparent'] ,
					 'semester' => $semestermhs ,
					 'alamat' => $alamat ,
					 'kota' => $row['namakota'] ,
					 'prov' => $row['namapropinsi'] ,
					 'tgllahir' => $tgllahir ,
					 'tmplahir' => $tmplahir ,
					 'ipk' => $ipk,
					 'noijasah' => $row['noijasah']
				);
		}
		return $data;
	}

	public function profil(){
		$data["antri"] = $this->Home_model->getantri($nim);
		$this->load->view('header');
		$this->load->view('form',$data);
		$this->load->view('footer');
	}
	public function delete($id){
		$data = array(
				 'status' => "0"
			);
		$this->Home_model->hapus($id,$data);
		header("location:".base_url()."home");
	}
	public function selesai($id){
		$data = array(
				 'status' => "3"
			);
		$this->Home_model->hapus($id,$data);
		header("location:".base_url()."admin");
	}

	public function rekom(){
		$this->load->view('header');
		$this->load->view('rekom');
		$this->load->view('footer');
	}
	public function surat(){
		$this->load->view('print/magang');
	}

	public function cekKab($id_prov){
		$kab = $this->Home_model->getkab($id_prov);
		foreach ($kab as $Hkab) {
			$simpan = $simpan."<option value='".$Hkab['id']."'>".$Hkab['nama']."</option>";
		}
		echo $simpan;
		// print_r ($data["kab"]);
	}

	public function pesanMagang($kode_surat){
		$kodefak = $this->session->userdata('kodefak');
		$nim = $this->session->userdata('nim');
		$data["antria"] = $this->Home_model->getantri($nim);
		$data["provinsi"] = $this->Home_model->getProv();
		$data["kode_surat"] = $kode_surat;
		$this->load->view('header');
		$this->load->view('print/formmagang',$data);
		$this->load->view('footer');
	}
	public function ceknama($nim,$id_input){
		$nim = strtoupper($nim);
		 $url = "http://eis.uinsby.ac.id/eis/nama/$nim";
		 $login = file_get_contents($url);
		 $pas = json_decode($login,$true);

		 echo "<br/>";
		 echo "<label for='nama' class='lb'>NIM</label><input type='text' class='form-control' id='$id_input' name='nim_mhs_akeh[]' value='".$pas[0]->nim."' />";
		 //print_r ();
		 echo "<br/>";
		 echo "<label for='nama' class='lb'>Nama</label><input type='text' class='form-control' value='".$pas[0]->nama."' disabled />";
		 echo "<input type='hidden' name='nama_mhs_akeh[]' value='".$pas[0]->nama."' />";
		 echo "<br/>";
		 echo "<label for='nama' class='lb'>Program Studi</label><input type='text' class='form-control'  value='".$pas[0]->prodi."' disabled />";
		 echo "<input type='hidden'  name='prodi_mhs_akeh[]' value='".$pas[0]->prodi."' />";
		 echo "<input type='hidden'  name='semester_mhs_akeh[]' value='".$pas[0]->semester."' />";

		// print_r ($pas[0]->username);
	}

}
