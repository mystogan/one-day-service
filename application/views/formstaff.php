<?php
function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
	$jam  = substr($date, 11, 2);
	$menit  = substr($date, 14, 2);
  $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun." ".$jam.":".$menit;
  return $data;
}

 ?>

<body style="background:url(<?php echo base_url();?>assets/images/background.png) center">
<div style="min-height:90%;">
<div class="satu" style="background-color:rgb(35, 140, 2);height:1vh;color:#fff;font-size:2vh;font-family: 'OpenSansLight', sans-serif;border-bottom:2px dotted #fff;"></div>
<div class="contentmenu">
<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="row">
			<div class="menu1">
				<ul class="nav tab1 nav-tabs " style="padding-left:10vw;" role="tablist">
              <li role="presentation" class="active" ><a href="#surat" aria-controls="surat" role="tab" data-toggle="tab"><img title="Permintaan Surat" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/1.png"/>Permintaan Surat</a></li>
              <li role="presentation"><a href="#Eksekusi" aria-controls="Eksekusi" role="tab" data-toggle="tab"><img title="Riwayat" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/2.png"/>Notifikasi</a></li>
              <li role="presentation"><a href="#Riwayat" aria-controls="Riwayat" role="tab" data-toggle="tab"><img title="Riwayat" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/clock_history.png"/>Riwayat</a></li>
              <li role="presentation"><a href="#Pengaturan" aria-controls="Pengaturan" role="tab" data-toggle="tab"><img title="Pengaturan" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/2.png"/>View Surat</a></li>
				<li role="presentation"><a href="<?php echo base_url();?>admin/logout"><img title="Status" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/logout.png"/>Keluar</a></li>
		  </ul>
			</div>
		</div>
	</div>
</div>
</div>
<div class="content">
<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="row">
				<div class="tab-content">
            	<div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="col-xs-4 col-sm-4 col-md-4" >
                     <select name="" id="" class="form-control">
                        <option value="">asd</option>
                     </select>
                  </div>
                  <div class="col-xs-8 col-sm-8 col-md-8" ></div>
               </div>
               <br>
               <br>
					<div role="tabpanel" class="tab-pane active" id="surat">
						<div class="panel-body">

                  <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">

                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example" style="background-color:#fff">
                      <thead>
                          <tr>
                              <th style="text-align:center;">NIM</th>
                              <th style="text-align:center;">Nama Mahasiswa</th>
                              <th style="text-align:center;">Prodi</th>
                              <th style="text-align:center;">Surat Terkait</th>
                              <th style="text-align:center;">Tanggal Pesan</th>
                              <th style="text-align:center;">Lampiran</th>
                              <th style="text-align:center;">Status</th>
                              <th style="text-align:center;">Print</th>
                              <th style="text-align:center;">Edit</th>
                          </tr>
                      </thead>
                      <tbody>
										<?php
										foreach ($antri as $Hantri ) { ?>

											<tr>
												<td style="text-align:center;"><?php echo $Hantri['nim'] ?></td>
												<td style="text-align:center;"><?php echo $Hantri['nama'] ?></td>
												<td style="text-align:center;"><?php echo $Hantri['prodi'] ?></td>
												<td style="text-align:center;"><?php echo $Hantri['srt_nama']; ?></td>
												<td style="text-align:center;"><?php
												$tgl = $Hantri['tgl_submit'];
												echo $tgl = indo($tgl);
												?></td>
												<td style="text-align:center;">
                          <?php
                          if($Hantri['file'] == null){
                            echo "-";
                          }else { ?>
                            <a href="<?php echo base_url()."assets/foto/".$Hantri['file']; ?>"><?php echo $Hantri['file']; ?></a>
                            <a href="<?php echo base_url()."assets/foto/".$Hantri['file_kk']; ?>"><?php echo $Hantri['file_kk']; ?></a>
                            <a href="<?php echo base_url()."assets/foto/".$Hantri['file_ktm']; ?>"><?php echo $Hantri['file_ktm']; ?></a>
                          <?php
                          }
                           ?>

                        </td>
												<td style="text-align:center;"><a class="btn-xs btn btn-warning">Belum di Print</a></td>
												<td style="text-align:center;">
													<form class="" action="<?php echo base_url(); ?>prin" method="post">
														<input type="hidden" name="id_surat" value="<?php echo $Hantri['id']; ?>" >
														<input type="hidden" name="kode_surat" value="<?php echo $Hantri['kode_surat']; ?>" >
														<input type="hidden" name="nim" value="<?php echo $Hantri['nim']; ?>" >
														<button type="submit" class="btn btn-default">Print</button>
													</form>
												</td>
												<td style="text-align:center;">
													<?php
													if($Hantri['kode_surat'] == 1  ){ ?>
														<div data-toggle="modal" data-target="#surat_1" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_1');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 2  ){ ?>
														<div data-toggle="modal" data-target="#surat_2" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_2');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 3 ){ ?>
														<div data-toggle="modal" data-target="#surat_3" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_3');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 4 ){ ?>
														<div data-toggle="modal" data-target="#surat_4" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_4');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 5 ){ ?>
														<div data-toggle="modal" data-target="#surat_5" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_5');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 6 ){ ?>
														<div data-toggle="modal" data-target="#surat_6" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_6');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 7 ){ ?>
														<div data-toggle="modal" data-target="#surat_7" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_7');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 8 ){ ?>
														<div data-toggle="modal" data-target="#surat_7" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_7');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 9 ){ ?>
														<div data-toggle="modal" data-target="#surat_7" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_7');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 10 ){ ?>
														<div data-toggle="modal" data-target="#surat_7" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_7');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 11 ){ ?>
														<div data-toggle="modal" data-target="#surat_11" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_11');">Edit</a></div>
														<?php
                          }else if($Hantri['kode_surat'] == 12 ){ ?>
														<div data-toggle="modal" data-target="#surat_12" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_12');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 13 ){ ?>
														<div data-toggle="modal" data-target="#surat_13" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_13');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 14 ){ ?>
														<div data-toggle="modal" data-target="#surat_7" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_7');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 15 ){ ?>
														<div data-toggle="modal" data-target="#surat_15" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_15');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 16 ){ ?>
														<div data-toggle="modal" data-target="#surat_16" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_16');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 18 ){ ?>
														<div data-toggle="modal" data-target="#surat_18" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_18');">Edit</a></div>
													<?php
                          }else if($Hantri['kode_surat'] == 19 ){ ?>
															<div data-toggle="modal" data-target="#surat_19" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_19');">Edit</a></div>
														<?php
                          }else if($Hantri['kode_surat'] == 20 ){ ?>
															<div data-toggle="modal" data-target="#surat_20" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hantri['id']; ?>','jenis_surat_20');">Edit</a></div>
														<?php
									}
													?>
												</td>
											</tr>

										<?php
										}
										 ?>


		                                </tbody>
		                            </table>

		                            <!-- /.table-responsive -->
		                            </div>
		                        </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="Eksekusi">
						<div class="panel-body">

		                            <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">

		                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example2" style="background-color:#fff">
		                                <thead>
		                                    <tr>
		                                        <th style="text-align:center;">NIM</th>
		                                        <th style="text-align:center;">Nama Mahasiswa</th>
		                                        <th style="text-align:center;">Prodi</th>
		                                        <th style="text-align:center;">Surat</th>
		                                        <th style="text-align:center;">Tanggal Pesan</th>
		                                        <th style="text-align:center;">Lampiran</th>
		                                        <th style="text-align:center;">Status</th>
		                                        <th style="text-align:center;"></th>
		                                        <th style="text-align:center;">Edit</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
											<?php
											foreach ($menunggu as $Hmenunggu ) { ?>
												<tr>
													<td style="text-align:center;">

														<form class="" action="<?php echo base_url(); ?>prin" method="post">
															<input type="hidden" name="id_surat" value="<?php echo $Hmenunggu['id']; ?>" >
															<input type="hidden" name="kode_surat" value="<?php echo $Hmenunggu['kode_surat']; ?>" >
															<input type="hidden" name="nim" value="<?php echo $Hmenunggu['nim']; ?>" >
															<button type="submit" class="btn"><?php echo $Hmenunggu['nim']; ?></button>
														</form>

													</td>
													<td style="text-align:center;"><?php echo $Hmenunggu['nama']; ?></td>
													<td style="text-align:center;"><?php echo $Hmenunggu['prodi']; ?></td>
													<td style="text-align:center;"><?php echo $Hmenunggu['srt_nama']; ?></td>
													<td style="text-align:center;"><?php
													$tgl = $Hmenunggu['tgl_submit'];
													echo $tgl = indo($tgl);
													?></td>
													<td style="text-align:center;">
														<?php
														if($Hmenunggu['file'] == null){
															echo "-";
														}else { ?>
															<a href="<?php echo base_url()."assets/foto/".$Hmenunggu['file']; ?>"><?php echo $Hmenunggu['file']; ?></a>
															<a href="<?php echo base_url()."assets/foto/".$Hmenunggu['file_kk']; ?>"><?php echo $Hmenunggu['file_kk']; ?></a>
															<a href="<?php echo base_url()."assets/foto/".$Hmenunggu['file_ktm']; ?>"><?php echo $Hmenunggu['file_ktm']; ?></a>
														<?php
														}
														 ?>
													</td>
													<td style="text-align:center;"><a class="btn-xs btn btn-info" >Menunggu</a></td>
													<td style="text-align:center;"><a class="btn btn-success" type='button' href="<?php echo base_url();?>home/selesai/<?php echo $Hmenunggu['id']; ?>" >Selesai</a></td>
													<td style="text-align:center;">
                            <?php
  													if($Hmenunggu['kode_surat'] == 1  ){ ?>
  														<div data-toggle="modal" data-target="#surat_1" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_1');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 2  ){ ?>
  														<div data-toggle="modal" data-target="#surat_2" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_2');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 3  ){ ?>
  														<div data-toggle="modal" data-target="#surat_3" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_3');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 4  ){ ?>
  														<div data-toggle="modal" data-target="#surat_4" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_4');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 5  ){ ?>
  														<div data-toggle="modal" data-target="#surat_5" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_5');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 6  ){ ?>
  														<div data-toggle="modal" data-target="#surat_6" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_6');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 7  ){ ?>
  														<div data-toggle="modal" data-target="#surat_7" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_7');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 8  ){ ?>
  														<div data-toggle="modal" data-target="#surat_7" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_7');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 9  ){ ?>
  														<div data-toggle="modal" data-target="#surat_7" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_7');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 10  ){ ?>
  														<div data-toggle="modal" data-target="#surat_7" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_7');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 11  ){ ?>
  														<div data-toggle="modal" data-target="#surat_11" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_11');">Edit</a></div>
														<?php
                            }else if($Hmenunggu['kode_surat'] == 12  ){ ?>
  														<div data-toggle="modal" data-target="#surat_12" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_12');">Edit</a></div>
														<?php
                            }else if($Hmenunggu['kode_surat'] == 13  ){ ?>
  														<div data-toggle="modal" data-target="#surat_13" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_13');">Edit</a></div>
													<?php
							}else if($Hmenunggu['kode_surat'] == 14 ){ ?>
														<div data-toggle="modal" data-target="#surat_7" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_7');">Edit</a></div>	
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 15  ){ ?>
  														<div data-toggle="modal" data-target="#surat_15" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_15');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 16  ){ ?>
  														<div data-toggle="modal" data-target="#surat_16" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_16');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 18  ){ ?>
  														<div data-toggle="modal" data-target="#surat_18" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_18');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 19  ){ ?>
  														<div data-toggle="modal" data-target="#surat_19" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_19');">Edit</a></div>
  													<?php
                            }else if($Hmenunggu['kode_surat'] == 20  ){ ?>
  														<div data-toggle="modal" data-target="#surat_20" ><a class="btn btn-default" type="button" href="javascript:getsurat('<?php echo $Hmenunggu['id']; ?>','jenis_surat_20');">Edit</a></div>
  													<?php
                            }
  													?>
													</td>
												</tr>
											<?php
											}
											?>

		                                </tbody>
		                            </table>

		                            <!-- /.table-responsive -->
		                            </div>
		                        </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="Riwayat">
						<div class="panel-body">

		                            <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">

		                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example3" style="background-color:#fff">
		                                <thead>
		                                    <tr>
		                                        <th style="text-align:center;">NIM</th>
		                                        <th style="text-align:center;">Nama Mahasiswa</th>
		                                        <th style="text-align:center;">Prodi</th>
		                                        <th style="text-align:center;">Jenis Surat</th>
		                                        <th style="text-align:center;">Tanggal Pesan</th>
		                                        <th style="text-align:center;">Status</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
																			<?php foreach ($all as $Hall){ ?>
																				<tr>
		                                        <td style="text-align:center;">
                                              <form class="" action="<?php echo base_url(); ?>prin" method="post">
                  															<input type="hidden" name="id_surat" value="<?php echo $Hall['id']; ?>" >
                  															<input type="hidden" name="kode_surat" value="<?php echo $Hall['kode_surat']; ?>" >
                  															<input type="hidden" name="nim" value="<?php echo $Hall['nim']; ?>" >
                  															<button type="submit" class="btn-xs"><?php echo $Hall['nim']; ?></button>
                  														</form>
                                            </td>
		                                        <td style="text-align:center;"><?php echo $Hall['nama'];?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['prodi'];?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['srt_nama'];?></td>
		                                        <td style="text-align:center;"><?php

																						$tgl = $Hall['tgl_submit'];
																						echo $tgl = indo($tgl);
																						?></td>
																						<td style="text-align:center;">
					                                  <?php
					                                     if($Hall['status'] == "1"){ ?>
							                                  <a class="btn-xs btn btn-warning">Belum diprint</a>
							                                  <?php
							                                }else if($Hall['status'] == "2"){ ?>
							                                  <a class="btn-xs btn btn-info" >Menunggu</a>
							                                  <?php
							                                }else { ?>
							                                   <a class="btn-xs btn btn-success" >Bisa diambil</a>
							                                  <?php
							                                }
					                                     ?>
					                                  </td>
		                                    </tr>
																			<?php
																			}
																			?>

		                                </tbody>
		                            </table>

		                            <!-- /.table-responsive -->
		                            </div>
		                        </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="Pengaturan">
                  <ul class="thumbnails">
					<div class="row">
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">1</div>
                           <div class="caption">
                              <h4><?php echo $surat[0]['srt_nama'] ?> </h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <!-- <a  class="btn btn-success" href="javascript:cek();">» Buat Surat</a> -->
                              <a data-toggle="modal" data-target="#surat_aktif_studi" class="btn btn-success" >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">2</div>
                           <div class="caption">
                              <h4><?php echo $surat[1]['srt_nama'] ?></h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#surat_aktif_studi_besiswa" class="btn btn-success" >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
					 
					 <?php if($fakultas == 'Fak. Sains dan Teknologi' || $fakultas == 'Fak. Dakwah dan Komunikasi' || $fakultas == 'Fak. Adab dan Humaniora'){ ?>
						<li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">3</div>
                            <div class="caption">
                               <h4><?php echo $surat[13]['srt_nama'] ?></h4>
							   <br>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a href="<?php echo base_url();?>home/pesanMagang/14" class="btn btn-success " >» Buat Surat</a>
                            </div>
                         </div>
                      </li>
					<?php } else { ?> 
						 <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">3</div>
                           <div class="caption">
                              <h4><?php echo $surat[2]['srt_nama'] ?></h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#surat_pra_penelitian" class="btn btn-success" >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
					<?php } ?> 

					<?php if($fakultas == 'Fak. Dakwah dan Komunikasi'){ ?>
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">4</div>
                           <div class="caption">
                              <h4><?php echo $surat[3]['srt_nama'] ?></h4>
                              <br>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a href="<?php echo base_url();?>home/pesanMagang/4" class="btn btn-success " >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
					<?php } else { ?> 
						<li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">4</div>
                           <div class="caption">
                              <h4><?php echo $surat[3]['srt_nama'] ?></h4>
                              <br>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#surat_penelitian" class="btn btn-success" >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
					<?php } ?> 
					 
					 
					 </div>
					 <div class="row">
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">5</div>
                           <div class="caption">
                              <h4><?php echo $surat[4]['srt_nama'] ?></h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#surat_cuti_studi" class="btn btn-success" >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">6</div>
                           <div class="caption">
                              <h4><?php echo $surat[5]['srt_nama'] ?></h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#surat_pernyataan_masih_kuliah" class="btn btn-success" >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">7</div>
                           <div class="caption">
                              <h4><?php echo $surat[6]['srt_nama'] ?></h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a href="<?php echo base_url();?>home/pesanMagang/7" class="btn btn-success" href="#">» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">8</div>
                           <div class="caption">
                              <h4><?php echo $surat[7]['srt_nama'] ?></h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a href="<?php echo base_url();?>home/pesanMagang/8" class="btn btn-success" href="#">» Buat Surat</a>
                           </div>
                        </div>
                     </li>
					 </div>
					<div class="row">
					 <?php if($fakultas == 'Fak. Sains dan Teknologi'):?>
							 <li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">9</div>
                            <div class="caption">
                               <h4><?php echo $surat[11]['srt_nama'] ?></h4>
							   <br>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a data-toggle="modal" data-target="#surat_rekomendasi_saintek" class="btn btn-success " >» Buat Surat</a>
                            </div>
                         </div>
                      </li>
						<li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">10</div>
                            <div class="caption">
                               <h4><?php echo $surat[12]['srt_nama'] ?></h4>
							   <br>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a data-toggle="modal" data-target="#surat_uji_laboratorium" class="btn btn-success " >» Buat Surat</a>
                            </div>
                         </div>
                      </li>
						<li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">11</div>
                            <div class="caption">
                               <h4><?php echo $surat[14]['srt_nama'] ?></h4>
							   <br>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a data-toggle="modal" data-target="#surat_permohonan_data" class="btn btn-success " >» Buat Surat</a>
                            </div>
                         </div>
                      </li>
						<li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">12</div>
                            <div class="caption">
                               <h4><?php echo $surat[15]['srt_nama'] ?></h4>
							   <br>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a href="<?php echo base_url();?>home/pesanMagang/16" class="btn btn-success " >» Buat Surat</a>
                            </div>
                         </div>
                      </li>
					</div>
					<?php else: ?>
					<li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">9</div>
                           <div class="caption">
                              <h4><?php echo $surat[10]['srt_nama'] ?></h4>
                              <br>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#surat_rekomendasi" class="btn btn-success " >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
					<?php endif;?>
					
               
					<?php if($prodi == 'Ekonomi Syariah'):?>
					
					<li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">10</div>
                            <div class="caption">
                               <h4><?php echo $surat[8]['srt_nama'] ?></h4>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a href="<?php echo base_url();?>home/pesanMagang/9" class="btn btn-success" href="#">» Buat Surat</a>
                            </div>
                         </div>
                      </li>
                      <li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">11</div>
                            <div class="caption">
                               <h4><?php echo $surat[9]['srt_nama'] ?></h4>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a href="<?php echo base_url();?>home/pesanMagang/10" class="btn btn-success" href="#">» Buat Surat</a>
                            </div>
                         </div>
                      </li>
					  </div>
					
					
					<?php endif;?>

               <?php if($fakultas == 'Fak. Adab dan Humaniora'): ?>
					<li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">10</div>
                           <div class="caption">
                           <form class="" action="<?php echo base_url(); ?>surat/add_surat_20" method="post">
                              <h4><?php echo $surat[19]['srt_nama'] ?></h4>
                              <br>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <button class="btn btn-success ">» Buat Surat</button>
                           </form>
                           </div>
                        </div>
                     </li>
					<?php endif;?>
					
					<?php if($fakultas == 'Fak. Dakwah dan Komunikasi'):?>
					<!--
					<li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">10</div>
                            <div class="caption">
                               <h4><?php echo $surat[18]['srt_nama'] ?></h4>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a data-toggle="modal" data-target="#" class="btn btn-success ">» Buat Surat</a>
                            </div>
                         </div>
                      </li>
                      -->
                      <li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">10</div>
                            <div class="caption">
							<form class="" action="<?php echo base_url(); ?>surat/add_surat_18" method="post">
                               <h4><?php echo $surat[17]['srt_nama'] ?></h4>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <?php if($profil['noijasah']!=null):?>
                                 <button data-toggle="modal" data-target="#" class="btn btn-success ">» Buat Surat</button>
							</form>
                              <?php else:?>
                                 <button class="btn btn-success disabled" disabled>» Belum Lulus</button>
                               <?php endif;?>
                            </div>
                         </div>
                      </li>
                      
					  </div>
					
					
					<?php endif;?>
				  </ul>
               
					</div>
				</div>
		</div>
	</div>
</div>
</div>
</div>

<!-- Kelompok Modal  -->
<!-- Modal Surat 1  -->
<div class="modal fade" id="surat_1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_1/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_1">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat kedua -->
<div class="modal fade" id="surat_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_2/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_2">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat ketiga -->
<div class="modal fade" id="surat_3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_3/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_3">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat keempat -->
<div class="modal fade" id="surat_4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_4/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_4">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat kelima -->
<div class="modal fade" id="surat_5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_5" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_5">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat keenam -->
<div class="modal fade" id="surat_6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_6" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_6">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat Tujuh -->
<div class="modal fade" id="surat_7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_7/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_7">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat Sebelas -->
<div class="modal fade" id="surat_11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_11/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_11">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat dua belas -->
<div class="modal fade" id="surat_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_12/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_12">
			
         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat tiga belas -->
<div class="modal fade" id="surat_13" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_13/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_13">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat lima belas -->
<div class="modal fade" id="surat_15" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_15/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_15">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat enam belas -->
<div class="modal fade" id="surat_16" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_16/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_16">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat delapan belas -->
<div class="modal fade" id="surat_18" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_18/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Input Surat Keputusan Dekan tentang Yudisium Program Sarjana Strata Satu (S.1)</h4>
         </div>
         <div class="modal-body" id="jenis_surat_18">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat dua puluh -->
<div class="modal fade" id="surat_20" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
		<form class="" action="<?php echo base_url(); ?>surat/update_surat_19/1" method="post">
		 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_20">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
