<?php
SESSION_START();

   $nim = $this->session->userdata('nim');
   $sex = $_SESSION['sex'];
   $prodi = $_SESSION['prodi'];
   $nama = $_SESSION['nama'];
   $fakultas = $this->session->userdata('fakultas');
	$kodefak = $this->session->userdata('kodefak');

   function indo($date){
     $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
   	$tahun = substr($date, 0, 4);
   	$bulan = substr($date, 5, 2);
   	$tgl   = substr($date, 8, 2);
   	$jam  = substr($date, 11, 2);
   	$menit  = substr($date, 14, 2);

     $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun." ".$jam.":".$menit;
     return $data;
   }

    ?>

<body style="background:url(<?php echo base_url();?>assets/images/background.png) center">
<div style="min-height:90%;">
<div class="satu" style="background-color:rgb(35, 140, 2);height:1vh;color:#fff;font-size:2vh;font-family: 'OpenSansLight', sans-serif;border-bottom:2px dotted #fff;"></div>
<div class="contentmenu" >
   <div class="container" >
      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="row">
            <div class="menu">
               <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation"><a href="#surat" aria-controls="surat" role="tab" data-toggle="tab"><img title="Tambah Surat" style="width:3vh;margin-right:1vh;;" src="<?php echo base_url();?>assets/images/1.png"/>Tambah Surat</a></li>
                  <li role="presentation" class="active"><a href="#status" aria-controls="status" role="tab" data-toggle="tab"><img title="Status" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/2.png"/>Status</a></li>
                  <li role="presentation"><a href="<?php echo base_url();?>logout"><img title="Status" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/logout.png"/>Keluar</a></li>
               </ul>
            </div>
         </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" style="border-bottom:3px solid #eee; padding-bottom:3vh;padding-top:3vh;padding-left:6vh;background-color:#fff">
         <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
               <div class="row">
                  <div class="tempatNama">
                     <div class="nama1">NIM</div>
                     <div class="nama">: <?php echo $nim; ?></div>
                  </div>
                  <div class="tempatNama">
                     <div class="nama1">Nama </div>
                     <div class="nama">: <?php echo $nama; ?></div>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
               <div class="row">
                  <div class="tempatNama">
                     <div class="nama1">Prodi </div>
                     <div class="nama">: <?php echo $prodi; ?></div>
                  </div>
                  <div class="tempatNama">
                     <div class="nama1">Fakultas </div>
                     <div class="nama">: <?php echo $fakultas; ?></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="content" >
   <div class="container">
      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="row">
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane" id="surat">
				<div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
				  <strong>Informasi!</strong> Jika Surat Belum Tersedia Pada Sistem, Silahkan Menghubungi Bagian Akademik.
				  </br><strong>Pastikan Data Diri Anda (NIM, Nama, Prodi, Fakultas) diatas Muncul Semua</strong>. Lakukan Login Ulang ketika Data diatas Kosong
				</div>
                  <ul class="thumbnails">
					<div class="row">
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">1</div>
                           <div class="caption">
                              <h4><?php echo $surat[0]['srt_nama'] ?> </h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <!-- <a  class="btn btn-success" href="javascript:cek();">» Buat Surat</a> -->
                              <a data-toggle="modal" data-target="#surat_aktif_studi" class="btn btn-success" >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">2</div>
                           <div class="caption">
                              <h4><?php echo $surat[1]['srt_nama'] ?></h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#surat_aktif_studi_besiswa" class="btn btn-success" >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
					 
					 <?php if($fakultas == 'Fak. Sains dan Teknologi' || $fakultas == 'Fak. Dakwah dan Komunikasi' || $fakultas == 'Fak. Adab dan Humaniora'){ ?>
						<li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">3</div>
                            <div class="caption">
                               <h4><?php echo $surat[13]['srt_nama'] ?></h4>
							   <br>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a href="<?php echo base_url();?>home/pesanMagang/14" class="btn btn-success " >» Buat Surat</a>
                            </div>
                         </div>
                      </li>
					<?php } else { ?> 
						 <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">3</div>
                           <div class="caption">
                              <h4><?php echo $surat[2]['srt_nama'] ?></h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#surat_pra_penelitian" class="btn btn-success" >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
					<?php } ?> 

					<?php if($fakultas == 'Fak. Dakwah dan Komunikasi'){ ?>
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">4</div>
                           <div class="caption">
                              <h4><?php echo $surat[3]['srt_nama'] ?></h4>
                              <br>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a href="<?php echo base_url();?>home/pesanMagang/4" class="btn btn-success " >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
					<?php } else { ?> 
						<li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">4</div>
                           <div class="caption">
                              <h4><?php echo $surat[3]['srt_nama'] ?></h4>
                              <br>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#surat_penelitian" class="btn btn-success" >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
					<?php } ?> 
					 
					 
					 </div>
					 <div class="row">
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">5</div>
                           <div class="caption">
                              <h4><?php echo $surat[4]['srt_nama'] ?></h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#surat_cuti_studi" class="btn btn-success" >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">6</div>
                           <div class="caption">
                              <h4><?php echo $surat[5]['srt_nama'] ?></h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#surat_pernyataan_masih_kuliah" class="btn btn-success" >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">7</div>
                           <div class="caption">
                              <h4><?php echo $surat[6]['srt_nama'] ?></h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a href="<?php echo base_url();?>home/pesanMagang/7" class="btn btn-success" href="#">» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">8</div>
                           <div class="caption">
                              <h4><?php echo $surat[7]['srt_nama'] ?></h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a href="<?php echo base_url();?>home/pesanMagang/8" class="btn btn-success" href="#">» Buat Surat</a>
                           </div>
                        </div>
                     </li>
					 </div>
					<div class="row">
					 <?php if($fakultas == 'Fak. Sains dan Teknologi'):?>
							 <li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">9</div>
                            <div class="caption">
                               <h4><?php echo $surat[11]['srt_nama'] ?></h4>
							   <br>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a data-toggle="modal" data-target="#surat_rekomendasi_saintek" class="btn btn-success " >» Buat Surat</a>
                            </div>
                         </div>
                      </li>
						<li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">10</div>
                            <div class="caption">
                               <h4><?php echo $surat[12]['srt_nama'] ?></h4>
							   <br>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a data-toggle="modal" data-target="#surat_uji_laboratorium" class="btn btn-success " >» Buat Surat</a>
                            </div>
                         </div>
                      </li>
						<li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">11</div>
                            <div class="caption">
                               <h4><?php echo $surat[14]['srt_nama'] ?></h4>
							   <br>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a data-toggle="modal" data-target="#surat_permohonan_data" class="btn btn-success " >» Buat Surat</a>
                            </div>
                         </div>
                      </li>
						<li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">12</div>
                            <div class="caption">
                               <h4><?php echo $surat[15]['srt_nama'] ?></h4>
							   <br>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a href="<?php echo base_url();?>home/pesanMagang/16" class="btn btn-success " >» Buat Surat</a>
                            </div>
                         </div>
                      </li>
					</div>
					<?php else: ?>
					<li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">9</div>
                           <div class="caption">
                              <h4><?php echo $surat[10]['srt_nama'] ?></h4>
                              <br>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#surat_rekomendasi" class="btn btn-success " >» Buat Surat</a>
                           </div>
                        </div>
                     </li>
					<?php endif;?>
					
               
					<?php if($prodi == 'Ekonomi Syariah'):?>
					
					<li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">10</div>
                            <div class="caption">
                               <h4><?php echo $surat[8]['srt_nama'] ?></h4>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a href="<?php echo base_url();?>home/pesanMagang/9" class="btn btn-success" href="#">» Buat Surat</a>
                            </div>
                         </div>
                      </li>
                      <li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">11</div>
                            <div class="caption">
                               <h4><?php echo $surat[9]['srt_nama'] ?></h4>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a href="<?php echo base_url();?>home/pesanMagang/10" class="btn btn-success" href="#">» Buat Surat</a>
                            </div>
                         </div>
                      </li>
					  </div>
					
					
					<?php endif;?>

               <?php if($fakultas == 'Fak. Adab dan Humaniora'): ?>
					<li class="col-sm-6 col-md-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">10</div>
                           <div class="caption">
                           <form class="" action="<?php echo base_url(); ?>surat/add_surat_20" method="post">
                              <h4><?php echo $surat[19]['srt_nama'] ?></h4>
                              <br>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <button class="btn btn-success ">» Buat Surat</button>
                           </form>
                           </div>
                        </div>
                     </li>
					<?php endif;?>
					
					<?php if($fakultas == 'Fak. Dakwah dan Komunikasi'):?>
					<!--
					<li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">10</div>
                            <div class="caption">
                               <h4><?php echo $surat[18]['srt_nama'] ?></h4>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <a data-toggle="modal" data-target="#" class="btn btn-success ">» Buat Surat</a>
                            </div>
                         </div>
                      </li>
                      -->
                      <li class="col-sm-6 col-md-3">
                         <div class="fff">
                            <div class="thumbnail">
                               <a href="#"></a>
                            </div>
                            <div class="nomer">10</div>
                            <div class="caption">
							         <form class="" action="<?php echo base_url(); ?>surat/add_surat_18" method="post">
                               <h4><?php echo $surat[17]['srt_nama'] ?></h4>
                               <p>Akademik <?php echo $fakultas; ?></p>
                               <?php if($profil['noijasah']!=null):?>
                                 <button data-toggle="modal" data-target="#" class="btn btn-success ">» Buat Surat</button>
							
                              <?php else:?>
                                 <button class="btn btn-success disabled" disabled>» Belum Lulus</button>
                               <?php endif;?>
                               </form>
                            </div>
                         </div>
                      </li>
					  </div>
					
					
					<?php endif;?>
				  </ul>
               </div>
               <!-- Kumpulan modal  -->
                <!-- Modal Surat ke 1 -->
               <div class="modal fade" id="surat_aktif_studi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>surat/add_surat_1" method="post">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel"><?php echo $surat[0]['srt_nama'] ?></h4>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                             <label for="nama" class="lb">Tanggal Lahir</label>
                             <input type="date" class="form-control" id="tgl_lahir_mhs" name="tgl_lahir_mhs" value="<?php echo $profil['tgllahir']; ?>" />
                          </div>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                             <label for="nama" class="lb">Kota Tempat Lahir</label>
                             <input type="text" class="form-control" id="kota_tmpt_mhs" name="kota_tmpt_mhs" value="<?php echo $profil['tmplahir']; ?>">
                          </div>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                             <label for="nama" class="lb">Alamat Lengkap</label>
                             <textarea type="text" class="form-control" id="surat1_alamat" name="surat1_alamat" style="height:100px;"><?php echo $profil['alamat']; ?></textarea>
                          </div>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                             <label for="nama" class="lb">Keperluan</label>
                             <input type="text" class="form-control " id="surat1_keperluan" name="surat1_keperluan" placeholder="Kehilangan KTM, Perpanjangan BPJS" />
                          </div>
                        </div>
                        <div class="modal-footer">
                           <button class="btn btn-success">» Pesan</button>
                        </div>
                      </form>
                     </div>
                  </div>
               </div>
               <!-- Modal Surat ke 2 -->
               <div class="modal fade" id="surat_aktif_studi_besiswa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>surat/add_surat_2" method="post">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel"><?php echo $surat[1]['srt_nama'] ?></h4>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                             <label for="nama" class="lb">Tanggal Lahir</label>
                             <input type="date" class="form-control" name="tgl_lahir_mhs" value="<?php echo $profil['tgllahir']; ?>" />
                          </div>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                             <label for="nama" class="lb">Kota Tempat Lahir</label>
                             <input type="text" class="form-control" name="kota_tmpt_mhs" value="<?php echo $profil['tmplahir']; ?>">
                          </div>
                        </div>
                        <div class="modal-body" id="modal_foto">
                          <div class="form-group">
                             <label for="nama" class="lb">Alamat Lengkap </label>
                             <textarea type="text" class="form-control" id="surat1_alamat" name="surat1_alamat" style="height:100px;"><?php echo $profil['alamat']; ?></textarea>
                          </div>
                        </div>
                        <div class="modal-footer">
                           <button class="btn btn-success">» Pesan</button>
                        </div>
                      </form>
                     </div>
                  </div>
               </div>
               <!-- Modal Surat ke 3 -->
               <div class="modal fade" id="surat_pra_penelitian" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>surat/add_surat_3" method="post">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Pra Penelitian</h4>
                        </div>
                        <div class="modal-body" id="modal_foto">
                          <div class="form-group">
                            <label for="nama" class="lb">Tujuan Instansi</label>
                            <input type="text" class="form-control " id="srt3_instansi" name="srt3_instansi" placeholder="Contoh : Ketua Koperasi Bina Syariah Ummah" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Alamat Instansi</label>
                            <input type="text" class="form-control " id="srt3_alamat" name="srt3_alamat" placeholder="Contoh : Jl. Deandles, Wadeng Sidayu"/>
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Kota Instansi</label>
                            <input type="text" class="form-control " id="srt3_kabupaten" name="srt3_kabupaten" placeholder="Gresik" />
                          </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Judul Penelitian</label>
                              <textarea type="text" class="form-control" id="srt3_judul" name="srt3_judul"  ></textarea>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Penelitian</label>
                              <input type="date" class="form-control " id="srt3_tgl" name="srt3_tgl" />
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
               <!-- Modal Surat ke 4 -->
               <div class="modal fade" id="surat_penelitian" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>surat/add_surat_4" method="post">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Izin Penelitian</h4>
                        </div>
                        <div class="modal-body" id="modal_foto">
                          <div class="form-group">
                            <label for="nama" class="lb">Tujuan Instansi</label>
                            <input type="text" class="form-control " id="srt3_instansi" name="srt3_instansi" placeholder="Contoh : Ketua Koperasi Bina Syariah Ummah" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Alamat Instansi</label>
                            <input type="text" class="form-control " id="srt3_alamat" name="srt3_alamat" placeholder="Contoh : Jl. Deandles, Wadeng Sidayu"/>
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Kota Instansi</label>
                            <input type="text" class="form-control " id="srt3_kabupaten" name="srt3_kabupaten" placeholder="Gresik" />
                          </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Judul Penelitian</label>
                              <textarea type="text" class="form-control" id="srt3_judul" name="srt3_judul"  ></textarea>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Penelitian</label>
                              <input type="date" class="form-control " id="srt3_tgl" name="srt3_tgl" />
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
               <!-- Modal Surat ke 5 -->
               <div class="modal fade" id="surat_cuti_studi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>surat/add_surat_5" method="post">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Cuti Studi</h4>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                             <label for="nama" class="lb">Tanggal Lahir</label>
                             <input type="date" class="form-control" name="tgl_lahir_mhs" value="<?php echo $profil['tgllahir']; ?>" />
                          </div>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                             <label for="nama" class="lb">Kota Tempat Lahir</label>
                             <input type="text" class="form-control" name="kota_tmpt_mhs" value="<?php echo $profil['tmplahir']; ?>">
                          </div>
                        </div>
                        <div class="modal-body" id="modal_foto">
                          <div class="form-group">
                             <label for="nama" class="lb">Alamat Lengkap </label>
                             <textarea type="text" class="form-control" id="surat5_alamat" name="surat5_alamat" style="height:100px;"><?php echo $profil['alamat']; ?></textarea>
                          </div>
                          <div class="form-group">
                             <label for="nama" class="lb">Alasan Cuti </label>
                             <textarea type="text" class="form-control" id="mhs_alasan" name="mhs_alasan" style="height:100px;"></textarea>
                          </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
               <!-- Modal Surat ke 6 -->
               <div class="modal fade" id="surat_pernyataan_masih_kuliah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>surat/add_surat_6" method="post" enctype="multipart/form-data" >
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Pernyataan Masih Kuliah</h4>
                        </div>
                        <div class="modal-body" id="modal_foto">
                          <div class="form-group">
                            <label for="nama" class="lb">Nama Orang Tua</label>
                            <input type="text" class="form-control " id="srt6_nama_ortu" name="srt6_nama_ortu" placeholder="Contoh : Maksum" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">NIP/NRP</label>
                            <input type="text" class="form-control " id="srt6_nip" name="srt6_nip" placeholder="Contoh : 123456789" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Pangkat/ Gol. Ruang</label>
                            <select class="form-control" name="srt6_pangkat">
                         			<option>Pembina Utama (IV/E)</option>
                         			<option>Pembina Utama Madya (IV/D)</option>
                         			<option>Pembina Utama Muda (IV/C)</option>
                         			<option>Pembina Tingkat I (IV/B)</option>
                         			<option>Pembina (IV/A)</option>
                         			<option>Penata Tingkat I (III/D)</option>
                         			<option>Penata (III/C)</option>
                         			<option>Penata Muda Tingkat I (III/B)</option>
                         			<option>Penata Muda (III/A)</option>
                         			<option>Pengatur Tingkat I (II/D)</option>
                         			<option>Pengatur (II/C)</option>
                         			<option>Pengatur Muda Tingkat I (II/B)</option>
                         			<option>Pengatur Muda (II/A)</option>
                         			<option>Juru Tingkat I (I/D)</option>
                         			<option>Juru (I/C)</option>
                         			<option>Juru Muda Tingkat I (I/B)</option>
                         			<option>Juru Muda (I/A)</option>
                       		 </select>
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Instansi</label>
                            <input type="text" class="form-control " id="srt6_instansi" name="srt6_instansi" placeholder="Contoh : DPRD" />
                          </div>

                          <div class="form-group">
                             <label for="nama" class="lb">Bukti SK Tempat Bekerja</label>
                             <input name="filepen" id="filepen" type="file" accept="application/pdf,application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" required />
                             <a style="color:red">format harus pdf atau word</a>
                          </div>
						  <?php if($fakultas != 'Fak. Sains dan Teknologi'){ ?>
                          <div class="form-group">
                             <label for="nama" class="lb">Bukti Kartu Keluarga (KK)</label>
                             <input name="file_kk" id="file_kk" type="file" accept="application/pdf,application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" required />
                             <a style="color:red">format harus pdf atau word</a>
                          </div>
						  <?php } ?> 
					
                          <div class="form-group">
                             <label for="nama" class="lb">Bukti Kartu Tanda Mahasiswa (KTM)</label>
                             <input name="file_ktm" id="file_ktm" type="file" accept="application/pdf,application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" required />
                             <a style="color:red">format harus pdf atau word</a>
                          </div>
                          <div class="form-group">
                             <label for="nama" class="lb">Tanggal Lahir Mahasiswa</label>
                             <input type="date" class="form-control" name="tgl_lahir_mhs" value="<?php echo $profil['tgllahir']; ?>" />
                          </div>
                          <div class="form-group">
                             <label for="nama" class="lb">Kota Tempat Lahir Mahasiswa</label>
                             <input type="text" class="form-control" name="kota_tmpt_mhs" value="<?php echo $profil['tmplahir']; ?>">
                          </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
               <!-- Modal Surat ke rekom  -->
               <div class="modal fade" id="surat_rekomendasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>surat/add_surat_11" method="post" enctype="multipart/form-data" >
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Rekomendasi</h4>
                        </div>
                        <div class="modal-body" id="modal_foto">
                          <div class="modal-body">
                            <div class="form-group">
                               <label for="nama" class="lb">Tanggal Lahir</label>
                               <input type="date" class="form-control" name="tgl_lahir_mhs" value="<?php echo $profil['tgllahir']; ?>" />
                            </div>
                          </div>
                          <div class="modal-body">
                            <div class="form-group">
                               <label for="nama" class="lb">Kota Tempat Lahir</label>
                               <input type="text" class="form-control" name="kota_tmpt_mhs" value="<?php echo $profil['tmplahir']; ?>">
                            </div>
                          </div>
                          <div class="modal-body">
                            <div class="form-group">
                               <label for="nama" class="lb">Alamat Lengkap </label>
                               <textarea type="text" class="form-control" id="surat1_alamat" name="surat1_alamat" style="height:100px;"><?php echo $profil['alamat']; ?></textarea>
                            </div>
                          </div>

                          <div class="modal-body">
                            <div class="form-group">
                               <label for="nama" class="lb">Keperluan</label>
                               <input type="text" class="form-control " name="surat1_keperluan" placeholder="mengajukan beasiswa Juara Kompas Indonesia" />
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
			   <!-- Modal Surat ke rekom bi -->
               <div class="modal fade" id="surat_rekomendasi_saintek" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>surat/add_surat_12" method="post" enctype="multipart/form-data" >
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Rekomendasi</h4>
                        </div>
                        <div class="modal-body" id="modal_foto">
                          <div class="modal-body">
                            <div class="form-group">
                               <label for="nama" class="lb">Tanggal Lahir</label>
                               <input type="date" class="form-control" name="tgl_lahir_mhs" value="<?php echo $profil['tgllahir']; ?>" />
                            </div>
                          </div>
                          <div class="modal-body">
                            <div class="form-group">
                               <label for="nama" class="lb">Kota Tempat Lahir</label>
                               <input type="text" class="form-control" name="kota_tmpt_mhs" value="<?php echo $profil['tmplahir']; ?>">
                            </div>
                          </div>
                          <div class="modal-body">
                            <div class="form-group">
                               <label for="nama" class="lb">Alamat Lengkap </label>
                               <textarea type="text" class="form-control" id="surat1_alamat" name="surat1_alamat" style="height:100px;"><?php echo $profil['alamat']; ?></textarea>
                            </div>
                          </div>

                          <div class="modal-body">
                            <div class="form-group">
                               <label for="nama" class="lb">Keperluan</label>
                               <input type="text" class="form-control " name="surat1_keperluan" placeholder="mengajukan beasiswa Bank Indonesia" />
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
			   <!-- Modal Surat uji laboratorium -->
			   <div class="modal fade" id="surat_uji_laboratorium" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>surat/add_surat_13" method="post">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Izin Uji Laboratorium</h4>
                        </div>
                        <div class="modal-body" id="modal_foto">
                          <div class="form-group">
                            <label for="nama" class="lb">Tujuan Instansi</label>
                            <input type="text" class="form-control " id="srt13_instansi" name="srt13_instansi" placeholder="Contoh : Ketua Laboratorium Integrasi UIN Sunan Ampel Surabaya" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Alamat Instansi</label>
                            <input type="text" class="form-control " id="srt13_alamat" name="srt13_alamat" placeholder="Contoh : Jl. Ahmad Yani 117"/>
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Kota Instansi</label>
                            <input type="text" class="form-control " id="srt13_kabupaten" name="srt13_kabupaten" placeholder="Contoh : Surabaya" />
                          </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tujuan Laboratorium</label>
                              <input type="text" class="form-control" id="srt13_tujuan" name="srt13_tujuan" placeholder="Contoh : Laboratorium Kimia"  ></input>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Keperluan</label>
                              <input type="text" class="form-control" id="srt13_keperluan" name="srt13_keperluan" placeholder="Contoh : Penyusunan Penelitian Skripsi" ></input>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Mata Kuliah</label>
                              <input type="text" class="form-control" id="srt13_matakuliah" name="srt13_matakuliah" placeholder="Contoh : Skripsi, Biologi Molekuler" ></input>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Judul Penelitian</label>
                              <textarea type="text" class="form-control" id="srt13_judul" name="srt13_judul" ></textarea>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Mulai</label>
                              <input type="date" class="form-control " id="srt13_tglawal" name="srt13_tglawal" />
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Akhir</label>
                              <input type="date" class="form-control " id="srt13_tglakhir" name="srt13_tglakhir" />
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
			   <!-- Modal Surat Permohonan Data -->
			   <div class="modal fade" id="surat_permohonan_data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>surat/add_surat_15" method="post">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Permohonan Data</h4>
                        </div>
                        <div class="modal-body" id="modal_foto">
                          <div class="form-group">
                            <label for="nama" class="lb">Tujuan Instansi</label>
                            <input type="text" class="form-control " id="srt15_instansi" name="srt15_instansi" placeholder="Contoh : Manajer PT. XYZ" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Alamat Instansi</label>
                            <input type="text" class="form-control " id="srt15_alamat" name="srt15_alamat" placeholder="Contoh : Jl. Ahmad Yani 117"/>
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Kota Instansi</label>
                            <input type="text" class="form-control " id="srt15_kabupaten" name="srt15_kabupaten" placeholder="Contoh : Surabaya" />
                          </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Nama Tempat Pengambilan Data</label>
                              <input type="text" class="form-control" id="srt15_tujuan" name="srt15_tujuan" placeholder="Contoh : PT. XYZ"  ></input>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Mata Kuliah</label>
                              <input type="text" class="form-control" id="srt15_matakuliah" name="srt15_matakuliah" placeholder="Contoh : Skripsi, Biologi Molekuler" ></input>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Data Yang Dibutuhkan</label>
                              <input type="text" class="form-control" id="srt15_data" name="srt15_data" placeholder="Contoh : Sensus Penduduk (Nama, tanggal lahir, jenis kelamin)" ></input>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Judul Penelitian</label>
                              <textarea type="text" class="form-control" id="srt15_judul" name="srt15_judul" ></textarea>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Mulai</label>
                              <input type="date" class="form-control " id="srt15_tglawal" name="srt15_tglawal" />
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Akhir</label>
                              <input type="date" class="form-control " id="srt15_tglakhir" name="srt15_tglakhir" />
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
			   <!-- Modal Surat Bakesbangpol 
			   <div class="modal fade" id="surat_bakesbangpol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>surat/add_surat_16" method="post">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Izin Bakesbangpol</h4>
                        </div>
                        <div class="modal-body" id="modal_foto">
                          <div class="form-group">
                            <label for="nama" class="lb">Tujuan Instansi</label>
                            <input type="text" class="form-control " id="srt16_instansi" name="srt16_instansi" placeholder="Contoh : Kepala Badan Kesatuan Bangsa dan Politik (Bakesbangpol)" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Alamat Instansi</label>
                            <input type="text" class="form-control " id="srt16_alamat" name="srt16_alamat" placeholder="Contoh : Jl. Putat Indah No.1 Putat Gede"/>
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Kota Instansi</label>
                            <input type="text" class="form-control " id="srt16_kabupaten" name="srt16_kabupaten" placeholder="Contoh : Surabaya" />
                          </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Nama Tempat Penelitian</label>
                              <input type="text" class="form-control" id="srt16_tujuan" name="srt16_tujuan" placeholder="Contoh : Kantor Dinas Kependudukan dan Pencatatan Sipil Sidoarjo"  ></input>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Keperluan Izin</label>
                              <input type="text" class="form-control" id="srt16_data" name="srt16_data" placeholder="Contoh : Kuliah Kerja Lapangan (KKL) " ></input>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Mulai</label>
                              <input type="date" class="form-control " id="srt16_tglawal" name="srt16_tglawal" />
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Akhir</label>
                              <input type="date" class="form-control " id="srt16_tglakhir" name="srt16_tglakhir" />
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
			   -->
			   
               <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Keterangan Aktif Mahasiswa</h4>
                        </div>
                        <div class="modal-body" id="modal_foto">
                           <div class="form-group">
                              <label for="nama" class="lb">Keterangan</label>
                              <textarea type="text" class="form-control" id="usr" value="Keperluan Surat" ></textarea>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Batas Tanggal</label>
                              <input type="date" class="form-control " id="tgl" name="tgl" />
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button class="btn btn-success">» Kirim</button>
                        </div>
                     </div>
                  </div>
               </div>
               <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/select/bootstrap-select.css">
               <script src="<?php echo base_url(); ?>assets/js/select/bootstrap-select.js" defer></script>
               <div class="modal fade" id="magang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Form</h4>
                        </div>
                        <form class="" action="<?php echo base_url(); ?>prin/addMagang" method="post">
                        <div class="modal-body" >
                          <div class="form-group">
                            <label for="nama" class="lb">Nama Instansi</label>
                            <input type="text" class="form-control " id="instansi" name="instansi" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Nama Jalan</label>
                            <input type="text" class="form-control " id="alamat" name="alamat" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">No Instansi</label>
                            <input type="number" class="form-control " id="no_instansi" name="no_instansi" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Provinsi</label>
                            <select onchange="javascript:ganti_kab();" name="provinsi" id="basic" class="selectpicker show-tick form-control" data-live-search="true">
                              <?php
                              foreach ($provinsi as $Hprovinsi) { ?>
                                <option value="<?php print_r ($Hprovinsi['id']); ?>"><?php print_r ($Hprovinsi['name']); ?></option>
                              <?php
                              }
                              ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="nama" id="sapi" class="lb">Kabupaten</label>
                            <select id="kabupaten" name="kabupaten" class="show-tick form-control" >
                                <option value="0">- Pilih Provinsi -</option>
                            </select>
                          </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Mulai</label>
                              <input type="date" class="form-control " id="mulai_tgl" name="mulai_tgl" />
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Akhir</label>
                              <input type="date" class="form-control " id="tgl_akhir" name="tgl_akhir" />
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Jenis Magang</label>
							  <br/>
                              <label><input type="radio" onclick="javascript:add();" name="jenis_magang" id="magang_individu" value="">individu</label>
                              <label><input type="radio" onclick="javascript:add();" name="jenis_magang" id="magang_kelompok" value="">Kelompok</label>
                           </div>
						               <div id="jumlah_mhs_magang">

                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                      </form>
                     </div>
                  </div>
               </div>
               <div class="modal fade" id="penelitian" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>prin/addPenelitian" method="post">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Pra Penelitian</h4>
                        </div>
                        <div class="modal-body" id="modal_foto">
                          <div class="form-group">
                            <label for="nama" class="lb">Nama Instansi</label>
                            <input type="text" class="form-control " id="instansi" name="instansi" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Alamat Instansi</label>
                            <input type="text" class="form-control " id="alamat" name="alamat" />
                          </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Judul Penelitian</label>
                              <textarea type="text" class="form-control" id="judul" name="judul"  ></textarea>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Penelitian</label>
                              <input type="date" class="form-control " id="tgl" name="tgl" />
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
               <div role="tabpanel" class="tab-pane active" id="status">
                  <div class="panel-body" style="padding-bottom:4vw;">
                     <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example1" style="background-color:#fff">
                           <thead>
                              <tr>
                                 <th style="text-align:center;">No</th>
                                 <th style="text-align:center;">Nama Surat</th>
                                 <th style="text-align:center;">Tanggal Pesan</th>
                                 <th style="text-align:center;">Status</th>
                                 <th style="text-align:center;">Action</th>
                                 <?php 
                                 if($kodefak == "D"){ ?>
                                 <th style="text-align:center;">Print</th>
                                 <?php
                                 }
                                 ?>
                              </tr>
                           </thead>
                           <tbody>
                              <?php
                              $i=1;
                              foreach ($antria as $Hall){ ?>
                              <tr>
                                 <td style="text-align:center;"><?php echo $i; ?></td>
                                 <td style="text-align:center;"><?php echo $Hall['srt_nama'];?></td>
                                 <td style="text-align:center;"><?php
                                 $tgl = $Hall['tgl_submit'];
                                 echo $tgl = indo($tgl);
                                 ?></td>
                                 <td style="text-align:center;">
                                 <?php
                                    if($Hall['status'] == "1"){ ?>
                                 <a class="btn-xs btn btn-warning">Antri</a>
                                 <?php
                               }else if($Hall['status'] == "2"){ ?>
                                 <a class="btn-xs btn btn-primary" >Menunggu</a>
                                 <?php
                               }else if($Hall['status'] == "88"){ ?>
                                  <a class="btn-xs btn btn-danger" >Persetujuan Kaprodi</a>
                                 <?php
                               }else if($Hall['status'] == "96"){ ?>
                                  <a class="btn-xs btn btn-info" data-toggle="modal" href="#static" >Revisi <span class="badge badge-danger"> 1 </span></a>
                                 <?php
                               }else { ?>
                                  <a class="btn-xs btn btn-success" >Bisa diambil</a>
                                 <?php
                               }
                                    ?>
                                 </td>
                                 <td>
                                   <?php
                                   if($Hall['status'] == "1" || $Hall['status'] == "88"){ ?>
                                     <a class="btn-xs btn btn-danger" href="<?php echo base_url();?>home/delete/<?php echo $Hall['id'];?>" >Delete</a>
                                  <?php  }else if($Hall['status'] == "96"){ ?>
                                     <button class="btn-xs btn btn-warning" data-toggle="modal" href="#edit" >Edit</button>
									 <a class="btn-xs btn btn-danger" href="<?php echo base_url();?>home/delete/<?php echo $Hall['id'];?>" >Delete</a>
                                   <?php }else { ?>
                                     <button class="btn-xs btn btn-danger" disabled >Delete</button>
                                   <?php }
                                    ?>
                                 </td>
                                 <?php 
                                 if($kodefak == "D"){ ?>
                                 <td style="text-align:center;">
                                    <form class="" action="<?php echo base_url(); ?>prin" method="post">
                                       <input type="hidden" name="id_surat" value="<?php echo $Hall['id']; ?>" >
                                       <input type="hidden" name="kode_surat" value="<?php echo $Hall['kode_surat']; ?>" >
                                       <input type="hidden" name="nim" value="<?php echo $Hall['nim']; ?>" >
                                       <button type="submit" class="btn btn-default">Print</button>
                                    </form>
                                 </td>
                                 <?php
                                 }
                                 ?>
                                 
                              </tr>
                              <?php
                              $i++;
                                 }
                                 ?>
                           </tbody>
                        </table>
                        <!--/.table-responsive -->
                     </div>
                  </div>
				<div class="col-xs-12 col-sm-12 col-md-12" style="border-top:1px solid rgb(67, 181, 31);width:100%; padding-bottom:3vh; padding-top:3vh;">
					<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-2">
						<i style="color:#000;margin-left:2vh;">Keterangan Status :</i></span>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2">
						<span class="glyphicon glyphicon-stop" aria-hidden="true" style="color:orange"></span><i style="color:#000;margin-left:1vh;">Antri (Surat Belum di Print)</i>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2">
						<span class="glyphicon glyphicon-stop" aria-hidden="true" style="color:#337ab7"></span><i style="color:#000;margin-left:1vh;">Menunggu (Surat Belum di Tanda tangani)</i>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2">
						<span class="glyphicon glyphicon-stop" aria-hidden="true" style="color:#5cb85c"></span><i style="color:#000;margin-left:1vh;">Bisa diambil<i>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2">
						<span class="glyphicon glyphicon-stop" aria-hidden="true" style="color:#d9534f"></span><i style="color:#000;margin-left:1vh;">Persetujuan Kaprodi (Surat Masih Belum disetujui Kaprodi)<i>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2">
						<span class="glyphicon glyphicon-stop" aria-hidden="true" style="color:#5bc0de"></span><i style="color:#000;margin-left:1vh;">Revisi (Terdapat Catatan Dari Kaprodi)<i>
					</div>
					</div>
				</div>
                </div>
               <!--<div role="tabpanel" class="tab-pane" id="profil">
                  <div class="">
                  </div>
               </div>-->
            </div>
         </div>
      </div>
   </div>
</div>

<!-- /.modal Komen -->
<div id="static2" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" style="text-align:center;color:green">Konfirmasi Revisi</h4>
				<hr>
				<div class="form-group">
                    <div class="col-md-12">
                        <ul class="media-list">
                            <li class="media">
                                <a class="pull-left" href="javascript:;">
                                    <img class="todo-userpic" src="<?php echo base_url();?>/assets/images/user1.jpg" width="27px" height="27px"> </a>
                                <div class="media-body todo-comment">
                                    <p class="todo-comment-head">
										<strong><span class="todo-comment-username">Christina Aguilera</span></strong>
										<small><span class="todo-comment-date">17 Sep 2014 at 2:05pm</span></small>
                                    </p>
                                    <p class="todo-text-color"> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
                                    </p>
                                    <!-- Nested media object -->
                                    <div class="media">
                                        <a class="pull-left" href="javascript:;">
                                            <img class="todo-userpic" src="<?php echo base_url();?>/assets/images/user2.jpg" width="27px" height="27px"> </a>
                                        <div class="media-body">
                                            <p class="todo-comment-head">
										<strong><span class="todo-comment-username">Carles Puyol</span></strong>
										<small><span class="todo-comment-date">17 Sep 2014 at 2:05pm</span></small>
                                            </p>
                                        <p class="todo-text-color"> Thanks so much my dear! </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-md-12">
                        <ul class="media-list">
                            <li class="media">
                                <a class="pull-left" href="javascript:;">
                                    <img class="todo-userpic" src="<?php echo base_url();?>/assets/images/user2.jpg" width="27px" height="27px"> </a>
                                <div class="media-body">
                                    <textarea class="form-control todo-taskbody-taskdesc" rows="4" placeholder="Sudah Saya Revisi Pak/Bu..."></textarea>
                                </div>
                            </li>
                        </ul>
                        <button type="button" class="pull-right btn btn-sm btn-circle btn-success"> &nbsp; Submit &nbsp; </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
									
    <!-- /.modal Revisi -->
	<div class="modal fade draggable-modal" id="static" tabindex="-1" role="static" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" style="text-align:center;color:green">Catatan Revisi<small><strong> Segera Edit Surat Anda</strong></small></h4>
                </div>
                <div class="modal-body"> Modal body goes here </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- /.modal Edit -->
	<div class="modal fade draggable-modal" id="edit" tabindex="-1" role="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" style="text-align:center;color:green">TESTING MODAL</h4>
                </div>
                <div class="modal-body"> Modal body goes here </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success " data-dismiss="modal" data-toggle="modal" href="#static2">Kirim</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
	
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form class="" action="<?php echo base_url();?>prin/addSBea" method="post" enctype="multipart/form-data">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:1.5vw; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Rekomendasi</h4>
         </div>
         <div class="modal-body" id="modal_foto">
            <div class="form-group">
               <label for="nama" class="lb">File Pendukung</label>
               <input name="filepen" id="filepen" type="file" accept="application/pdf,application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" required />
               <a style="color:red">format harus pdf atau word<a>
            </div>
         </div>
         <div class="modal-body" id="modal_foto">
            <div class="form-group">
               <label for="nama" class="lb">Keterangan</label>
               <textarea type="text" class="form-control" id="ket_beasiswa" name="ket_beasiswa" required ></textarea>
            </div>
         </div>
         <div class="modal-footer" id="modal_foto" style="text-align:left">
            <div class="form-group">
               <label for="nama" class="lb">Contoh</label>
               <img src="<?php echo base_url();?>assets/images/1.jpg"/>
			   <div class="well well-sm">Lebih jelas silahkan klik link berikut <a href="<?php echo base_url();?>assets/images/ket2.jpg" target="_blank" >lihat contoh</a></div>
            </div>
         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Kirim</button>
         </div>
         </form>
      </div>
   </div>
</div>
</div>
