<?php
$fakultascilik1 = str_replace("Fak.","Fakultas",$fakultas);
$fakultascilik2 = $fakultascilik1;
$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);
//echo $fakultas;

if($nobulan >= 3 && $nobulan < 8){
  $tahunajaran = ($tahun-1)."/".$tahun;
	$sem = "Genap";
}else {
  $tahunajaran = ($tahun)."/".($tahun+1);
	$sem = "Gasal";
}
$tgl = indo($pengajuan[0]['tgl_penelitian']);
$tgl1 = indo($pengajuan[0]['tgl_akhir']);

$tanggalan = indo(date("Y-m-d"));

if ($pengajuan[0]['dekanat'] == 0 || $pengajuan[0]['dekanat'] == "" ) {
    $ttd =  "<br/>Dekan,";
    $kodedekan = "D";
  	$dekanat = $dekan['nama'];
  	$nip_dekanat = $dekan['nip'];
}else if ($pengajuan[0]['dekanat'] == 1) {
    $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Akademik dan Kelembagaan";
    $kodedekan = "D1";
    $dekanat = $wadek1['nama'];
    $nip_dekanat = $wadek1['nip'];
}else if ($pengajuan[0]['dekanat'] == 2) {
    $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Administrasi Umum, Perencanaan dan Keuangan";
    $kodedekan = "D2";
  	$dekanat = $wadek2['nama'];
  	$nip_dekanat = $wadek2['nip'];
}else if ($pengajuan[0]['dekanat'] == 3) {
    $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Kemahasiswaan dan Kerjasama";
    $kodedekan = "D3";
  	$dekanat = $wadek3['nama'];
  	$nip_dekanat = $wadek3['nip'];
}else if($pengajuan[0]['dekanat'] == 4){
    $ttd =  "<br/>a.n. Dekan,<br/>Kepala Bagian Tata Usaha";
    $kodedekan = "TU";
  	$dekanat = $tu['nama'];
  	$nip_dekanat = $tu['nip'];
}else {
    $ttd =  "<br/>Dekan,<br/>";
    $kodedekan = "D";
    $dekanat = $dekan['nama'];
    $nip_dekanat = $dekan['nip'];
}

function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);

  $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun;
  return $data;
}
 ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		 // window.print();

	});
</script>
<style type="text/css" media="print">
@page
    {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html
    {
        background-color: #FFFFFF;
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }

    body
    {
        margin: 5mm 15mm 10mm 15mm; /* margin you want for the content */
    }
</style>
<html>

	<head><title>Permohonan Izin Penelitian atas nama <?php echo $nama;?></title></head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
  <font face="arial" size="11">
	<body>

<table width="650px;" style="margin:auto; margin-top:20px;">
	<tr>
		<td>
      <div id="header">
          <div id="gambar"><img style="height:10%; width:80%;" src="<?php echo base_url(); ?>assets/images/uin.png"/></div>
          <div id="tulisan_uin"><strong>KEMENTERIAN AGAMA REPUBLIK INDONESIA</strong><br/><span style="font-size:0.85em">
            <strong>UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA</strong></span>
        <br/><span style="font-size:0.85em"> <strong><?php echo $fakultas; ?></strong> </span>
        <br/>
          <p style=" margin-top: 10px;font-size:0.6em">
          Jl. A. Yani 117 Surabaya 60237 Telp. 031-8410298 Fax. 031-8413300 email : <u><?php echo $singkatan; ?>@uinsby.ac.id</u>
        </p></div>
        </div>

        <div id="garis"></div>
        <br/>
	<div id="satu">
        <table width="630px;" style="margin:auto; font-size:0.9em;">
          <tr>
            <td>

              <table style="font-size:0.9em;">
                <tr>
                  <td>Nomor</td>
                  <td>:</td>
                  <td>B -&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/Un.07/<?php echo $nomor; ?>/<?php echo $kodedekan; ?>/PP.00.9/<?php echo $nobulan."/".$tahun;?></td>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td>&emsp;&emsp;
                    Surabaya, <?php echo $tanggalan ;?></td>
                </tr>
                <tr>
                  <td>Lampiran</td>
                  <td>:</td>
                  <td> -</td>
                </tr>
                <tr>
                  <td>Perihal</td>
                  <td>:</td>
                  <td><strong> Izin Uji Laboratorium</strong></td>
                </tr>
                <tr><td>
				</br>
				</br>
				</td></tr>

              </table>
                <table style="font-size:0.95em;margin-left:4.5em ">

                    <tr>
                      <td></td>
                      <td><div id="">Kepada Yth,</div></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><div id=""><strong><?php print_r ($pengajuan[0]['ket_instansi']);  ?></strong>
                        <br>
                        <div style="width:75%;"><?php print_r ($pengajuan[0]['alamat_instansi']);  ?> </div>
                      </div></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Di <?php print_r ($pengajuan[0]['kab_penelitian']);  ?></td>
                    </tr>
                    <!--<tr>
                      <td></td>
                      <td>&emsp;<?php print_r ($pengajuan[0]['kab_penelitian']);  ?></td>
                    </tr>-->
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr>
                      <td></td>
                      <td>
                        <br>
                        <br>
                        <div id=""><i><strong>Assalamu'alaikum Wr. Wb.</strong></i></div><br></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><div id="" style="text-align:justify;line-height:23px;">
                          Sehubungan dengan tugas mata kuliah <?php print_r ($pengajuan[0]['srt7_matkul']); ?>, Dengan hormat mahasiswa <?php echo $fakultascilik2; ?>
                          Universitas Islam Negeri Sunan Ampel Surabaya tersebut di bawah ini:</div>
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>
                        <table style="font-size:0.95em; line-height:18px;">
                  				<tr>
                            <td>&emsp;&emsp;&ensp;</td>
                  					<td>Nama</td>
                  					<td></td>
                  					<td>:</td>
                  					<td> <strong><?php echo ucwords(strtolower($nama));?></strong> </td>
                  				</tr>
                  				<tr>
                  					<td></td>
                  					<td>NIM</td>
                  					<td></td>
                  					<td>:</td>
                  					<td><?php echo $nim; ?> </td>
                  				</tr>
                  				<tr>
                  					<td></td>
                  					<td>Semester/Prodi</td>
                  					<td></td>
                  					<td>:</td>
                  					<td><?php echo $semestermhs."/".$prodi;?></td>
                  				</tr>
                  			</table>
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>
                        <div style="text-align:justify; line-height:23px;">
                          Mohon diijinkan untuk Uji Laboratotium pada <i> <?php print_r ($pengajuan[0]['laboratorium']); ?> </i> yang akan digunakan sebagai <?php print_r ($pengajuan[0]['srt1_keperluan']); ?> berjudul:<i> <?php print_r ($pengajuan[0]['judul_penelitian']); ?> </i>yang akan dilaksanakan pada tanggal <?php print_r ($tgl); ?> sampai tanggal <?php print_r ($tgl1); ?> sebagaimana proposal terlampir. 
                    	</div>
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td style="line-height:23px; text-align:justify;">
                          Demikian permohonan izin ini, dan atas kerjasamanya kami sampaikan terimakasih.</td>
                    </tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr>
                      <td></td>
                      <td>
                        <p>
                  				<i><strong>Wassalamu'alaikum Wr. Wb.</strong></i>
                  			</p>
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                    </tr>
                </table>


              		<br/>
              		<br/>
              		<div>
                    <div id="id">
                      <strong><?php echo $ttd; ?>
                		<br/>
                		<br/>
                		<br/>
                		<br/>
                		<br/>
                		<?php echo $dekanat; ?><br/>
                		NIP. <?php echo $nip_dekanat; ?>
					</strong>

                		</div>
					</div>
              		</div>

            </td>
          </tr>
        </table>

	</td>
	</tr>
</table>



	</body>
</font>
</html>
