<?php
// print_r ($data);
if($data['kode_surat'] == 1){ ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<input type="hidden" name="nim_mhs" id="nim_mhs" value="<?php print_r ($data['nim']);?>" />
	<div class="form-group">
	  <label for="nama" class="lb">Tanggal Lahir Mahasiswa</label>
	  <input type="date" class="form-control" name="tgl_lahir_mhs" value="<?php echo $data_siakad['tgllahir']; ?>" >
	</div>
	<div class="form-group">
	  <label for="nama" class="lb">Kota Tempat Lahir Mahasiswa</label>
	  <input type="text" class="form-control" name="kota_tmpt_mhs" value="<?php echo $data_siakad['tmplahir']; ?>" >
	</div>
	<div class="form-group">
	  <label for="nama" class="lb">Alamat Mahasiswa</label>
	  <textarea type="text" class="form-control" name="mhs_alamat" style="height:100px;" ><?php print_r ($data['mhs_alamat']);?></textarea>
	</div>
	<div class="form-group">
	  <label for="nama" class="lb">Keperluan Mahasiswa</label>
	  <textarea type="text" class="form-control" name="surat1_keperluan" style="height:100px;" ><?php print_r ($data['srt1_keperluan']);?></textarea>
	</div>
	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>

<?php
}else if ($data['kode_surat'] == 2) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<input type="hidden" name="nim_mhs" id="nim_mhs" value="<?php print_r ($data['nim']);?>" />
	<div class="form-group">
	  <label for="nama" class="lb">Tanggal Lahir Mahasiswa</label>
	  <input type="date" class="form-control" name="tgl_lahir_mhs" value="<?php echo $data_siakad['tgllahir']; ?>" >
	</div>
	<div class="form-group">
	  <label for="nama" class="lb">Kota Tempat Lahir Mahasiswa</label>
	  <input type="text" class="form-control" name="kota_tmpt_mhs" value="<?php echo $data_siakad['tmplahir']; ?>" >
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Alamat Mahasiswa</label>
		<textarea type="text" class="form-control" name="mhs_alamat" style="height:100px;" ><?php print_r ($data['mhs_alamat']);?></textarea>
	</div>
	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>

<?php
}else if ($data['kode_surat'] == 3) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<div class="form-group">
		<label for="nama" class="lb">Tujuan Instansi</label>
		<input type="text" class="form-control" value="<?php echo $data['ket_instansi'];?>" id="srt3_instansi" name="srt3_instansi" placeholder="Contoh : Ketua Koperasi Bina Syariah Ummah" />
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Alamat Instansi</label>
		<input type="text" class="form-control " value="<?php echo $data['alamat_instansi'];?>" id="srt3_alamat" name="srt3_alamat" placeholder="Contoh : Jl. Deandles, Wadeng Sidayu"/>
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Kota Instansi</label>
		<input type="text" class="form-control " value="<?php echo $data['kab_penelitian'];?>" id="srt3_kabupaten" name="srt3_kabupaten" placeholder="Gresik" />
	</div>
	 <div class="form-group">
			<label for="nama" class="lb">Judul Penelitian</label>
			<textarea type="text" class="form-control" id="srt3_judul" name="srt3_judul"  ><?php echo $data['judul_penelitian'];?></textarea>
	 </div>
	 <div class="form-group">
			<label for="nama" class="lb">Tanggal Penelitian</label>
			<input type="date" class="form-control" value="<?php echo $data['tgl_penelitian'];?>" id="srt3_tgl" name="srt3_tgl" />
	 </div>
 	<div class="form-group">
 		 <label for="nama" class="lb">Keterangan</label>
 		 <select class="form-control" name="dekanat">
 			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
 			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
 			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
 			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
 		 </select>
 	</div>
<?php
}else if ($data['kode_surat'] == 4) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<div class="form-group">
		<label for="nama" class="lb">Tujuan Instansi</label>
		<input type="text" class="form-control" value="<?php echo $data['ket_instansi'];?>" id="srt3_instansi" name="srt3_instansi" placeholder="Contoh : Ketua Koperasi Bina Syariah Ummah" />
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Alamat Instansi</label>
		<input type="text" class="form-control " value="<?php echo $data['alamat_instansi'];?>" id="srt3_alamat" name="srt3_alamat" placeholder="Contoh : Jl. Deandles, Wadeng Sidayu"/>
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Kota Instansi</label>
		<input type="text" class="form-control " value="<?php echo $data['kab_penelitian'];?>" id="srt3_kabupaten" name="srt3_kabupaten" placeholder="Gresik" />
	</div>
	 <div class="form-group">
		<label for="nama" class="lb">Judul Penelitian</label>
		<textarea type="text" class="form-control" id="srt3_judul" name="srt3_judul"  ><?php echo $data['judul_penelitian'];?></textarea>
	 </div>
	 <div class="form-group">
		<label for="nama" class="lb">Tanggal Penelitian</label>
		<input type="date" class="form-control" value="<?php echo $data['tgl_penelitian'];?>" id="srt3_tgl" name="srt3_tgl" />
	 </div>
 	<div class="form-group">
 		 <label for="nama" class="lb">Keterangan</label>
 		 <select class="form-control" name="dekanat">
 			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
 			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
 			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
 			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
 		 </select>
 	</div>
<?php
}else if ($data['kode_surat'] == 5) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<input type="hidden" name="nim_mhs" id="nim_mhs" value="<?php print_r ($data['nim']);?>" />
	<div class="form-group">
	  <label for="nama" class="lb">Tanggal Lahir Mahasiswa</label>
	  <input type="date" class="form-control" name="tgl_lahir_mhs" value="<?php echo $data_siakad['tgllahir']; ?>" >
	</div>
	<div class="form-group">
	  <label for="nama" class="lb">Kota Tempat Lahir Mahasiswa</label>
	  <input type="text" class="form-control" name="kota_tmpt_mhs" value="<?php echo $data_siakad['tmplahir']; ?>" >
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Alamat Mahasiswa</label>
		<textarea type="text" class="form-control" name="mhs_alamat" style="height:100px;" ><?php print_r ($data['mhs_alamat']);?></textarea>
	</div>
	<div class="form-group">
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
		<label for="nama" class="lb">Alasan Cuti Bagi Mahasiswa</label>
		<textarea type="text" class="form-control" name="mhs_alasan" style="height:100px;" ><?php print_r ($data['mhs_alasan']);?></textarea>
	</div>
	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>

<?php
}else if ($data['kode_surat'] == 6) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<div class="form-group">
		<div class="form-group">
			<label for="nama" class="lb">Nama Orang Tua</label>
			<input type="text" class="form-control " value="<?php print_r ($data['srt6_nama_ortu']);?>" id="srt6_nama_ortu" name="srt6_nama_ortu" placeholder="Contoh : Maksum" />
		</div>
		<div class="form-group">
			<label for="nama" class="lb">NIP/NRP</label>
			<input type="text" class="form-control " value="<?php print_r ($data['srt6_nip']);?>" id="srt6_nip" name="srt6_nip" placeholder="Contoh : 123456789" />
		</div>
		<div class="form-group">
			<label for="nama" class="lb">Pangkat/ Gol. Ruang</label>
			<select class="form-control" name="srt6_pangkat">
				<option <?php if($data['srt6_pangkat'] == "Pembina Utama (IV/e)"){ echo "selected";} ?> >Pembina Utama (IV/e)</option>
				<option <?php if($data['srt6_pangkat'] == "Pembina Utama Madya (IV/d)"){ echo "selected";} ?> >Pembina Utama Madya (IV/d)</option>
				<option <?php if($data['srt6_pangkat'] == "Pembina Utama Muda (IV/c)"){ echo "selected";} ?> >Pembina Utama Muda (IV/c)</option>
				<option <?php if($data['srt6_pangkat'] == "Pembina Tingkat I (IV/b)"){ echo "selected";} ?> >Pembina Tingkat I (IV/b)</option>
				<option <?php if($data['srt6_pangkat'] == "Pembina (IV/a)"){ echo "selected";} ?> >Pembina (IV/a)</option>
				<option <?php if($data['srt6_pangkat'] == "Penata Tingkat I (III/d)"){ echo "selected";} ?> >Penata Tingkat I (III/d)</option>
				<option <?php if($data['srt6_pangkat'] == "Penata (III/c)"){ echo "selected";} ?> >Penata (III/c)</option>
				<option <?php if($data['srt6_pangkat'] == "Penata Muda Tingkat I (III/b)"){ echo "selected";} ?> >Penata Muda Tingkat I (III/b)</option>
				<option <?php if($data['srt6_pangkat'] == "Penata Muda (III/a)"){ echo "selected";} ?> >Penata Muda (III/a)</option>
				<option <?php if($data['srt6_pangkat'] == "Pengatur Tingkat I (II/d)"){ echo "selected";} ?> >Pengatur Tingkat I (II/d)</option>
				<option <?php if($data['srt6_pangkat'] == "Pengatur (II/c)"){ echo "selected";} ?> >Pengatur (II/c)</option>
				<option <?php if($data['srt6_pangkat'] == "Pengatur Muda Tingkat I (II/b)"){ echo "selected";} ?> >Pengatur Muda Tingkat I (II/b)</option>
				<option <?php if($data['srt6_pangkat'] == "Pengatur Muda (II/a)"){ echo "selected";} ?> >Pengatur Muda (II/a)</option>
				<option <?php if($data['srt6_pangkat'] == "Juru Tingkat I (I/d)"){ echo "selected";} ?> >Juru Tingkat I (I/d)</option>
				<option <?php if($data['srt6_pangkat'] == "Juru (I/c)"){ echo "selected";} ?> >Juru (I/c)</option>
				<option <?php if($data['srt6_pangkat'] == "Juru Muda Tingkat I (I/b)"){ echo "selected";} ?> >Juru Muda Tingkat I (I/b)</option>
				<option <?php if($data['srt6_pangkat'] == "Juru Muda (I/a)"){ echo "selected";} ?> >Juru Muda (I/a)</option>
		 </select>
		</div>
		<div class="form-group">
			<label for="nama" class="lb">Instansi</label>
			<input type="text" class="form-control " value="<?php print_r ($data['srt6_instansi']);?>" id="srt6_instansi" name="srt6_instansi" placeholder="Contoh : DPRD" />
		</div>
		<input type="hidden" name="nim_mhs" id="nim_mhs" value="<?php print_r ($data['nim']);?>" />
		<div class="form-group">
		  <label for="nama" class="lb">Tanggal Lahir Mahasiswa</label>
		  <input type="date" class="form-control" name="tgl_lahir_mhs" value="<?php echo $data_siakad['tgllahir']; ?>" >
		</div>
		<div class="form-group">
		  <label for="nama" class="lb">Kota Tempat Lahir Mahasiswa</label>
		  <input type="text" class="form-control" name="kota_tmpt_mhs" value="<?php echo $data_siakad['tmplahir']; ?>" >
		</div>
	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>

<?php
}else if ($data['kode_surat'] == 7) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<div class="form-group">
		<label for="nama" class="lb">Tujuan Instansi</label>
		<input type="text" class="form-control " value="<?php print_r ($data['ket_instansi']); ?>" id="instansi" name="instansi" required  />
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Alamat instansi</label>
		<input type="text" class="form-control " value="<?php print_r ($data['alamat_instansi']); ?>" id="alamat" name="alamat" required />
	</div>

	 <div class="form-group">
			<label for="nama" class="lb">Mata Kuliah </label>
			<input type="text" class="form-control " value="<?php print_r ($data['srt7_matkul']); ?>" id="srt7_matkul" name="srt7_matkul" required />
	 </div>
	 <div class="form-group">
			<label for="nama" class="lb">Tanggal Mulai</label>
			<input type="date" class="form-control " value="<?php print_r ($data['tgl_penelitian']); ?>" id="mulai_tgl" name="mulai_tgl" required />
	 </div>
	 <div class="form-group">
			<label for="nama" class="lb">Kota Instansi</label>
			<input type="text" class="form-control " value="<?php print_r ($data['kab_penelitian']); ?>" id="kab_penelitian" name="kab_penelitian" required />
	 </div>
	 <div class="form-group">
			<label for="nama" class="lb">Mulai Pada Jam </label>
			<input type="time" class="form-control " value="<?php print_r ($data['jam_mulai']); ?>" id="time" name="time_mulai" required />
	 </div>
	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>
v   
<?php
}else if ($data['kode_surat'] == 8) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<div class="form-group">
		<label for="nama" class="lb">Tujuan Instansi</label>
		<input type="text" class="form-control " value="<?php print_r ($data['ket_instansi']); ?>" id="instansi" name="instansi" required  />
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Alamat instansi</label>
		<input type="text" class="form-control " value="<?php print_r ($data['alamat_instansi']); ?>" id="alamat" name="alamat" required />
	</div>
	 <div class="form-group">
			<label for="nama" class="lb">Tanggal Mulai</label>
			<input type="date" class="form-control " value="<?php print_r ($data['tgl_penelitian']); ?>" id="mulai_tgl" name="mulai_tgl" required />
	 </div>
	 <div class="form-group">
			<label for="nama" class="lb">Tanggal Akhir</label>
			<input type="date" class="form-control " value="<?php print_r ($data['tgl_akhir']); ?>" id="tgl_akhir" name="tgl_akhir" required />
	 </div>
	 <div class="form-group">
			<label for="nama" class="lb">Kota Instansi</label>
			<input type="text" class="form-control " value="<?php print_r ($data['kab_penelitian']); ?>" id="kab_penelitian" name="kab_penelitian" required />
	 </div>

	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>

<?php
}else if ($data['kode_surat'] == 9) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<div class="form-group">
		<label for="nama" class="lb">Nama Instansi</label>
		<input type="text" class="form-control " value="<?php print_r ($data['ket_instansi']); ?>" id="instansi" name="instansi" required  />
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Nama Jalan</label>
		<input type="text" class="form-control " value="<?php print_r ($data['alamat_instansi']); ?>" id="alamat" name="alamat" required />
	</div>
	 <div class="form-group">
			<label for="nama" class="lb">Tanggal Mulai</label>
			<input type="date" class="form-control " value="<?php print_r ($data['tgl_penelitian']); ?>" id="mulai_tgl" name="mulai_tgl" required />
	 </div>
	 <div class="form-group">
			<label for="nama" class="lb">Tanggal Akhir</label>
			<input type="date" class="form-control " value="<?php print_r ($data['tgl_akhir']); ?>" id="tgl_akhir" name="tgl_akhir" required />
	 </div>
	 <div class="form-group">
			<label for="nama" class="lb">Kota Instansi</label>
			<input type="text" class="form-control " value="<?php print_r ($data['kab_penelitian']); ?>" id="kab_penelitian" name="kab_penelitian" required />
	 </div>

	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>

<?php
}else if ($data['kode_surat'] == 10) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<div class="form-group">
		<label for="nama" class="lb">Nama Instansi</label>
		<input type="text" class="form-control " value="<?php print_r ($data['ket_instansi']); ?>" id="instansi" name="instansi" required  />
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Nama Jalan</label>
		<input type="text" class="form-control " value="<?php print_r ($data['alamat_instansi']); ?>" id="alamat" name="alamat" required />
	</div>
	 <div class="form-group">
			<label for="nama" class="lb">Tanggal Mulai</label>
			<input type="date" class="form-control " value="<?php print_r ($data['tgl_penelitian']); ?>" id="mulai_tgl" name="mulai_tgl" required />
	 </div>
	 <div class="form-group">
			<label for="nama" class="lb">Tanggal Akhir</label>
			<input type="date" class="form-control " value="<?php print_r ($data['tgl_akhir']); ?>" id="tgl_akhir" name="tgl_akhir" required />
	 </div>
	 <div class="form-group">
			<label for="nama" class="lb">Kota Instansi</label>
			<input type="text" class="form-control " value="<?php print_r ($data['kab_penelitian']); ?>" id="kab_penelitian" name="kab_penelitian" required />
	 </div>

	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>

<?php
}else if ($data['kode_surat'] == 11) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<input type="hidden" name="nim_mhs" id="nim_mhs" value="<?php print_r ($data['nim']);?>" />
	<div class="form-group">
	  <label for="nama" class="lb">Tanggal Lahir Mahasiswa</label>
	  <input type="date" class="form-control" name="tgl_lahir_mhs" value="<?php echo $data_siakad['tgllahir']; ?>" >
	</div>
	<div class="form-group">
	  <label for="nama" class="lb">Kota Tempat Lahir Mahasiswa</label>
	  <input type="text" class="form-control" name="kota_tmpt_mhs" value="<?php echo $data_siakad['tmplahir']; ?>" >
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Alamat Mahasiswa</label>
		<textarea type="text" class="form-control" name="mhs_alamat" style="height:100px;" ><?php print_r ($data['mhs_alamat']);?></textarea>
	</div>

	<div class="form-group">
	  <label for="nama" class="lb">Keperluan Mahasiswa</label>
		<input type="text" class="form-control " name="surat1_keperluan" placeholder="mengajukan beasiswa Juara Kompas Indonesia" value="<?php print_r ($data['srt1_keperluan']);?>"/>
	</div>
	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>

<?php
}else if ($data['kode_surat'] == 12) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<input type="hidden" name="nim_mhs" id="nim_mhs" value="<?php print_r ($data['nim']);?>" />
	<div class="form-group">
	  <label for="nama" class="lb">Tanggal Lahir Mahasiswa</label>
	  <input type="date" class="form-control" name="tgl_lahir_mhs" value="<?php echo $data_siakad['tgllahir']; ?>" >
	</div>
	<div class="form-group">
	  <label for="nama" class="lb">Kota Tempat Lahir Mahasiswa</label>
	  <input type="text" class="form-control" name="kota_tmpt_mhs" value="<?php echo $data_siakad['tmplahir']; ?>" >
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Alamat Mahasiswa</label>
		<textarea type="text" class="form-control" name="mhs_alamat" style="height:100px;" ><?php print_r ($data['mhs_alamat']);?></textarea>
	</div>

	<div class="form-group">
	  <label for="nama" class="lb">Keperluan Mahasiswa</label>
		<input type="text" class="form-control " name="surat1_keperluan" placeholder="mengajukan beasiswa Juara Kompas Indonesia" value="<?php print_r ($data['srt1_keperluan']);?>"/>
	</div>
	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>

<?php
}else if ($data['kode_surat'] == 13) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<input type="hidden" name="nim_mhs" id="nim_mhs" value="<?php print_r ($data['nim']);?>" />
	<div class="form-group">
        <label for="nama" class="lb">Tujuan Instansi</label>
        <input type="text" class="form-control " id="srt13_instansi" name="srt13_instansi" value="<?php print_r ($data['ket_instansi']);?>" required />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Alamat Instansi</label>
        <input type="text" class="form-control " id="srt13_alamat" name="srt13_alamat" value="<?php print_r ($data['alamat_instansi']);?>" required />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Kota Instansi</label>
        <input type="text" class="form-control " id="srt13_kabupaten" name="srt13_kabupaten" value="<?php print_r ($data['kab_penelitian']);?>" required />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Tujuan Laboratorium</label>
        <input type="text" class="form-control" id="srt13_tujuan" name="srt13_tujuan" value="<?php print_r ($data['laboratorium']);?>" required  ></input>
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Keperluan</label>
        <input type="text" class="form-control" id="srt13_keperluan" name="srt13_keperluan" value="<?php print_r ($data['srt1_keperluan']);?>" required ></input>
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Mata Kuliah</label>
        <input type="text" class="form-control" id="srt13_matakuliah" name="srt13_matakuliah" value="<?php print_r ($data['srt7_matkul']);?>" required ></input>
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Judul Penelitian</label>
        <textarea type="text" class="form-control" id="srt13_judul" name="srt13_judul" > <?php print_r ($data['judul_penelitian']);?> </textarea>
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Tanggal Mulai</label>
        <input type="date" class="form-control " id="srt13_tglawal" name="srt13_tglawal" value="<?php print_r ($data['tgl_penelitian']);?>" required />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Tanggal Akhir</label>
        <input type="date" class="form-control " id="srt13_tglakhir" name="srt13_tglakhir" value="<?php print_r ($data['tgl_akhir']);?>" required />
    </div>
	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>

<?php
}else if ($data['kode_surat'] == 14) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<input type="hidden" name="nim_mhs" id="nim_mhs" value="<?php print_r ($data['nim']);?>" />
	<div class="form-group">
        <label for="nama" class="lb">Tujuan Instansi</label>
        <input type="text" class="form-control " id="instansi" name="instansi" value="<?php print_r ($data['ket_instansi']);?>" required  />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Alamat Instansi</label>
        <input type="text" class="form-control " id="alamat" name="alamat" value="<?php print_r ($data['alamat_instansi']);?>" required />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Kota Instansi</label>
        <input type="text" class="form-control " id="kabupaten" name="kabupaten" value="<?php print_r ($data['kab_penelitian']);?>" required />
    </div>
	<div class="form-group">
        <label for="nama" class="lb">Mata Kuliah </label>
        <input type="text" class="form-control " id="srt7_matkul" name="srt7_matkul" value="<?php print_r ($data['srt7_matkul']);?>" required />
    </div>
	<div class="form-group">
        <label for="nama" class="lb">Judul Penelitian/Judul Tugas</label>
        <textarea type="text" class="form-control" id="srt3_judul" name="srt3_judul" ><?php print_r ($data['judul_penelitian']);?></textarea>
    </div>
	<div class="form-group">
        <label for="nama" class="lb">Nama Tempat Pengambilan Data</label>
        <input type="text" class="form-control" id="srt15_tujuan" name="srt13_tujuan" value="<?php print_r ($data['laboratorium']);?>"  ></input>
    </div>
	<div class="form-group">
        <label for="nama" class="lb">Tanggal Mulai</label>
        <input type="date" class="form-control " id="mulai_tgl" name="mulai_tgl" value="<?php print_r ($data['tgl_penelitian']);?>" required />
    </div>
	<div class="form-group">
        <label for="nama" class="lb">Tanggal Akhir</label>
        <input type="date" class="form-control " id="tgl_akhir" name="tgl_akhir" value="<?php print_r ($data['tgl_akhir']);?>" required />
    </div>
	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>

<?php
}else if ($data['kode_surat'] == 15) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<input type="hidden" name="nim_mhs" id="nim_mhs" value="<?php print_r ($data['nim']);?>" />
	<div class="form-group">
        <label for="nama" class="lb">Tujuan Instansi</label>
        <input type="text" class="form-control " id="srt15_instansi" name="srt15_instansi" value="<?php print_r ($data['ket_instansi']);?>" />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Alamat Instansi</label>
        <input type="text" class="form-control " id="srt15_alamat" name="srt15_alamat" value="<?php print_r ($data['alamat_instansi']);?>" />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Kota Instansi</label>
        <input type="text" class="form-control " id="srt15_kabupaten" name="srt15_kabupaten" value="<?php print_r ($data['kab_penelitian']);?>" />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Nama Tempat Pengambilan Data</label>
        <input type="text" class="form-control" id="srt15_tujuan" name="srt15_tujuan" value="<?php print_r ($data['laboratorium']);?>"  ></input>
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Mata Kuliah</label>
        <input type="text" class="form-control" id="srt15_matakuliah" name="srt15_matakuliah" value="<?php print_r ($data['srt7_matkul']);?>" ></input>
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Data Yang Dibutuhkan</label>
        <input type="text" class="form-control" id="srt15_data" name="srt15_data" value="<?php print_r ($data['str15_data']);?>" ></input>
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Judul Penelitian</label>
        <textarea type="text" class="form-control" id="srt15_judul" name="srt15_judul" ><?php print_r ($data['judul_penelitian']);?></textarea>
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Tanggal Mulai</label>
        <input type="date" class="form-control " id="srt15_tglawal" name="srt15_tglawal" value="<?php echo $data['tgl_penelitian'];?>"/>
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Tanggal Akhir</label>
        <input type="date" class="form-control " id="srt15_tglakhir" name="srt15_tglakhir" value="<?php echo $data['tgl_akhir'];?>"/>
    </div>
	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>
<?php
}else if ($data['kode_surat'] == 16) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<input type="hidden" name="nim_mhs" id="nim_mhs" value="<?php print_r ($data['nim']);?>" />
	<div class="form-group">
        <label for="nama" class="lb">Tujuan Instansi</label>
        <input type="text" class="form-control " id="srt16_instansi" name="srt16_instansi" value="<?php print_r ($data['ket_instansi']);?>" />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Alamat Instansi</label>
        <input type="text" class="form-control " id="srt16_alamat" name="srt16_alamat" value="<?php print_r ($data['alamat_instansi']);?>" />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Kota Instansi</label>
        <input type="text" class="form-control " id="srt16_kabupaten" name="srt16_kabupaten" value="<?php print_r ($data['kab_penelitian']);?>" />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Nama Tempat Penelitian</label>
        <input type="text" class="form-control" id="srt16_tujuan" name="srt16_tujuan" value="<?php print_r ($data['laboratorium']);?>" ></input>
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Keperluan Izin</label>
        <input type="text" class="form-control" id="srt16_data" name="srt16_data" value="<?php print_r ($data['str15_data']);?>" ></input>
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Tanggal Mulai</label>
        <input type="date" class="form-control " id="srt16_tglawal" name="srt16_tglawal" value="<?php echo $data['tgl_penelitian'];?>" />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Tanggal Akhir</label>
        <input type="date" class="form-control " id="srt16_tglakhir" name="srt16_tglakhir" value="<?php echo $data['tgl_akhir'];?> "/>
    </div>
	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>
<?php
}else if ($data['kode_surat'] == 18) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<input type="hidden" name="nim_mhs" id="nim_mhs" value="<?php print_r ($data['nim']);?>" />
	<div class="form-group">
        <label for="nama" class="lb">Nomor Surat Keputusan</label>
        <input type="text" class="form-control " id="nomor_surat" name="nomor_surat" value="<?php print_r ($data['ket_instansi']);?>" />
    </div>
    <div class="form-group">
        <label for="nama" class="lb">Tanggal Surat Keputusan</label>
        <input type="date" class="form-control " id="srt16_tglawal" name="srt16_tglawal" value="<?php echo $data['tgl_penelitian'];?>" />
    </div>
<?php
}else if ($data['kode_surat'] == 20) { ?>
	<input type="hidden" name="id" id="id" value="<?php print_r ($data['id']);?>" />
	<input type="hidden" name="nim_mhs" id="nim_mhs" value="<?php print_r ($data['nim']);?>" />
	<div class="form-group">
	  <label for="nama" class="lb">Tanggal Lahir Mahasiswa</label>
	  <input type="date" class="form-control" name="tgl_lahir_mhs" value="<?php echo $data_siakad['tgllahir']; ?>" >
	</div>
	<div class="form-group">
	  <label for="nama" class="lb">Kota Tempat Lahir Mahasiswa</label>
	  <input type="text" class="form-control" name="kota_tmpt_mhs" value="<?php echo $data_siakad['tmplahir']; ?>" >
	</div>
	<div class="form-group">
		<label for="nama" class="lb">Alamat Mahasiswa</label>
		<textarea type="text" class="form-control" name="mhs_alamat" style="height:100px;" ><?php print_r ($data['mhs_alamat']);?></textarea>
	</div>
	<div class="form-group">
		 <label for="nama" class="lb">Keterangan</label>
		 <select class="form-control" name="dekanat">
			<option value="0" <?php if($data['dekanat'] == 0){ echo "selected";} ?> >Dekan</option>
			<option value="1" <?php if($data['dekanat'] == 1){ echo "selected";} ?> >Wakil Dekan 1</option>
			<option value="2" <?php if($data['dekanat'] == 2){ echo "selected";} ?> >Wakil Dekan 2</option>
			<option value="3" <?php if($data['dekanat'] == 3){ echo "selected";} ?> >Wakil Dekan 3</option>
			<option value="4">Kepala Bagian Tata Usaha</option>
		 </select>
	</div>

<?php
}
?>
