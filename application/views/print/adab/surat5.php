<?php

$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);
//echo $nobulan;



if($nobulan >= 3 && $nobulan < 8){
  if($nobulan == 7 && $hari == 31){
    $tahunajaran = ($tahun)."/".($tahun+1);
    $sem = "Gasal";
  }else {
    $tahunajaran = ($tahun-1)."/".$tahun;
    $sem = "Genap";
  }
}else {
  $tahunajaran = ($tahun)."/".($tahun+1);
	$sem = "Gasal";
}
	$kodedekan = "D";
	$jabatan = "Dekan";
	$namaDekan = $dekan['nama'];
	$nipDekan = $dekan['nip'];

  $tgllahir = indo($tgllahir);

  $tanggalan = indo(date("Y-m-d"));
function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
  // print_r ($BulanIndo[$bulan-1]);
  // echo $bulan;

  $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun;
  return $data;
}

$nama = ucwords(strtolower($nama));

if ($pengajuan[0]['dekanat'] == 0 || $pengajuan[0]['dekanat'] == "" ) {
    $ttd =  "Dekan, &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
              &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
              &emsp;&emsp;&emsp;&emsp;&emsp;
              &emsp;&emsp;&emsp;&emsp;&emsp; ";
    $dekanat2 = $dekan['nama']." &emsp;&emsp;";
  	$nip_dekanat = $dekan['nip']." &emsp;&emsp;&emsp;&emsp;
                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                    &emsp;&emsp;&emsp;&ensp;";
}else if ($pengajuan[0]['dekanat'] == 1) {
    $ttd =  "a.n. Dekan,<br/>Wakil Dekan Bidang Akademik dan Kelembagaan
              &emsp; <br/><br/>";
    $dekanat = $wadek1['nama']." &emsp;&emsp;&emsp;&emsp;
                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;".$nama;
    $dekanat2 = $wadek1['nama']." &emsp;&emsp;&emsp;&emsp;
                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;";
    $nip_dekanat = $wadek1['nip']." &emsp;&emsp;&emsp;&emsp;
                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;";
}else if ($pengajuan[0]['dekanat'] == 2) {
    $ttd =  "a.n. Dekan,<br/>Wakil Dekan Bidang Administrasi Umum <br>
            Perencanaan dan Keuangan &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            &emsp;&emsp;&emsp;&emsp; <br/><br/><br/> ";
    $dekanat = $wadek2['nama']." &emsp;&emsp;&emsp;&emsp;
                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;".$nama;
    $dekanat2 = $wadek2['nama']." &emsp;&emsp;&emsp;&emsp;
                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;";
  	$nip_dekanat = $wadek2['nip']." &emsp;&emsp;&emsp;&emsp;
                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;";
}else if ($pengajuan[0]['dekanat'] == 3) {
    $ttd =  "a.n. Dekan,<br/>Wakil Dekan Bidang Kemahasiswaan <br>
            dan Kerjasama &emsp;&emsp;&emsp;&emsp;&emsp;
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            &emsp;&emsp;&emsp; <br/><br/>";
  	$dekanat = $wadek3['nama']." &emsp;&emsp;&emsp;&emsp;
                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;
                &emsp;&emsp;&ensp;".$nama;
  	$dekanat2 = $wadek3['nama']." &emsp;&emsp;&emsp;&emsp;
                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;
                &emsp;&emsp;&ensp;";
  	$nip_dekanat = $wadek3['nip']." &emsp;&emsp;&emsp;&emsp;
                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;";
}else if ($pengajuan[0]['dekanat'] == 4) {
  $ttd =  "a.n. Dekan,<br/>Kabag. Tata Usaha <br>
          dan Kerjasama &emsp;&emsp;&emsp;&emsp;&emsp;
          &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
          &emsp;&emsp;&emsp; <br/><br/>";
  $dekanat = $tu['nama']." &emsp;&emsp;&emsp;&emsp;
              &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;
              &emsp;&emsp;&ensp;".$nama;
  $dekanat2 = $tu['nama']." &emsp;&emsp;&emsp;&emsp;
              &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;
              &emsp;&emsp;&ensp;";
  $nip_dekanat = $tu['nip']." &emsp;&emsp;&emsp;&emsp;
                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;";
}else {
    $ttd =  "Dekan,<br/>";
    $dekanat = $dekan['nama'];
    $nip_dekanat = $dekan['nip'];
}


?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		// window.print();

	});
</script>
<html>

	<head><title>Surat Permohonan Cuti Studi atas nama <?php echo $nama; ?></title></head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
<font face="arial" size="11">
	<body>

<table width="650px;" style=margin:auto;>
	<tr>
		<td>
      <table style="font-size:0.9em;">
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr>
          <td>Perihal</td>
          <td>:</td>
          <td><strong>Permohonan Cuti Studi</strong></td>
          <td>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</td>
          <td>&emsp;&emsp;&emsp;</td>
          <td>Surabaya, <?php echo $tanggalan ;?></td>
        </tr>
      </table>
      <br>
      <br>
      <br>
      <table style="font-size:0.9em;">
        <tr>
          <td>&emsp;&emsp;&emsp;</td>
          <td>Kepada Yth,</td>
        </tr>
        <tr>
          <td></td>
          <td><strong>Rektor UIN Sunan Ampel Surabaya</strong></td>
        </tr>
        <tr>
          <td></td>
          <td><strong>Jl. A. Yani 117</strong></td>
        </tr>
        <tr>
          <td></td>
          <td>di </td>
        </tr>
        <tr>
          <td></td>
          <td>&emsp;<u>Surabaya</u></td>
        </tr>

          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>

          <tr>
            <td></td>
            <td><i>Assalamu'alaikum Wr. Wb.</i></td>
          </tr>

          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr>
            <td></td>
            <td>Yang bertanda tangan di bawah ini:</td>
          </tr>

          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr>
            <td></td>
            <td>
              <table style="font-size:0.9em;">
                <tr>
                  <td>&emsp;1. </td>
                  <td>&emsp;Nama</td>
                  <td></td>
                  <td>:</td>
                  <td>&emsp;<?php echo ucwords(strtolower($nama));?></td>
                </tr>

                <tr> <td></td> </tr>
                <tr> <td></td> </tr>
                <tr>
                  <td>&emsp;2. </td>
                  <td>&emsp;NIM</td>
                  <td></td>
                  <td>:</td>
                  <td>&emsp;<?php echo $nim?></td>
                </tr>

                <tr> <td></td> </tr>
                <tr> <td></td> </tr>
                <tr>
                  <td>&emsp;3. </td>
                  <td>&emsp;Tempat/ Tanggal Lahir</td>
                  <td></td>
                  <td>:</td>
                  <td>&emsp;<?php echo $tmplahir.", ".$tgllahir;?></td>
                </tr>
                <tr> <td></td> </tr>
                <tr> <td></td> </tr>
                <tr>
                  <td>&emsp;4. </td>
                  <td>&emsp;Program Studi</td>
                  <td></td>
                  <td>:</td>
                  <td>&emsp;<?php echo $prodi;?></td>
                </tr>
                <tr> <td></td> </tr>
                <tr> <td></td> </tr>
                <tr>
                  <td>&emsp;5. </td>
                  <td>&emsp;Alamat</td>
                  <td></td>
                  <td>:</td>
                  <td>&emsp;<?php echo $pengajuan[0]['mhs_alamat']; ?></td>
                </tr>
                <tr> <td></td> </tr>
                <tr> <td></td> </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>
              <div style="text-align:justify; line-height:25px;">
<?php $data = array('1','2','3','4','5');?>
<?php 	$cari = array_search($cuti, $data); ?>  
<?php $hasil = array('Pertama','Kedua','Ketiga','Keempat','Kelima'); ?>

                Dengan ini saya mohon kepada Rektor, agar dapat diberi izin cuti studi yang <?php echo $hasil[$cari]; ?> pada Semester <?php echo $sem; ?> Tahun Akademik <?php echo $tahunajaran; ?>, dengan alasan <?php echo $pengajuan[0]['mhs_alasan']; ?>.
          		</div>
            </td>
          </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr> <td></td> </tr>
          <tr>
            <td></td>
            <td>
              <p>
      				  <i>Wassalamu'alaikum Wr. Wb.</i>
      			  </p>
            </td>
          </tr>
      </table>
      <br>
      <table style="font-size:0.9em;">
        <tr>
          <td>&emsp;&emsp;&emsp;</td>
        </tr>
        <tr>
          <td></td>
          <td><?php echo $ttd; ?></td>
          <td style="text-align: center; vertical-align: middle;">Pemohon</td>
        </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr> <td></td> </tr>
        <tr>
          <td></td>
          <td><?php echo $dekanat2; ?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $nama; ?></td>
        </tr>
        <tr>
          <td></td>
          <td><?php echo $nip_dekanat; ?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $nim; ?></td>
        </tr>
        <tr>
          <td>&emsp;&emsp;&emsp;</td>
          <td></td>
        </tr>
      </table>


		<br/>
		<br/>
		<div style="line-height:25px; font-size:0.9em;">Tindasan disampaikan kepada Yth : <br>
      1. Wali Studi yang bersangkutan <br>
      2. Kaprodi Prodi yang Bersangkutan	</div>

	</td>
	</tr>
</table>



	</body>
</font>
</html>
