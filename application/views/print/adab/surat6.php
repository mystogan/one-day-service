<?php

$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);
$fakultascilik = ucwords(strtolower($fakultas));
// echo $fakultas;

if($nobulan >= 3 && $nobulan < 8){
  $tahunajaran = ($tahun-1)."/".$tahun;
	$sem = "Genap";
}else {
  $tahunajaran = ($tahun)."/".($tahun+1);
	$sem = "Gasal";
}
	$kodedekan = "D";
	$jabatan = "Dekan";
	$namaDekan = $dekan['nama'];
	$nipDekan = $dekan['nip'];

	$tgllahir = indo($tgllahir);

  $tanggalan = indo(date("Y-m-d"));
function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
  // print_r ($BulanIndo[$bulan-1]);
  // echo $bulan;

  $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun;
  return $data;
}


if ($pengajuan[0]['dekanat'] == 0 || $pengajuan[0]['dekanat'] == "" ) {
  $ttd =  "<br/>Dekan,";
  $kodedekan = "D";
  $dekanat = $dekan['nama'];
  $nip_dekanat = $dekan['nip'];
}else if ($pengajuan[0]['dekanat'] == 1) {
  $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Akademik dan Kelembagaan";
  $kodedekan = "D1";
  $dekanat = $wadek1['nama'];
  $nip_dekanat = $wadek1['nip'];
}else if ($pengajuan[0]['dekanat'] == 2) {
  $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Administrasi Umum Perencanaan dan Keuangan";
  $kodedekan = "D2";
  $dekanat = $wadek2['nama'];
  $nip_dekanat = $wadek2['nip'];
}else if ($pengajuan[0]['dekanat'] == 3) {
  $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Kemahasiswaan dan Kerjasama";
  $kodedekan = "D3";
  $dekanat = $wadek3['nama'];
  $nip_dekanat = $wadek3['nip'];
} else if($pengajuan[0]['dekanat'] == 4){
  $ttd =  "<br/>a.n. Dekan,<br/>Kabag. Tata Usaha";
  $kodedekan = "TU";
  $dekanat = $tu['nama'];
  $nip_dekanat = $tu['nip'];
}else {
  $ttd =  "<br/>Dekan,";
  $kodedekan = "D";
  $dekanat = $dekan['nama'];
  $nip_dekanat = $dekan['nip'];
}


?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		// window.print();

	});
</script>

<html>

	<head>
		<title>Surat Pernyataan Masih Sekolah atas Nama <?php echo $nama; ?></title>
				<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
  <font face="arial" size="11">
	<body>

<table width="650px;" style="margin:auto; margin-top:10px;">
	<tr>
		<td>
		<div id="header">
          <div id="gambar"><img style="height:10%; width:80%;" src="<?php echo base_url(); ?>assets/images/uin.png"/></div>
          <div id="tulisan_uin"><strong>KEMENTERIAN AGAMA REPUBLIK INDONESIA</strong><br/><span style="font-size:0.85em">
            <strong>UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA</strong></span>
        <br/><span style="font-size:0.85em"> <strong><?php echo $fakultas; ?></strong> </span>
        <br/>
          <p style=" margin-top: 10px;font-size:0.6em">
          Jl. A. Yani 117 Surabaya 60237 Telp. 031-8410298 Fax. 031-8413300 email : <u><?php echo $singkatan; ?>@uinsby.ac.id</u>
        </p></div>
        </div>

        <div id="garis"></div>
        <br/>
	<div id="satu">
      <div style="margin-left: 25%; font-size: 0.9em;"><strong> <u>SURAT PERNYATAAN MASIH SEKOLAH/ KULIAH</u> </strong>
        <br/>
        <strong>&emsp;&nbsp;Nomor : B -&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/Un.07/<?php echo $nomor; ?>/<?php echo $kodedekan; ?>/PP.00.9/<?php echo $nobulan."/".$tahun;?></strong>
      </div>
      <br>
      <label for="" style="font-size:0.95em;">Yang bertanda tangan dibawah ini :</label>
      <table style="line-height:25px; font-size:0.95em;">
        <tr>
          <td>1.&emsp;</td>
          <td>N a m a </td>
          <td>:</td>
          <td><?php echo $dekanat; ?></td>
        </tr>
        <tr>
          <td>2.&emsp;</td>
          <td>N I P </td>
          <td>:</td>
          <td><?php echo $nip_dekanat; ?></td>
        </tr>
        <tr>
          <td style="vertical-align: text-top;">3.&emsp;</td>
          <td style="vertical-align: text-top;">Pangkat/ Gol.Ruang &emsp;&emsp;&emsp;</td>
          <td style="vertical-align: text-top;">:</td>
          <td  style="vertical-align: text-top;"><?php
              $this->db2 = $this->load->database('simpeg', TRUE);
              $this->db2->select('b.*');
          		$this->db2->from('tbpegawai a');
          		$this->db2->join('m_pangkat b', 'a.id_pangkat = b.id ');
          		$this->db2->where('a.nip =', $nip_dekanat);
          		$query = $this->db2->get();
          		$pangkat = $query->result_array();

              // print_r ($pangkat);
              echo $pangkat[0]['nama_pangkat']."(".$pangkat[0]['nama_golongan'].")";
           ?></td>
        </tr>
        <tr>
          <td style="vertical-align: text-top;">4.&emsp;</td>
          <td style="vertical-align: text-top;">Jabatan </td>
          <td style="vertical-align: text-top;">:</td>
          <td><?php echo $pangkats; ?></td>
        </tr>

      </table>
      <br>
      <strong><label for="" style="font-size:0.9em;">dengan ini menyatakan dengan sesungguhnya bahwa : </label></strong>

      <br>
      <table style="line-height:25px; font-size:0.9em;">
        <tr>
          <td>1. </td>
          <td>&emsp;N a m a</td>
          <td></td>
          <td>&ensp;:</td>
          <td>&emsp;<?php echo ucwords(strtolower($nama));?></td>
        </tr>
        <tr>
          <td>2. </td>
          <td>&emsp;Tempat/ Tanggal Lahir </td>
          <td></td>
          <td>&ensp;:</td>
          <td>&emsp;<?php echo $tmplahir.", ".$tgllahir;?></td>
        </tr>
        <tr>
          <td>3. </td>
          <td>&emsp;NIM</td>
          <td></td>
          <td>&ensp;:</td>
          <td>&emsp;<?php echo $nim?></td>
        </tr>

        <tr>
          <td>4. </td>
          <td>&emsp;Dengan lama masa belajar</td>
          <td></td>
          <td>&ensp;:</td>
          <td>&emsp;4 - 7 Tahun</td>
        </tr>
        <tr>
          <td>5. </td>
          <td>&emsp;Semester/Tahun Akademik</td>
          <td></td>
          <td>&ensp;:</td>
          <td>&emsp;<?php echo $semestermhs," / ",$tahunajaran; ?></td>
        </tr>
      </table>
      <br>
      <strong>
        <label for="" style="font-size:0.9em;">Adalah benar-benar mahasiswa <?php echo $fakultascilik; ?> UIN Sunan Ampel Surabaya</label>
      </strong>
      <br>
      <br>
      <strong>
        <label for="" style="font-size:0.9em;">Dan Bahwa Wali Mahasiswa/i Tersebut : </label>
      </strong>
      <table style="line-height:25px; font-size:0.9em;">
        <tr>
          <td>1. </td>
          <td>&emsp;N a m a</td>
          <td></td>
          <td>&ensp;:</td>
          <td>&emsp;<?php echo $pengajuan[0]['srt6_nama_ortu'];?></td>
        </tr>
        <tr>
          <td>2. </td>
          <td>&emsp;N I P / NRP</td>
          <td></td>
          <td>&ensp;:</td>
          <td>&emsp;<?php echo $pengajuan[0]['srt6_nip'];?></td>
        </tr>
        <tr>
          <td>3. </td>
          <td>&emsp;Pangkat/ Gol.Ruang&emsp;&emsp;&emsp;</td>
          <td></td>
          <td>&ensp;:</td>
          <td>&emsp;<?php echo $pengajuan[0]['srt6_pangkat'];?></td>
        </tr>

        <tr>
          <td>4. </td>
          <td>&emsp;Instansi</td>
          <td></td>
          <td>&ensp;:</td>
          <td>&emsp;<?php echo $pengajuan[0]['srt6_instansi'];?></td>

        </tr>
      </table>
      <br>
      <div style="line-height:30px; font-size:0.9em; text-align: justify; text-justify: inter-word;"  for="">&emsp;&emsp;&emsp;Demikian surat pernyataan ini dibuat dengan sesungguhnya dan apabila
      dikemudian hari surat pernyataan ini ternyata tidak benar, yang mengakibatkan
      kerugian terhadap Negara Republik Indonesia, maka saya bersedia menanggung
      kerugian tersebut.</div>
      <br>
	  
		  <div class="row">
			  <div class="col-md-4">
			  </div>
			  <div class="col-md-8">
				<div id="id" style="font-size:0.9em;">Surabaya, <?php echo $tanggalan;?>
					<br>
					<br>

					<?php echo $ttd; ?>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<?php echo $dekanat; ?><br/>
					NIP. <?php echo $nip_dekanat; ?>

				  </div>
			  </div>			  
		  </div>
	  </div>


	</td>
	</tr>
</table>



	</body>
</font>
</html>
