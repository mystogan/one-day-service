<?php

$fakultascilik1 = str_replace("Fak.","Fakultas",$fakultas);
$fakultascilik2 = $fakultascilik1;
$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);
$kota = str_replace("KAB.","Kabupaten",$kota);

if($nobulan >= 3 && $nobulan < 8){
  $tahunajaran = ($tahun-1)."/".$tahun;
  $sem = "Genap";
}else {
  $tahunajaran = ($tahun)."/".($tahun+1);
  $sem = "Gasal";
}
$tgllahir = indo($tgllahir);
$tanggalan = indo(date("Y-m-d"));
function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
  $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun;
  return $data;
}

if ($pengajuan[0]['dekanat'] == 0 || $pengajuan[0]['dekanat'] == "" ) {
  $ttd =  "<br/>Dekan,";
  $kodedekan = "D";
  $dekanat = $dekan['nama'];
  $nip_dekanat = $dekan['nip'];
}else if ($pengajuan[0]['dekanat'] == 1) {
  $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Akademik dan Kelembagaan";
  $kodedekan = "D1";
  $dekanat = $wadek1['nama'];
  $nip_dekanat = $wadek1['nip'];
}else if ($pengajuan[0]['dekanat'] == 2) {
  $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Administrasi Umum Perencanaan dan Keuangan";
  $kodedekan = "D2";
  $dekanat = $wadek2['nama'];
  $nip_dekanat = $wadek2['nip'];
}else if ($pengajuan[0]['dekanat'] == 3) {
  $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Kemahasiswaan dan Kerjasama";
  $kodedekan = "D3";
  $dekanat = $wadek3['nama'];
  $nip_dekanat = $wadek3['nip'];
} else if($pengajuan[0]['dekanat'] == 4){
  $ttd =  "<br/>a.n. Dekan,<br/>Kabag. Tata Usaha";
  $kodedekan = "TU";
  $dekanat = $tu['nama'];
  $nip_dekanat = $tu['nip'];
}else {
  $ttd =  "<br/>Dekan,";
  $kodedekan = "D";
  $dekanat = $dekan['nama'];
  $nip_dekanat = $dekan['nip'];
}


 ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		// window.print();

	});
</script>
<style type="text/css" media="print">
@page
    {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html
    {
        background-color: #FFFFFF;
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }

    body
    {
        margin: 5mm 15mm 10mm 15mm; /* margin you want for the content */
    }
</style>
<html>

	<head><title>Surat Keterangan Aktif Studi Atas Nama <?php echo $nama;?></title></head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
<font face="arial" size="11">
	<body>

<table width="650px;" style="margin:auto;">
	<tr>
		<td>


	<div id="header">
			<div id="gambar"><img style="height:10%; width:80%;" src="<?php echo base_url(); ?>assets/images/uin.png"/></div>
      <div id="tulisan_uin"><strong>KEMENTERIAN AGAMA REPUBLIK INDONESIA</strong><br/><span style="font-size:0.85em">
        <strong>UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA</strong></span>
	  <br/><span style="font-size:0.85em"> <strong><?php echo $fakultas; ?></strong> </span>
    <br/>
      <p style=" margin-top: 10px;font-size:0.6em">
      Jl. A. Yani 117 Surabaya 60237 Telp. 031-8410298 Fax. 031-8413300 email : <u><?php echo $singkatan; ?>@uinsby.ac.id</u>
    </p></div>
		</div>

		<div id="garis"></div>
    <br/>
	<div id="satu">
    <table width="580px;" style="margin:auto; font-size:0.9em;">
      <tr>
        <td>
          <div style="text-align:center;"><strong> <u>SURAT REKOMENDASI</u> </strong>
            <br/>
            <strong>Nomor : B -&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/Un.07/<?php echo $nomor; ?>/<?php echo $kodedekan; ?>/PP.00.9/<?php echo $nobulan."/".$tahun;?></strong>
          </div>
          <br/>
          <br/>
          <br/>
          <div id="">Dekan <?php echo $fakultascilik2; ?> UIN Sunan Ampel menerangkan bahwa:</div>
          <br/>
          <div id="">
            <table style="font-size:0.95em;">
              <tr>
                <td></td>
                <td>Nama</td>
                <td></td>
                <td>:</td>
                <td><?php echo ucwords(strtolower($nama));?></td>
              </tr>
              <tr></tr>
              <tr></tr>
              <tr>
                <td></td>
                <td>Tempat, Tanggal lahir</td>
                <td></td>
                <td>:</td>
                <td><?php echo ucwords(strtolower($tmplahir)).", ".$tgllahir;?></td>
              </tr>
              <tr></tr>
              <tr></tr>
              <tr>
                <td></td>
                <td>NIM</td>
                <td></td>
                <td>:</td>
                <td><?php echo $nim;?></td>
              </tr>
              <tr></tr>
              <tr></tr>
              <tr>
                <td></td>
                <td>Semester/Prodi</td>
                <td></td>
                <td>:</td>
                <td><?php echo $semestermhs."/".$prodi;?></td>
              </tr>
              <tr></tr>
              <tr></tr>
              <tr>
                <td></td>
                <td>Alamat</td>
                <td></td>
                <td>:</td>
                <td><?php print_r ($pengajuan[0]['mhs_alamat']); ?></td>
              </tr>
            </table>
          </div>
          <div style="margin-top: 7px;text-align:justify; line-height:25px;">adalah mahasiswa aktif studi pada <?php echo $fakultascilik2; ?>
            UIN Sunan Ampel Surabaya Semester <?php echo $sem; ?>
            Tahun Akademik <?php echo $tahunajaran; ?> dan tidak sedang menerima beasiswa dari pihak manapun,
            oleh karena itu direkomendasikan untuk mengajukan <?php print_r ($pengajuan[0]['srt1_keperluan']); ?>.
            <br>
            Demikian Surat Keterangan ini dibuat agar dapat dipergunakan sebagaimana mestinya.
          </div>
          <div></div>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <div id="id">
            <strong>
              Surabaya, <?php echo $tanggalan;?>
              <?php echo $ttd; ?>
            </strong>

          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <strong>
            <?php echo $dekanat; ?><br/>
            NIP. <?php echo $nip_dekanat; ?>

          </strong>

          </div>
		  </div></td>
          </table>
        </td>
      </tr>
	</tr>
</table>



	</body>
</font>
</html>
