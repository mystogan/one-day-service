<?php
$fakultascilik1 = str_replace("Fak.","Fakultas",$fakultas);
$fakultascilik2 = $fakultascilik1;
$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);
//echo $fakultas;

if($nobulan >= 3 && $nobulan < 8){
  $tahunajaran = ($tahun-1)."/".$tahun;
	$sem = "Genap";
}else {
  $tahunajaran = ($tahun)."/".($tahun+1);
	$sem = "Gasal";
}

$tgl = indo($pengajuan[0]['tgl_penelitian']);
$tgl1 = indo($pengajuan[0]['tgl_akhir']);

$tanggalan = indo(date("Y-m-d"));

if ($pengajuan[0]['dekanat'] == 0 || $pengajuan[0]['dekanat'] == "" ) {
  $ttd =  "<br/>Dekan,";
  $kodedekan = "D";
  $dekanat = $dekan['nama'];
  $nip_dekanat = $dekan['nip'];
}else if ($pengajuan[0]['dekanat'] == 1) {
  $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Akademik dan Kelembagaan";
  $kodedekan = "D1";
  $dekanat = $wadek1['nama'];
  $nip_dekanat = $wadek1['nip'];
}else if ($pengajuan[0]['dekanat'] == 2) {
  $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Administrasi Umum Perencanaan dan Keuangan";
  $kodedekan = "D2";
  $dekanat = $wadek2['nama'];
  $nip_dekanat = $wadek2['nip'];
}else if ($pengajuan[0]['dekanat'] == 3) {
  $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Kemahasiswaan dan Kerjasama";
  $kodedekan = "D3";
  $dekanat = $wadek3['nama'];
  $nip_dekanat = $wadek3['nip'];
} else if($pengajuan[0]['dekanat'] == 4){
  $ttd =  "<br/>a.n. Dekan,<br/>Kabag. Tata Usaha";
  $kodedekan = "TU";
  $dekanat = $tu['nama'];
  $nip_dekanat = $tu['nip'];
}else {
  $ttd =  "<br/>Dekan,";
  $kodedekan = "D";
  $dekanat = $dekan['nama'];
  $nip_dekanat = $dekan['nip'];
}

function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);

  $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun;
  return $data;
}


$hariindo = array(
	'Sun' => 'Minggu',
	'Mon' => 'Senin',
	'Tue' => 'Selasa',
	'Wed' => 'Rabu',
	'Thu' => 'Kamis',
	'Fri' => 'Jumat',
	'Sat' => 'Sabtu'
);
 ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		 window.print();

	});
</script>
<style type="text/css" media="print">
@page
    {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html
    {
        background-color: #FFFFFF;
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }

    body
    {
        margin: 5mm 15mm 10mm 15mm; /* margin you want for the content */
    }
</style>
<html>

	<head><title>Surat Kerjasama Praktek Kerja Lapangan atas nama <?php echo $nama;?></title></head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
  <font face="arial" size="11">
	<body>

<table width="650px;" style="margin:auto; margin-top:20px;">
	<tr>
		<td>
      <div id="header">
          <div id="gambar"><img style="height:10%; width:80%;" src="<?php echo base_url(); ?>assets/images/uin.png"/></div>
          <div id="tulisan_uin"><strong>KEMENTERIAN AGAMA REPUBLIK INDONESIA</strong><br/><span style="font-size:0.85em">
            <strong>UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA</strong></span>
        <br/><span style="font-size:0.85em"> <strong><?php echo $fakultas; ?></strong> </span>
        <br/>
          <p style=" margin-top: 10px;font-size:0.6em">
          Jl. A. Yani 117 Surabaya 60237 Telp. 031-8410298 Fax. 031-8413300 email : <u><?php echo $singkatan; ?>@uinsby.ac.id</u>
        </p></div>
        </div>

        <div id="garis"></div>
        <br/>
	<div id="satu">
	
	
        <table width="630px;" style="margin:auto; font-size:0.9em;">
          <tr>
            <td>
<table style="font-size:0.9em;">
  <tr>
    <td>&nbsp;Nomor</td>
    <td>:</td>
    <td>B -&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/Un.07/<?php echo $nomor; ?>/<?php echo $kodedekan; ?>/PP.00.9/<?php echo $nobulan."/".$tahun;?></td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>  &emsp;&emsp;&emsp;&emsp;Surabaya, <?php echo $tanggalan ;?></td>
  </tr>
  <tr>
    <td>&nbsp;Lampiran</td>
    <td>:</td>
    <td>1 (satu) Lembar</td>
  </tr>
  <tr>
    <td style="vertical-align: text-top;">&nbsp;Perihal</td>
    <td style="vertical-align: text-top;">:</td>
    <td><strong>Permohonan Ijin</strong> </td>
  </tr>
  <tr><td></td></tr>
  <tr><td></td></tr>
  <tr><td></td></tr>
  <tr><td></td></tr>

</table>
  <table style="font-size:0.9em; margin-left:4.5em">

      <tr>
        <td></td>
        <td>
          <br>
          <div id="">Kepada Yth,</div></td>
      </tr>
      <tr>
        <td></td>
        <td><div id=""><strong><?php print_r ($pengajuan[0]['ket_instansi']);  ?></strong></div></td>
      </tr>
      <tr>
        <td></td>
        <td><div style="width:75%;"><?php print_r ($pengajuan[0]['alamat_instansi']);  ?></div></td>
      </tr>
      <tr>
        <td></td>
        <td>Di <?php
        $kabu = str_replace("KABUPATEN","",$pengajuan[0]['kab_penelitian']);
        $kabu = str_replace("KOTA","",$kabu);
        $kabu  = ucwords(strtolower($kabu));
        echo $kabu;  ?></td>
      </tr>
      <!--<tr>
        <td></td>
        <td>&emsp;<?php
        $kabu = str_replace("KABUPATEN","",$pengajuan[0]['kab_penelitian']);
        $kabu = str_replace("KOTA","",$kabu);
        $kabu  = ucwords(strtolower($kabu));
        echo $kabu;  ?></td>
      </tr>-->
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr>
        <td></td>
        <td>
          <br><div id=""> <strong><i>Assalamu'alaikum Wr. Wb.</i></strong> </div><br></td>
      </tr>
      <tr>
        <td></td>
        <td><div id="" style="text-align:justify;  line-height:20px;">
			Dengan hormat, mahasiswa Prodi <?php echo $prodi; ?> <?php echo $fakultascilik2; ?> UIN Sunan Ampel Surabaya tersebut dibawah ini :
			<table border="1" id="customers" style="border-collapse: collapse; width: 100%;font-size: 0.9em;margin-top:.5vw;margin-bottom:.5vw;">
			  <tr>
				<th style="border: 1px solid #000;  padding: 8px; text-align:center;" >NO</th>
				<th style="border: 1px solid #000;  padding: 8px; text-align:center;" >NIM</th>
				<th style="border: 1px solid #000;  padding: 8px; text-align:center;" >NAMA</th>
				<th style="border: 1px solid #000;  padding: 8px; text-align:center;" >SEMESTER</th>
			  </tr>
			  <tr>
				<td style="border: 1px solid #000;  padding: 8px; text-align:center;" >1</td>
				<td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php echo $nim; ?></td>
				<td style="border: 1px solid #000;  padding: 8px; text-align:left;" ><?php echo ucwords(strtolower($nama)); ?></td>
				<td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php echo $semestermhs; ?></td>
			  </tr>
			  <?php
			  $i = 2;
			  foreach ($mhsdataAjuan as $hasil): ?>
				<tr>
				  <td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php echo $i; ?></td>
				  <td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php echo $hasil['nim_mhs']; ?></td>
				  <td style="border: 1px solid #000;  padding: 8px; text-align:left;" ><?php echo ucwords(strtolower($hasil['nama_mhs'])); ?></td>
				  <td style="border: 1px solid #000;  padding: 8px; text-align:center;" ><?php echo $hasil['semester_mhs']; ?></td>

				</tr>

			  <?php
			  $i++;
			  endforeach; ?>

			</table>
			Mohon memperoleh izin untuk melakukan <?php print_r ($pengajuan[0]['str15_data']); ?> pada <i><strong> <?php print_r ($pengajuan[0]['laboratorium']); ?> </strong></i> yang akan dilaksanakan pada tanggal <?php print_r ($tgl); ?> sampai tanggal <?php print_r ($tgl1); ?> sebagaimana proposal terlampir
			</br>
			</br>
			Demikian permohonan ini dibuat, atas perkenan dan kerjasamanya disampaikan terima kasih
            </div>
        </td>
      </tr>
      <tr style="line-height:40px;">
        <td></td>
        <td>
          <p>
    				<i><strong>Wassalamu'alaikum Wr. Wb.</strong></i>
    			</p>
        </td>
      </tr>
      <tr>
        <td></td>
        <td></td>
      </tr>
  </table>
		<div>
      <br>
      <br>
      <br>
      <div id="id" style="font-size:0.9em;">
        <strong><?php echo $ttd; ?></strong>

  		<br/>
  		<br/>
  		<br/>
  		<br/>
  		<br/>
      <strong><?php echo $dekanat; ?><br/>
      NIP. <?php echo $nip_dekanat; ?>
      </strong>
  		</div>
		
            </td>
          </tr>
        </table>
		</div>
</div>
      
</table>

	</body>
</font>
</html>

