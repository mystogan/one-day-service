<?php
$fakultascilik1 = str_replace("Fak.","Fakultas",$fakultas);
$fakultascilik2 = $fakultascilik1;
$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);
//echo $fakultas;

   $fakultas1 = $this->session->userdata('fakultas');

if($nobulan >= 3 && $nobulan < 8){
  $tahunajaran = ($tahun-1)."/".$tahun;
	$sem = "Genap";
}else {
  $tahunajaran = ($tahun)."/".($tahun+1);
	$sem = "Gasal";
}
$tgl = indo($pengajuan[0]['tgl_penelitian']);
$tgl2 = date('Y-m-d', strtotime('+6 days', strtotime($pengajuan[0]['tgl_penelitian'])));
$tgl2 = indo($tgl2);
$tanggalan = indo(date("Y-m-d"));

if ($pengajuan[0]['dekanat'] == 0 || $pengajuan[0]['dekanat'] == "" ) {
    $ttd =  "<br/>Dekan,";
    $kodedekan = "D";
  	$dekanat = $dekan['nama'];
  	$nip_dekanat = $dekan['nip'];
}else if ($pengajuan[0]['dekanat'] == 1) {
    $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Akademik dan Kelembagaan";
    $kodedekan = "D1";
    $dekanat = $wadek1['nama'];
    $nip_dekanat = $wadek1['nip'];
}else if ($pengajuan[0]['dekanat'] == 2) {
    $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Administrasi Umum, Perencanaan dan Keuangan";
    $kodedekan = "D2";
  	$dekanat = $wadek2['nama'];
  	$nip_dekanat = $wadek2['nip'];
}else if ($pengajuan[0]['dekanat'] == 3) {
    $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Kemahasiswaan dan Kerjasama";
    $kodedekan = "D3";
  	$dekanat = $wadek3['nama'];
  	$nip_dekanat = $wadek3['nip'];
}else if($pengajuan[0]['dekanat'] == 4){
    $ttd =  "<br/>a.n. Dekan,<br/>Kepala Bagian Tata Usaha";
    $kodedekan = "TU";
  	$dekanat = $tu['nama'];
  	$nip_dekanat = $tu['nip'];
}else {
    $ttd =  "<br/>Dekan,<br/>";
    $kodedekan = "D";
    $dekanat = $dekan['nama'];
    $nip_dekanat = $dekan['nip'];
}

function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);

  $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun;
  return $data;
}
 ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		 // window.print();

	});
</script>
<style type="text/css" media="print">
@page
    {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html
    {
        background-color: #FFFFFF;
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }

    body
    {
        margin: 5mm 15mm 10mm 15mm; /* margin you want for the content */
    }
</style>
<html>

	<head><title>Permohonan Izin Pra Penelitian atas nama <?php echo $nama;?></title></head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
  <font face="arial" size="11">
	<body>

<table width="650px;" style="margin:auto; margin-top:0px;">
	<tr>
		<td>
      <div id="header">
          <div id="gambar"><img style="height:10%; width:80%;" src="<?php echo base_url(); ?>assets/images/uin.png"/></div>
          <div id="tulisan_uin"><strong>KEMENTERIAN AGAMA REPUBLIK INDONESIA</strong><br/><span style="font-size:0.85em">
            <strong>UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA</strong></span>
        <br/><span style="font-size:0.85em"> <strong><?php echo $fakultas; ?></strong> </span>
        <br/>
          <p style=" margin-top: 10px;font-size:0.6em">
          Jl. A. Yani 117 Surabaya 60237 Telp. 031-8410298 Fax. 031-8413300 email : <u><?php echo $singkatan; ?>@uinsby.ac.id</u>
        </p></div>
        </div>

        <div id="garis"></div>
        <br/>
	<div id="satu">
<table style="font-size:0.9em">
  <tr>
    <td>&emsp;&emsp;Nomor</td>
    <td>:</td>
    <td>B -&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/Un.07/<?php echo $nomor; ?>/<?php echo $kodedekan; ?>/TL.00/<?php echo $nobulan."/".$tahun;?></td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>Surabaya, <?php echo $tanggalan ;?></td>
  </tr>
  <tr>
    <td>&emsp;&emsp;Lampiran</td>
    <td>:</td>
    <td> -</td>
  </tr>
  <tr>
    <td>&emsp;&emsp;Perihal</td>
    <td>:</td>
		<td>
		<?php if($fakultas1 == 'Fak. Sains dan Teknologi'){ ?>
			<strong> Surat Izin Survey </strong>
		<?php } else { ?>
			<strong> Permohonan Izin Pra Penelitian </strong>
		<?php } ?>
		</td>
  </tr>
  <tr><td></td></tr>
  <tr><td></td></tr>
  <tr><td></td></tr>
  <tr><td></td></tr>

</table>
  <table style="font-size:0.9em">

      <tr>
        <td>&emsp;&ensp;</td>
        <td><div id="">Yth,</div></td>
      </tr>
      <tr>
        <td></td>
        <td><div id=""><strong><?php print_r ($pengajuan[0]['ket_instansi']);  ?></strong>
          <br>
          <?php print_r ($pengajuan[0]['alamat_instansi']);  ?>
        </div></td>
      </tr>
      <tr>
        <td></td>
        <td>di</td>
      </tr>
      <tr>
        <td></td>
        <td>&emsp;<?php print_r ($pengajuan[0]['kab_penelitian']);  ?></td>
      </tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr>
        <td></td>
        <td>
          <br>
          <br>
          <div id=""><i><strong>Assalamu'alaikum Wr. Wb.</strong></i></div><br></td>
      </tr>
      <tr>
        <td></td>
        <td><div id="" style="text-align:justify;  line-height:25px;">
            &emsp;&emsp;
            Sehubungan dengan program peningkatan kompetensi dan ketrampilan mahasiswa <?php echo $fakultascilik2; ?>
            Universitas Islam Negeri Sunan Ampel Surabaya dalam bidang
            penelitian, bersama ini dekan menyampaikan bahwa mahasiswa dengan identitas sebagai berikut:</div>
        </td>
      </tr>
      <tr>
        <td></td>
        <td>
          <table style="line-height:18px;font-size:0.95em" >
    				<tr>
              <td>&emsp;&emsp;&ensp;</td>
    					<td>Nama</td>
    					<td></td>
    					<td>:</td>
    					<td> <strong><?php echo ucwords(strtolower($nama));?></strong> </td>
    				</tr>
    				<tr>
    					<td></td>
    					<td>NIM</td>
    					<td></td>
    					<td>:</td>
    					<td><?php echo $nim; ?> </td>
    				</tr>
    				<tr>
    					<td></td>
    					<td>Semester/Prodi</td>
    					<td></td>
    					<td>:</td>
    					<td><?php echo $semestermhs."/".$prodi;?></td>
    				</tr>
    			</table>
        </td>
      </tr>
      <tr>
        <td></td>
        <td>
          <div style="text-align:justify; line-height:25px;">
            bermaksud melakukan pra penelitian pada tanggal <?php print_r ($tgl);  ?> sampai tanggal <?php print_r ($tgl2);  ?>  dengan judul "<strong><?php print_r ($pengajuan[0]['judul_penelitian']);  ?> </strong>". Oleh karena
            itu, kami mohon kepada <?php print_r ($pengajuan[0]['ket_instansi']);  ?>  untuk berkenan memberikan izin, demi kelancaran penelitian yang bersangkutan.
      		</div>
        </td>
      </tr>
      <tr>
        <td></td>
        <td style="line-height:25px;text-align:justify;">
          &emsp;&emsp;
          Demikian permohonan izin ini, atas kerjasamanya kami sampaikan terimakasih.</td>
      </tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr>
        <td></td>
        <td>
          <p>
    				<i><strong>Wassalamu'alaikum Wr. Wb.</strong></i>
    			</p>
        </td>
      </tr>
      <tr>
        <td></td>
        <td></td>
      </tr>
  </table>
      <div id="id" style="font-size:o.9em">
      <strong>
  		<br/>
  		<br/>
        <?php echo $ttd; ?>
  		<br/>
  		<br/>
  		<br/>
  		<br/>
  		<br/>
      <?php echo $dekanat; ?><br/>
  		NIP. <?php echo $nip_dekanat; ?>
      </strong>

  		</div>
</div>
		</div>

	</td>
	</tr>
</table>



	</body>
</font>
</html>
