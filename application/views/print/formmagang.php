<?php
   $nim = $this->session->userdata('nim');
   $sex = $this->session->userdata('sex');
   $prodi = $_SESSION['prodi'];
   // $prodi = $this->session->userdata('prodi');
   // $nama = $this->session->userdata('nama');
   $nama = $_SESSION['nama'];
   $fakultas = $this->session->userdata('fakultas');

   function indo($date){
     $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
   	$tahun = substr($date, 0, 4);
   	$bulan = substr($date, 5, 2);
   	$tgl   = substr($date, 8, 2);
   	$jam  = substr($date, 11, 2);
   	$menit  = substr($date, 14, 2);
     // print_r ($BulanIndo[$bulan-1]);
     //  echo $jam."-".$menit;

     $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun." ".$jam.":".$menit;
     return $data;
   }

    ?>
	
	
<body style="background:url(<?php echo base_url();?>assets/images/background.png) center">
<div style="min-height:90%;">
<div class="satu" style="background-color:rgb(35, 140, 2);height:1vh;color:#fff;font-size:2vh;font-family: 'OpenSansLight', sans-serif;border-bottom:2px dotted #fff;"></div>
<div class="contentmenu" >
   <div class="container" >
      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="row">
            <div class="menu">

            </div>
         </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" style="border-bottom:3px solid #eee;border-top:3px solid #eee; padding-bottom:3vh;padding-left:6vh;padding-top:3vh;background-color:#fff;">
         <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
               <div class="row">
                  <div class="tempatNama">
                     <div class="nama1">NIM</div>
                     <div class="nama">: <?php echo $nim; ?></div>
                  </div>
                  <div class="tempatNama">
                     <div class="nama1">Nama </div>
                     <div class="nama">: <?php echo $nama; ?></div>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
               <div class="row">
                  <div class="tempatNama">
                     <div class="nama1">Prodi </div>
                     <div class="nama">: <?php echo $prodi; ?></div>
                  </div>
                  <div class="tempatNama">
                     <div class="nama1">Fakultas </div>
                     <div class="nama">: <?php echo $fakultas; ?></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="content" >
   <div class="container">
      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="row">
            <div class="tab-content" style="background-color:#fff;">
               <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/select/bootstrap-select.css">
               <script src="<?php echo base_url(); ?>assets/js/select/bootstrap-select.js" defer></script>
               <div role="tabpanel" class="tab-pane active" id="status">
                  <div class="panel-body" style="padding-bottom:4vw;">
                     <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">
					   <div class="modal-header">
                           <h4 style="font-size:3vh; text-align:center;" class="modal-title" id="myModalLabel">
							<?php if($kode_surat == 7){ ?>
						   Form Surat Ijin Kunjungan Edukasi
						<?php } else if($kode_surat == 8){ ?>
							Form Surat Kerjasama Praktek Kerja Lapangan 
						<?php } else if($kode_surat == 9){ ?>
							Form Surat Kerjasama Praktek Perbankan
						<?php } else if($kode_surat == 10){ ?>
							Form Surat Kerjasama Praktek Lembaga Keuangan 
						<?php } else if($kode_surat == 14){ ?>
							Form Surat Ijin Survey 
						<?php } else if($kode_surat == 16){ ?>
							Form Surat Ijin Bakesbangpol
						<?php } else if($kode_surat == 4){ ?>
							Form Surat Ijin Penelitian
						<?php } ?>
						   </h4>						
                        </div>
                        <form class="" action="<?php echo base_url(); ?>surat/add_surat_7" method="post">
                          <input type="hidden" id="kode_surat" name="kode_surat" value="<?php echo $kode_surat; ?>" />
                        <div class="modal-body" >
                          <div class="form-group">
                            <label for="nama" class="lb">Tujuan Instansi</label>
                            <input type="text" class="form-control " id="instansi" name="instansi" placeholder="Contoh : Kepala Badan Kesatuan Bangsa dan Politik (Bakesbangpol)" required  />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Alamat Instansi</label>
                            <input type="text" class="form-control " id="alamat" name="alamat" placeholder="Contoh : Jl. Putat Indah No.1 Putat Gede" required />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Kota Instansi</label>
                            <input type="text" class="form-control " id="kabupaten" name="kabupaten" placeholder="Contoh : Surabaya" required />
                          </div>
                          <!-- <div class="form-group">
                            <label for="nama" class="lb">Provinsi</label>
                            <select onchange="javascript:ganti_kab();" name="provinsi"  id="basic" class="selectpicker show-tick form-control" data-live-search="true">
                              <?php
                              foreach ($provinsi as $Hprovinsi) { ?>
                                <option value="<?php print_r ($Hprovinsi['id']); ?>"><?php print_r ($Hprovinsi['name']); ?></option>
                              <?php
                              }
                              ?>
                            </select>
                          </div> -->
                          <!-- <div class="form-group">
                            <label for="nama" id="sapi" class="lb">Kabupaten</label>
                            <select id="kabupaten" name="kabupaten" class="show-tick form-control" >
                                <option value="0">- Pilih Provinsi -</option>
                            </select>
                          </div> -->
                          <?php
                          if ($kode_surat == 7 || $kode_surat == 14) { ?>
                            <div class="form-group">
                              <label for="nama" class="lb">Mata Kuliah </label>
                              <input type="text" class="form-control " id="srt7_matkul" name="srt7_matkul" required />
                            </div>

                          <?php
                            } ?>
							
						    <?php if ($kode_surat == 14){ ?>
								
                           <div class="form-group">
                              <label for="nama" class="lb">Judul Penelitian/Judul Tugas</label>
                              <textarea type="text" class="form-control" id="srt3_judul" name="srt3_judul"  ></textarea>
                           </div>
						   <?php	
							} else if ($kode_surat == 4){
                          ?>
                           <div class="form-group">
                              <label for="nama" class="lb">Judul Penelitian/Judul Tugas</label>
                              <textarea type="text" class="form-control" id="srt3_judul" name="srt3_judul"  ></textarea>
                           </div>
						   <div class="form-group">
                              <label for="nama" class="lb">Nama Tempat Penelitian</label>
                              <input type="text" class="form-control" id="srt13_tujuan" name="srt13_tujuan" placeholder="Contoh : Badan Kesatuan Bangsa dan Politik (Bakesbangpol)"  ></input>
                           </div>
						   <div class="form-group">
                              <label for="nama" class="lb">Keperluan</label>
                              <input type="text" class="form-control" id="srt1_keperluan" name="srt1_keperluan" placeholder="Contoh : Penyusunan Penelitian Skripsi"  ></input>
                           </div>
						  <?php	
							} else if ($kode_surat == 16){
                          ?>
						   <div class="form-group">
                              <label for="nama" class="lb">Nama Tempat Penelitian</label>
                              <input type="text" class="form-control" id="srt13_tujuan" name="srt13_tujuan" placeholder="Contoh : PT. XYZ"  ></input>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Keperluan Izin</label>
                              <input type="text" class="form-control" id="srt16_data" name="srt16_data" placeholder="Contoh : Kuliah Kerja Lapangan (KKL) " ></input>
                           </div>
						  <?php
                          }
                            ?>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Mulai</label>
                              <input type="date" class="form-control " id="mulai_tgl" name="mulai_tgl" required />
                           </div>
						   
                           <?php
                           if ($kode_surat == 7) { ?>
                             <div class="form-group">
                                <label for="nama" class="lb">Mulai Pada Jam </label>
                                <input type="time" class="form-control " id="time" name="time_mulai" required />
                             </div>
                            <?php
                          }else { ?>
                            <div class="form-group">
                              <label for="nama" class="lb">Tanggal Akhir</label>
                              <input type="date" class="form-control " id="tgl_akhir" name="tgl_akhir" required />
                            </div>
                            <?php
                          }
                            ?>
                           <div class="form-group" <?php if($kode_surat == 7){echo 'style="display: none;"';} ?> >
                              <label for="nama" class="lb">Jenis Magang</label>
							    <br/>
								<div class="row">
									<div class="col-sm-3 col-md-3">
									<label class="containerRB">Individu
									  <input type="radio" checked="checked" onclick="javascript:add();" name="jenis_magang" id="magang_individu" value="1">
									  <span class="checkmark"></span>
									</label>
									</div>
									<div class="col-sm-3 col-md-3">
									<label class="containerRB">Kelompok
									  <input type="radio" onclick="javascript:add();" name="jenis_magang" id="magang_kelompok" value="2" <?php if($kode_surat == 7){echo 'checked="checked"';} ?>>
									  <span class="checkmark"></span>
									</label>
									</div>
								</div>
                           </div>
            						   <div id="jumlah_mhs_magang"></div>
            						   <div id="nama_mhs_magang"></div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-warning" onclick="gohome()">Kembali</button>
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                      </form>


							<!--/.table-responsive -->
                     </div>
                  </div>
				<!--<div class="col-xs-12 col-sm-12 col-md-12" style="border-top:1px solid #eee;width:60%; padding-bottom:3vh; padding-top:3vh;padding-left:6vh;">
					<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-3">
						<i style="color:#000;margin-left:2vh;">Keterangan :</i></span>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3">
						<span class="glyphicon glyphicon-stop" aria-hidden="true" style="color:orange"></span><i style="color:#000;margin-left:2vh;">Antri</i>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3">
						<span class="glyphicon glyphicon-stop" aria-hidden="true" style="color:blue"></span><i style="color:#000;margin-left:2vh;">Menunggu</i>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3">
						<span class="glyphicon glyphicon-stop" aria-hidden="true" style="color:green"></span><i style="color:#000;margin-left:2vh;">Bisa diambil<i>
					</div>
					</div>
				</div>-->
                </div>
               <div role="tabpanel" class="tab-pane" id="profil">
                  <div class="">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form class="" action="<?php echo base_url();?>prin/addSBea" method="post" enctype="multipart/form-data">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:1.5vw; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Rekomendasi</h4>
         </div>
         <div class="modal-body" id="modal_foto">
            <div class="form-group">
               <label for="nama" class="lb">File Pendukung</label>
               <input name="filepen" id="filepen" type="file" accept="application/pdf,application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" required />
               <a style="color:red">format harus pdf atau word<a>
            </div>
         </div>
         <div class="modal-body" id="modal_foto">
            <div class="form-group">
               <label for="nama" class="lb">Keterangan</label>
               <textarea type="text" class="form-control" id="ket_beasiswa" name="ket_beasiswa" required ></textarea>
            </div>
         </div>
         <div class="modal-footer" id="modal_foto" style="text-align:left">
            <div class="form-group">
               <label for="nama" class="lb">Contoh</label>
               <img src="<?php echo base_url();?>assets/images/1.jpg"/>
			   <div class="well well-sm">Lebih jelas silahkan klik link berikut <a href="<?php echo base_url();?>assets/images/ket2.jpg" target="_blank" >lihat contoh</a></div>
            </div>
         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Kirim</button>
         </div>
         </form>
      </div>
   </div>
</div>

<script type="text/javascript">

$(document).ready(function() {
  add();
});

function add(){
  if(document.getElementById("magang_kelompok").checked == true){
    document.getElementById("jumlah_mhs_magang").innerHTML = '<div class="form-group"><label for="nama" class="lb">Jumlah Mahasiswa (Kecuali Anda)</label><input type="number" class="form-control " id="jml_mahasiswa" name="jml_mahasiswa" onchange="" placeholder="Masukkan Jumlah Mahasiswa" required /><br/><button class="btn btn-success" type="button" onclick="javascript:jml_mhs();" >Masukkan NIM per Mahasiswa</button></div>';
  }else {
    document.getElementById("jumlah_mhs_magang").innerHTML = "";
    document.getElementById("nama_mhs_magang").innerHTML = "";
  }

}

function gohome()
{
window.location.href="<?php echo base_url(); ?>"
}
</script>
