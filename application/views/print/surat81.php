<?php
$fakultascilik1 = str_replace("Fak.","Fakultas",$fakultas);
$fakultascilik2 = $fakultascilik1;
$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);
//echo $fakultas;


$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password='ampelakademik!3'");
$result = pg_query($conn, "select periodesekarang from akademik.ms_setting");
//$result = pg_query($conn, "my_query4", array($nim));
while ($row = pg_fetch_assoc($result)){
	$data = $row['periodesekarang'] ;
}
$tahuns = substr($data,0,4);
$tahund = $tahuns+1;
$smt = substr($data,4,1);
if($smt == 1){
	$sem = "Gasal";
}else{
	$sem = "Genap";
}
$tgl = indo($pengajuan[0]['tgl_penelitian']);

$tanggalan = indo(date("Y-m-d"));

if ($pengajuan[0]['dekanat'] == 0 || $pengajuan[0]['dekanat'] == "" ) {
    $ttd =  "<br/>Dekan,";
    $kodedekan = "D";
  	$dekanat = $dekan['nama'];
  	$nip_dekanat = $dekan['nip'];
}else if ($pengajuan[0]['dekanat'] == 1) {
    $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Akademik dan Kelembagaan";
    $kodedekan = "D1";
    $dekanat = $wadek1['nama'];
    $nip_dekanat = $wadek1['nip'];
}else if ($pengajuan[0]['dekanat'] == 2) {
    $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Administrasi Umum,<br/> Perencanaan dan Keuangan";
    $kodedekan = "D2";
  	$dekanat = $wadek2['nama'];
  	$nip_dekanat = $wadek2['nip'];
}else if ($pengajuan[0]['dekanat'] == 3) {
    $ttd =  "<br/>a.n. Dekan,<br/>Wakil Dekan Bidang Kemahasiswaan <br/> dan Kerjasama";
    $kodedekan = "D3";
  	$dekanat = $wadek3['nama'];
  	$nip_dekanat = $wadek3['nip'];
}else if($pengajuan[0]['dekanat'] == 4){
    $ttd =  "<br/>a.n. Dekan,<br/>Kepala Bagian Tata Usaha";
    $kodedekan = "TU";
  	$dekanat = $tu['nama'];
  	$nip_dekanat = $tu['nip'];
}else {
    $ttd =  "<br/>Dekan,<br/>";
    $kodedekan = "D";
    $dekanat = $dekan['nama'];
    $nip_dekanat = $dekan['nip'];
}

function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);

  $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun;
  return $data;
}


$hariindo = array(
	'Sun' => 'Minggu',
	'Mon' => 'Senin',
	'Tue' => 'Selasa',
	'Wed' => 'Rabu',
	'Thu' => 'Kamis',
	'Fri' => 'Jumat',
	'Sat' => 'Sabtu'
);
 ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		 window.print();

	});
</script>
<style type="text/css" media="print">
@page
    {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html
    {
        background-color: #FFFFFF;
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }

    body
    {
        margin: 5mm 15mm 10mm 15mm; /* margin you want for the content */
    }
</style>
<html>

	<head><title>Surat Kerjasama Praktek Kerja Lapangan atas nama <?php echo $nama;?></title></head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
  <font face="arial" size="11">
	<body>

<table width="650px;" style=margin:auto;>
	<tr>
		<td>
      <div id="header">
          <div id="gambar"><img style="height:10%; width:80%;" src="<?php echo base_url(); ?>assets/images/uin.png"/></div>
          <div id="tulisan_uin"><strong>KEMENTERIAN AGAMA REPUBLIK INDONESIA</strong><br/><span style="font-size:0.85em">
            <strong>UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA</strong></span>
        <br/><span style="font-size:0.85em"> <strong><?php echo $fakultas; ?></strong> </span>
        <br/>
          <p style=" margin-top: 10px;font-size:0.6em">
          Jl. A. Yani 117 Surabaya 60237 Telp. 031-8410298 Fax. 031-8413300 email : <u><?php echo $singkatan; ?>@uinsby.ac.id</u>
        </p></div>
        </div>

        <div id="garis"></div>
        <br/>
	<div id="satu">
        <table style="font-size:0.9em;">
          <tr>
            <td>Nomor</td>
            <td>:</td>
            <td>B -&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/Un.07/<?php echo $nomor; ?>/<?php echo $kodedekan; ?>/PP.00.9/<?php echo $nobulan."/".$tahun;?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td><?php echo $tanggalan ;?></td>
          </tr>
          <tr>
            <td>Lampiran</td>
            <td>:</td>
            <td>-</td>
          </tr>
          <tr>
            <td style="vertical-align: text-top;">Perihal</td>
            <td style="vertical-align: text-top;">:</td>
            <td><strong>Permohonan Kerjasama Pelaksanaan <br> Praktek Kerja Lapangan</strong> </td>
          </tr>
          <tr><td></td></tr>
          <tr><td></td></tr>
          <tr><td></td></tr>
          <tr><td></td></tr>

        </table>
  <table style="font-size:0.9em;">

      <tr>
        <td></td>
        <td>
          <br>
          <div id="">Kepada Yth,</div></td>
      </tr>
      <tr>
        <td></td>
        <td><div id=""><strong><?php print_r ($pengajuan[0]['ket_instansi']);  ?></strong></div></td>
      </tr>
      <tr>
        <td></td>
        <td><div id=""><?php print_r ($pengajuan[0]['alamat_instansi']);  ?></div></td>
      </tr>
      <tr>
        <td></td>
        <td>di-</td>
      </tr>
      <tr>
        <td></td>
        <td>&emsp;<?php
        $kabu = str_replace("KABUPATEN","",$pengajuan[0]['kab_penelitian']);
        $kabu = str_replace("KOTA","",$kabu);
        $kabu  = ucwords(strtolower($kabu));
        echo $kabu;  ?></td>
      </tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr>
        <td></td>
        <td>
          <br>
          <br>
          <br>
          <div id=""> <strong><i>Assalamu'alaikum Wr. Wb.</i></strong> </div><br></td>
      </tr>
      <tr>
        <td></td>
        <td><div id="" style="text-align:justify;  line-height:20px;">&emsp;&emsp;&emsp;
            Sehubungan dengan program peningkatan kualitas ketrampilan
            mahasiswa Program Studi <?php echo $prodi; echo $fakultascilik; ?>
            Universitas Islam Negeri Sunan Ampel Surabaya, maka dengan ini kami
            bermaksud mengajukan permohonan untuk menjalin kerjasama dalam
            bentuk bimbingan dan tempat pelaksanaan praktik  di lembaga yang Bapak/Ibu
            pimpin. Adapun pelaksanaan kegiatan tersebut direncanakan pada tanggal
            <strong><?php echo indo($pengajuan[0]['tgl_penelitian']); ?> s.d
            <?php echo indo($pengajuan[0]['tgl_akhir']); ?></strong>
             dengan peserta An.
             <strong><?php echo ucwords(strtolower($nama))."/NIM: ".$nim; ?>.</strong>
            <br>
            &emsp;&emsp;&emsp;
            Besar harapan kami kerjasama ini terjalin secara baik, atas perkenan dan
            dukungan Bapak/Ibu disampaikan terima kasih.
            </div>
        </td>
      </tr>
      <tr style="line-height:40px;">
        <td></td>
        <td>
          <p>
    				<i><strong>Wassalamu'alaikum Wr. Wb.</strong></i>
    			</p>
        </td>
      </tr>
      <tr>
        <td></td>
        <td></td>
      </tr>
  </table>
		<div>
      <br>
      
					<?php if($ttd == '<br/>Dekan,'){?>
              		<div>
						<div id="id1" style="font-size:0.9em;">
						  <strong><?php echo $ttd; ?>
							<br/>
							<br/>
							<br/>
							<br/>
							<br/>
							<?php echo $dekanat; ?><br/>
							NIP. <?php echo $nip_dekanat; ?>
						</strong>

						</div>
					</div>
					<?php } else { ?>
              		<div>
						<div id="id1" style="font-size:0.9em;">
						  <strong><?php echo $ttd; ?>
							<br/>
							<br/>
							<br/>
							<br/>
							<br/>
							<?php echo $dekanat; ?><br/>
							NIP. <?php echo $nip_dekanat; ?>
						</strong>

						</div>
					</div>
					<?php } ?>
		</div>
</div>

	</td>
	</tr>
</table>

	</body>
</font>
</html>
