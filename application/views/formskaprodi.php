<?php
function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
	$jam  = substr($date, 11, 2);
	$menit  = substr($date, 14, 2);
  $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun." ".$jam.":".$menit;
  return $data;
}

 ?>

<style>
.container {
    width: 1570px;
}
</style>
 
<body style="background:url(<?php echo base_url();?>assets/images/background.png) center">
<div style="min-height:90%;">
<div class="satu" style="background-color:rgb(35, 140, 2);height:1vh;color:#fff;font-size:2vh;font-family: 'OpenSansLight', sans-serif;border-bottom:2px dotted #fff;"></div>
<div class="contentmenu">
<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="row">
			<div class="menu1">
				<ul class="nav tab1 nav-tabs " style="padding-left:25vw;" role="tablist">
              <li role="presentation" class="active" ><a href="#surat" aria-controls="surat" role="tab" data-toggle="tab"><img title="Permintaan Surat" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/1.png"/>Permintaan Surat</a></li>
			  <li role="presentation"><a href="#Riwayat" aria-controls="Riwayat" role="tab" data-toggle="tab"><img title="Riwayat" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/clock_history.png"/>Riwayat</a></li>
              <!--<li role="presentation"><a href="#Pengaturan" aria-controls="Pengaturan" role="tab" data-toggle="tab"><img title="Pengaturan" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/5.png"/>Pengaturan</a></li>-->
				<li role="presentation"><a href="<?php echo base_url();?>admin/logout"><img title="Status" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/logout.png"/>Keluar</a></li>
		  </ul>
			</div>
		</div>
	</div>
</div>
</div>
<div class="content">
<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="row">
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="surat">
						<div class="panel-body">

                  <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">

                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example" style="background-color:#fff">
                      <thead>
                          <tr>
                              <th style="text-align:center;">NIM</th>
                              <th style="text-align:center;">Nama Mahasiswa</th>
                              <th style="text-align:center;">Prodi</th>
                              <th style="text-align:center;">Surat Terkait</th>
                              <th style="text-align:center;">Tanggal Pesan</th>
                              <th style="text-align:center;">Preview</th>
                              <th style="text-align:center;">Validasi</th>
                              <th style="text-align:center;">Keterangan</th>
                              <th style="text-align:center;">Konfirmasi</th>
                          </tr>
                      </thead>
                      <tbody>
										<?php
										foreach ($antri as $Hantri ) { ?>

											<tr>
												<td style="text-align:center;"><?php echo $Hantri['nim'] ?></td>
												<td style="text-align:center;"><?php echo $Hantri['nama'] ?></td>
												<td style="text-align:center;"><?php echo $Hantri['prodi'] ?></td>
												<td style="text-align:center;"><?php echo $Hantri['srt_nama']; ?></td>
												<td style="text-align:center;"><?php
												$tgl = $Hantri['tgl_submit'];
												echo $tgl = indo($tgl);
												?></td>
												<td style="text-align:center;">
													<form class="" action="<?php echo base_url(); ?>prinaja" method="post">
														<input type="hidden" name="id_surat" value="<?php echo $Hantri['id']; ?>" >
														<input type="hidden" name="kode_surat" value="<?php echo $Hantri['kode_surat']; ?>" >
														<input type="hidden" name="nim" value="<?php echo $Hantri['nim']; ?>" >
														<button type="submit" class="btn btn-info btn-xs">Preview</button>
													</form>
												</td>
												<td style="text-align:center;">
													<?php
													if($Hantri['status'] == 88  ){ ?>
														<a class="btn btn-success btn-xs" type="button" onclick="javascript:valid(<?php echo $Hantri['id']?>)">Valid</a>
														<a class="btn btn-danger btn-xs" type="button" data-toggle="modal" href="#edit">Revisi</a>
													<?php
                          }else if($Hantri['status'] == 1  ){ ?>
														<a class="btn btn-default btn-success" disabled type="button">Sudah Valid</a>
													<?php
                          }
													?>
													<script>
														function valid(id) {
															$.ajax({
																type: "POST",
																url: "prinaja/valid",
																// dataType: 'json',
																data: {'id':id},
																success: function(data) {
																	location.reload();
																}
															});
														}
													</script>
												</td>
												<td style="text-align:center;">
													<?php
													if($Hantri['status'] == 88  ){ ?>
														<b style="color:red">Belum Valid</b>
														</br>
														<?php if($Hall['status'] == "96"){ ?>
														<b style="color:green">Sudah Di Revisi</b>
														<?php } ?>
													<?php
                          }else if($Hantri['status'] == 1  ){ ?>
														Sudah Valid
													<?php
                          }
													?>
												</td>
												
												<td style="text-align:center;"></td>
											</tr>

										<?php
										}
										 ?>


		                                </tbody>
		                            </table>

		                            <!-- /.table-responsive -->
		                            </div>
		                        </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="Riwayat">
						<div class="panel-body">

		                            <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">

		                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example3" style="background-color:#fff">
		                                <thead>
		                                    <tr>
		                                        <th style="text-align:center;">NIM</th>
		                                        <th style="text-align:center;">Nama Mahasiswa</th>
		                                        <th style="text-align:center;">Prodi</th>
		                                        <th style="text-align:center;">Jenis Surat</th>
		                                        <th style="text-align:center;">Tanggal Pesan</th>
												          <th style="text-align:center;">Preview</th>
		                                        <th style="text-align:center;">Status</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
																			<?php foreach ($all as $Hall){ ?>
																				<tr>
		                                        <td style="text-align:center;"><?php echo $Hall['nim']; ?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['nama'];?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['prodi'];?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['srt_nama'];?></td>
		                                        <td style="text-align:center;"><?php

																						$tgl = $Hall['tgl_submit'];
																						echo $tgl = indo($tgl);
																						?></td>
												<td style="text-align:center;">
												<form class="" action="<?php echo base_url(); ?>prinaja/previewSurat" method="post">
													<input type="hidden" name="id_surat" value="<?php echo $Hall['id']; ?>" >
													<input type="hidden" name="kode_surat" value="<?php echo $Hall['kode_surat']; ?>" >
													<input type="hidden" name="nim" value="<?php echo $Hall['nim']; ?>" >
													<button type="submit" class="btn btn-default">Preview</button>
												</form>
												</td>
												<td style="text-align:center;">
					                                  <?php
					                                     if($Hall['status'] == "1"){ ?>
							                                  <a class="btn-xs btn btn-warning">Belum diprint Fakultas</a>
							                                  <?php
							                                }else if($Hall['status'] == "2"){ ?>
							                                  <a class="btn-xs btn btn-info" >Menunggu</a>
							                                  <?php
							                                }else { ?>
							                                   <a class="btn-xs btn btn-success" >Bisa diambil</a>
							                                  <?php
							                                }
					                                     ?>
					                                  </td>
		                                    </tr>
																			<?php
																			}
																			?>

		                                </tbody>
		                            </table>

		                            <!-- /.table-responsive -->
		                            </div>
		                        </div>
					</div>
					<!--<div role="tabpanel" class="tab-pane" id="Pengaturan">
						ya
					</div>-->
				</div>
		</div>
	</div>
</div>
</div>
</div>


    <!-- /.modal Edit -->
	<div class="modal fade draggable-modal" id="edit" tabindex="-1" role="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" style="text-align:center;color:green">Konfirmasi Revisi</h4>
				<hr>
				<div class="form-group">
                    <div class="col-md-12">
                        <ul class="media-list">
                            <li class="media">
                                <a class="pull-left" href="javascript:;">
                                    <img class="todo-userpic" src="<?php echo base_url();?>/assets/images/user1.jpg" width="27px" height="27px"> </a>
                                <div class="media-body todo-comment">
                                    <p class="todo-comment-head">
										<strong><span class="todo-comment-username">Christina Aguilera</span></strong>
										<small><span class="todo-comment-date">17 Sep 2014 at 2:05pm</span></small>
                                    </p>
                                    <p class="todo-text-color"> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
                                    </p>
                                    <!-- Nested media object -->
                                    <div class="media">
                                        <a class="pull-left" href="javascript:;">
                                            <img class="todo-userpic" src="<?php echo base_url();?>/assets/images/user2.jpg" width="27px" height="27px"> </a>
                                        <div class="media-body">
                                            <p class="todo-comment-head">
										<strong><span class="todo-comment-username">Carles Puyol</span></strong>
										<small><span class="todo-comment-date">17 Sep 2014 at 2:05pm</span></small>
                                            </p>
                                        <p class="todo-text-color"> Thanks so much my dear! </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-md-12">
                        <ul class="media-list">
                            <li class="media">
                                <a class="pull-left" href="javascript:;">
                                    <img class="todo-userpic" src="<?php echo base_url();?>/assets/images/user2.jpg" width="27px" height="27px"> </a>
                                <div class="media-body">
                                    <textarea class="form-control todo-taskbody-taskdesc" rows="4" placeholder="Tolong Untuk Di Edit Bagian..."></textarea>
                                </div>
                            </li>
                        </ul>
                        <button type="button" class="pull-right btn btn-sm btn-circle btn-success"> &nbsp; Submit &nbsp; </button>
                    </div>
                </div>
            </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<!-- Kelompok Modal  -->
<!-- Modal Surat 1  -->
<div class="modal fade" id="surat_1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_1/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_1">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat kedua -->
<div class="modal fade" id="surat_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_2/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_2">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat ketiga -->
<div class="modal fade" id="surat_3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_3/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_3">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat keempat -->
<div class="modal fade" id="surat_4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_4/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_4">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat kelima -->
<div class="modal fade" id="surat_5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_5" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_5">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat keenam -->
<div class="modal fade" id="surat_6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_6" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_6">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat Tujuh -->
<div class="modal fade" id="surat_7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_7/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_7">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat Sebelas -->
<div class="modal fade" id="surat_11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_11/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_11">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat dua belas -->
<div class="modal fade" id="surat_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_12/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_12">
			
         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- modal surat tiga belas -->
<div class="modal fade" id="surat_13" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>surat/update_surat_13/1" method="post">
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Edit</h4>
         </div>
         <div class="modal-body" id="jenis_surat_13">

         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Simpan</button>
         </div>
         </form>
      </div>
   </div>
</div>
