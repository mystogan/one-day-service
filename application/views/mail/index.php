<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$pecah = substr($nim, 2, 1);

//Load Composer's autoloader
require 'autoload.php';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 1;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'fst@uinsby.ac.id';                 // SMTP username
    $mail->Password = '12345678';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to

    //Recipients
    $ma = 'by';
    $mail->setFrom('fst@uinsby.ac.id', 'Pengajuan ODS Oleh '.$nim);
    $mail->addAddress($email.'@uinsby.ac.id', $user);     // Add a recipient

    //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Persetujuan ODS';
    $mail->Body    = '
        Salam Bapak/Ibu Kaprodi FST,
        <br/>
        Diberitahukan bahwa Mahasiswa dengan :
        <br/>
            <b>Nama : '.$user.'</b>
        <br/>
            <b>NIM : '.$nim.'</b>
        <br/>
        Telah mengajukan <b>One Day Service</b>.
        <br/>
        Di mohon segera untuk melakukan validasi surat mahasiswa tersebut. 
        <br/>
        http://ctrl.uinsby.ac.id 
        <br/>
        Terimakasih
    ';
    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}

?>