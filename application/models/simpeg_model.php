<?php
class Simpeg_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->db2 = $this->load->database('simpeg', TRUE);
	}

	function login($nip,$password,$unit){
		$this->db2->select('tbpegawai.id,nip,nama,id_unittugastambahan,id_unitkerja');
		$this->db2->from("tbpegawai ");
		$this->db2->join('tbsimpeguser', 'nip = username ');
		$this->db2->where('username =', $nip);
		$this->db2->where('password =', $password);
		$this->db2->where("(id_unitkerja = $unit OR id_unittugastambahan = $unit)");
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}

	function getpangkat($nip = 0){
		$this->db2->select('b.*');
		$this->db2->from('tbpegawai a');
		$this->db2->join('m_pangkat b', 'a.kode_pangkat = b.id ');
		$this->db2->where('a.nip =', $nip);
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}


}
?>
