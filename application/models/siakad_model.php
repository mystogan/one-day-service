<?php
class Siakad_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->db3 = $this->load->database('siakad', TRUE);
	}

	function getprofil($nim = 0){
		$this->db3->select('a.nim,a.nama,a.semestermhs,a.kodeunit , a.alamat,a.tmplahir , a.tgllahir , a.ipk , b.namaunit ,b.kodeunitparent,
												c.namakota, d.namapropinsi, a.noijasah');
		$this->db3->from('akademik.ms_mahasiswa a');
		$this->db3->join('gate.ms_unit b', 'a.kodeunit = b.kodeunit ');
		$this->db3->join('akademik.ms_kota c', 'c.kodekota = a.kodekota');
		$this->db3->join('akademik.ms_propinsi d', 'c.kodepropinsi = d.kodepropinsi');
		$this->db3->where('a.nim =', $nim);
		$query = $this->db3->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getsetting($nim = 0){
		$this->db3->select('periodesekarang');
		$this->db3->from('akademik.ms_setting');
		$query = $this->db3->get();
		$data = $query->result_array();
		return $data[0];
	}

	function update_data_siakad($nim,$data){
		$this->db3->where('nim', $nim);
		$this->db3->update('akademik.ms_mahasiswa', $data);
	}


}
?>
