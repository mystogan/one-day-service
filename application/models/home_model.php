<?php
class Home_model extends CI_Model {
	function __construct(){
		parent::__construct();
	// $this->db3 = $this->load->database('siakad', TRUE);
	}
	function login($username,$password){
		$this->db->select('*');
		$this->db->from("user");
		$this->db->where('username =', $username);
		$this->db->where('password =', $password);
		$data = $this->db->count_all_results(); 
		return $data;
	}
	function login_tembus($username,$password){
		$this->simpeg = $this->load->database('simpeg', TRUE);
		$this->simpeg->select('*');
		$this->simpeg->from("tbsimpeguser");
		$this->simpeg->where('username =', $username);
		$this->simpeg->where('password =', $password);
		$data = $this->simpeg->count_all_results();
		return $data;
	}
	function cekfak($username){
		$user = $this->session->userdata('username');
		$this->db->select('*');
		$this->db->from("login");
		$this->db->where('nip =', $username);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}


	function cekantri($nim,$kode_surat){
		$this->db->select('*');
		$this->db->from("pengajuan");
		$this->db->where('nim =', $nim);
		$this->db->where('kode_surat =', $kode_surat);
		$this->db->where('status =', '1');
		$data = $this->db->count_all_results();
		return $data;
	}

	function cekantri2($nim,$kode_surat){
		$this->db->select('*');
		$this->db->from("pengajuan");
		$this->db->where('nim =', $nim);
		$this->db->where('kode_surat =', $kode_surat);
		$this->db->where("tgl_submit like  '%".date("Y-m-d")."%'");
		$this->db->where('( status = 1 or status = 2 )');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function cekcuti($nim,$kode_surat){
		$this->db->select('*');
		$this->db->from("pengajuan");
		$this->db->where('nim =', $nim);
		$this->db->where('kode_surat = ', 5);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getNomor($kode = 0){
		$this->db->select('*');
		$this->db->from("kodeunit");
		$this->db->where('kodeunit =', "$kode");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function get_surat(){
		$this->db->select('*');
		$this->db->from("m_surat");
		$this->db->where('srt_status = 1 ');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getkab($id_prov = 0){
		$this->db->select('*');
		$this->db->from("kabupaten");
		$this->db->where('id_prov = ', "$id_prov");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getProv($kode = 0){
		$this->db->select('*');
		$this->db->from("provinsi");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getSurat($id){
		$this->db->select('a.*,b.nama as kabupaten,b.id as id_kab,c.id as id_prov ,c.name as provinsi');
		$this->db->select("DATE_FORMAT(a.tgl_penelitian, '%Y-%m-%d') AS tgl_penelitian",false);
		// $this->db->select("DATE_FORMAT(a.tgl_penelitian, '%Y-%m-%d') AS tgl_mulai",false);
		$this->db->select("DATE_FORMAT(a.tgl_penelitian, '%H:%i') AS jam_mulai",false);
		$this->db->from("pengajuan a ");
		$this->db->join('kabupaten b', 'a.kab = b.id','left');
		$this->db->join('provinsi c', 'b.id_prov = c.id','left');
		$this->db->where('a.id =', $id);
		$this->db->where('a.status !=', '0');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getSuratRekom($nim,$id,$id_surat){
		$this->db->select('a.*');
		$this->db->from("pengajuan a");
		$this->db->where('a.nim =', $nim);
		$this->db->where('a.id =', $id);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getantri($nim){
		$this->db->select('a.*,b.srt_nama');
		$this->db->from("pengajuan a");
		$this->db->join('m_surat b', 'a.kode_surat = b.srt_id','left');
		$this->db->where('a.nim = ', $nim);
		$this->db->where('a.status != ', '0');
		$this->db->order_by("a.id", "desc");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getMhs_magang($id = 0){
		$this->db->select('*');
		$this->db->from("mhs_pengajuan");
		$this->db->where('id_pengajuan =', $id);
		$this->db->order_by("nama_mhs", "asc");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getAjuan($id){
		$this->db->select('a.*,b.nama as kabupaten ,c.name as provinsi');
		$this->db->select("DATE_FORMAT(a.tgl_penelitian, '%Y-%m-%d') AS tgl_penelitian",false);
		// $this->db->select("DATE_FORMAT(a.tgl_penelitian, '%Y-%m-%d') AS tgl_mulai",false);
		$this->db->select("DATE_FORMAT(a.tgl_penelitian, '%H:%i') AS jam_mulai",false);
		$this->db->from("pengajuan a ");
		$this->db->join('kabupaten b', 'a.kab = b.id','left');
		$this->db->join('provinsi c', 'b.id_prov = c.id','left');
		$this->db->where('a.id =', $id);
		$this->db->where('a.status !=', '0');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getmhsajuan($id){
		$this->db->select('b.*');
		$this->db->from("pengajuan a ");
		$this->db->join('mhs_pengajuan b', 'a.id = b.id_pengajuan','left');
		$this->db->where('a.id =', $id);
		$this->db->where('a.status !=', '0');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function login_simpeg($user){
		 $sql = "SELECT COUNT(*) as par,kode FROM login WHERE TRIM(nip) = '".$user."';";
		 $query =  $this->db->query($sql);
		 $data = $query->result();
		 return $data;
	
  
	}
	function getPrintKhusus($status = 0, $dewo){
		// $kodefak = $this->session->userdata('SINF');
		$kodefak = $dewo;
		$pro = ['BIO', 'MTK', 'ART', 'IK', 'TL', 'SINF','BKI','PMI','MD','KOM','KPI','BSA','SI','SIND','SKI'];
		$pecah = ['1', '2', '3', '4', '5', '6','3','2','4','5','1','1','3','4','2'];
		$param = ['H', 'H', 'H', 'H', 'H', 'H','B','B','B','B','B','A','A','A','A'];


		$cari = array_search($kodefak, $pro);
		$fakkod = $param[$cari];
		$kodkod = $pecah[$cari];

		
		if($fakkod == 'H'){

			$this->db->select('a.*,b.srt_nama');
			$this->db->from("pengajuan a");
			$this->db->join('m_surat b', 'a.kode_surat = b.srt_id','left');
			if ($status != 0) {
				$this->db->where('a.status in (88)');
			}
			$this->db->where('a.status !=', '0');
			$this->db->where('a.nama !=', '0');
			$this->db->where('a.prodi !=', '0');
			$this->db->where('SUBSTRING(a.nim, 3, 1) =', $pecah[$cari]);
			$this->db->where("a.nim like '%H%'");
			$this->db->order_by("a.tgl_submit", "desc");
			$query = $this->db->get();
			$data = $query->result_array();
			return $data;

		}else if($fakkod == 'B'){
			if($kodkod == 5){
				
				$query = $this->db->query("SELECT `a`.*, `b`.`srt_nama` FROM (`pengajuan` a) LEFT JOIN `m_surat` b ON `a`.`kode_surat` = `b`.`srt_id` 
				WHERE `a`.`status` in (88) AND `a`.`status` != '0' AND `a`.`nama` != '0' AND `a`.`prodi` != '0' AND (SUBSTRING(a.nim, 3, 1) = '5'
				 or SUBSTRING(a.nim, 3, 1) ='6') AND `a`.`nim` like '%B%' ORDER BY `a`.`tgl_submit` desc");
				$result = $query->result_array();
				return $result;
			}else{
				
				$this->db->select('a.*,b.srt_nama');
				$this->db->from("pengajuan a");
				$this->db->join('m_surat b', 'a.kode_surat = b.srt_id','left');
				if ($status != 0) {
					$this->db->where('a.status in (88)');
				}
				$this->db->where('a.status !=', '0');
				$this->db->where('a.nama !=', '0');
				$this->db->where('a.prodi !=', '0');
				$this->db->where('SUBSTRING(a.nim, 3, 1) =', $pecah[$cari]);
				$this->db->where("a.nim like '%B%'");
				$this->db->order_by("a.tgl_submit", "desc");
				$query = $this->db->get();
				$data = $query->result_array();
				return $data;
			
			}
		} else if ($fakkod == 'A') {
			$this->db->select('a.*,b.srt_nama');
			$this->db->from("pengajuan a");
			$this->db->join('m_surat b', 'a.kode_surat = b.srt_id','left');
			if ($status != 0) {
				$this->db->where('a.status in (88)');
			}
			$this->db->where('a.status !=', '0');
			$this->db->where('a.nama !=', '0');
			$this->db->where('a.prodi !=', '0');
			$this->db->where('SUBSTRING(a.nim, 3, 1) =', $pecah[$cari]);
			$this->db->where("a.nim like '%A%'");
			$this->db->order_by("a.tgl_submit", "desc");
			$query = $this->db->get();
			$data = $query->result_array();
			return $data;
		}


		
		/*
		// return $pecah[5];
		//$kode= "H";
		$this->db->select('a.*,b.srt_nama');
		$this->db->from("pengajuan a");
		$this->db->join('m_surat b', 'a.kode_surat = b.srt_id','left');
		if ($status != 0) {
			$this->db->where('a.status in (88)');
		}
		$this->db->where('a.status !=', '0');
		$this->db->where('a.nama !=', '0');
		$this->db->where('a.prodi !=', '0');
		$this->db->where('SUBSTRING(a.nim, 3, 1) =', $pecah[$cari]);
		$this->db->where("a.nim like '%H%'");
		$this->db->order_by("a.tgl_submit", "desc");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	// }
	*/
	}
	function getPrintKhusus1($status = 0, $dewo){
		// $kodefak = $this->session->userdata('SINF');
		$kodefak = $dewo;
		// $pro = ['BIO', 'MTK', 'ART', 'IK', 'TL', 'SINF'];
		// $pecah = ['1', '2', '3', '4', '5', '6'];
		$pro = ['BIO', 'MTK', 'ART', 'IK', 'TL', 'SINF','BKI','PMI','MD','KOM','KPI','BSA','SI','SIND','SKI'];
		$pecah = ['1', '2', '3', '4', '5', '6','3','2','4','5','1','1','3','4','2'];
		$param = ['H', 'H', 'H', 'H', 'H', 'H','B','B','B','B','B','A','A','A','A'];

		$cari = array_search($kodefak, $pro);
		$fakkod = $param[$cari];
		// return $pecah[5];
		//$kode= "H";
		if($fakkod == 'H'){
			$this->db->select('a.*,b.srt_nama');
			$this->db->from("pengajuan a");
			$this->db->join('m_surat b', 'a.kode_surat = b.srt_id','left');
			if ($status != 0) {
				$this->db->where('a.status in (1,2,3)');
			}
			$this->db->where('a.status !=', '0');
			$this->db->where('a.nama !=', '0');
			$this->db->where('a.prodi !=', '0');
			$this->db->where('SUBSTRING(a.nim, 3, 1) =', $pecah[$cari]);
			$this->db->where("a.nim like '%H%'");
			$this->db->order_by("a.tgl_submit", "desc");
			$query = $this->db->get();
			$data = $query->result_array();
			return $data;
		} else if($fakkod == 'A'){
			$this->db->select('a.*,b.srt_nama');
			$this->db->from("pengajuan a");
			$this->db->join('m_surat b', 'a.kode_surat = b.srt_id','left');
			if ($status != 0) {
				$this->db->where('a.status in (1,2,3)');
			}
			$this->db->where('a.status !=', '0');
			$this->db->where('a.nama !=', '0');
			$this->db->where('a.prodi !=', '0');
			$this->db->where('SUBSTRING(a.nim, 3, 1) =', $pecah[$cari]);
			$this->db->where("a.nim like '%A%'");
			$this->db->order_by("a.tgl_submit", "desc");
			$query = $this->db->get();
			$data = $query->result_array();
			return $data;
		}
		
	// }
	}

	function getPrint($status = 0){
		$kodefak = $this->session->userdata('fak');
		//$kode= "G";
		$this->db->select('a.*,b.srt_nama');
		$this->db->from("pengajuan a");
		$this->db->join('m_surat b', 'a.kode_surat = b.srt_id','left');
		if ($status != 0) {
			$this->db->where('a.status =', $status);
		}
		$this->db->where('a.status !=', '0');
		$this->db->where('a.nama !=', '0');
		$this->db->where('a.prodi !=', '0');
		$this->db->like('a.nim', $kodefak);
		$this->db->order_by("a.tgl_submit", "desc");
		$this->db->limit(1000);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getPrint1($status = 0){
		$kodefak = $this->session->userdata('fak');
		//$kode= "G";
		$this->db->select('a.*,b.srt_nama');
		$this->db->from("pengajuan a");
		$this->db->join('m_surat b', 'a.kode_surat = b.srt_id','left');
		if ($status != 0) {
			$this->db->where('a.status =', $status);
		}
		$this->db->where('a.status !=', '0');
		$this->db->where('a.nama !=', '0');
		$this->db->where('a.prodi !=', '0');
		$this->db->order_by("a.tgl_submit", "desc");
		$this->db->limit(1000);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function savePengajuan($data){
		$this->db->insert('pengajuan', $data);
	}
	function saveMhsPengajuan($data){
		$this->db->insert('mhs_pengajuan', $data);
	}
	function savePengajuanMagang($data){
		$this->db->insert('pengajuan', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	function hapus($id,$data){
		$this->db->where('id', $id);
		$this->db->update('pengajuan', $data);
	}

	function getkaprod($nim) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("select ketua from gate.ms_unit a
		join akademik.ms_mahasiswa b on a.kodeunit = b.kodeunit
		where nim = '$nim'");
		$result = $query->result_array();
		return $result['0']['ketua'];
	}

	function getsekprod($nim) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("select sekretaris from gate.ms_unit a
		join akademik.ms_mahasiswa b on a.kodeunit = b.kodeunit
		where nim = '$nim'");
		$result = $query->result_array();
		return $result['0']['sekretaris'];
	}
	
	function getEmail($nip) {
		$this->simpeg = $this->load->database('simpeg', TRUE);
		$query = $this->simpeg->query("SELECT email FROM login_sso WHERE nip = '$nip'");
		$result = $query->result_array();
		return $result['0']['email'];
	}

	function getTU($prodi='z') {
		$this->simpeg = $this->load->database('simpeg', TRUE);
		$query = $this->simpeg->query("SELECT a.nip, a.nama, c.nama_unit, b.nama_jastruk FROM tbpegawai a
		JOIN m_jastruk b ON a.id_tugastambahan = b.id OR a.id_jastruk = b.id
		JOIN m_unit c ON b.kode_unit = c.id
		WHERE b.nama_jastruk LIKE '%Tata Usaha%' AND c.nama_unit LIKE '%$prodi%'");
		$result = $query->result_array();
		return $result;
	}





	function getDataMhsA($nim){
		$nim = "H76216055";
		$strSql = "SELECT a.periode, a.kodeunit, a.kodemk, 
					a.kelasmk, c.namamk, d.nipdosen,(select userdesc from gate.sc_user where 
					username = d.nipdosen limit 1) as dosen, (select count(*) from 
					gate.sc_nilaidosen where kodemk = a.kodemk and nipdosen = d.nipdosen and 
					kodeunit = a.kodeunit and periode = a.periode and nim = a.nim) as cek 
					FROM akademik.ak_krs a, akademik.ms_setting b, akademik.ak_matakuliah c, 
					akademik.ak_mengajar d where a.nim = '$nim' and a.periode = 
					b.periodesekarang and a.kodemk = c.kodemk and a.kodemk = d.kodemk and 
					c.thnkurikulum = a.thnkurikulum and a.kelasmk = d.kelasmk;";
		$query = $this->db3->query($strSql);
		$data = $query->result_array();
		
		return json_encode($data);
	}

}
?>
