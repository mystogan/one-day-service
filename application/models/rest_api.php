<?php
class Rest_api extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function getFak($kodefak) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("select * from gate.ms_unit where kodeunit = '$kodefak'");
		return $query->result_array();
	}

	function getAllMatkul($nim) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("SELECT a.periode, a.kodeunit, a.kodemk, a.kelasmk, c.namamk, d.nipdosen,
		(select userdesc from gate.sc_user where username = d.nipdosen limit 1) as dosen, 
		(select count(*) from gate.sc_nilaidosen 
		where kodemk = a.kodemk and nipdosen = d.nipdosen and kodeunit = a.kodeunit and periode = a.periode and nim = a.nim) as cek 
		FROM akademik.ak_krs a, akademik.ms_setting b, akademik.ak_matakuliah c, akademik.ak_mengajar d where a.nim = '$nim' 
		and a.periode = b.periodesekarang and a.kodemk = c.kodemk and a.kodemk = d.kodemk and c.thnkurikulum = a.thnkurikulum and a.kelasmk = d.kelasmk");
		return $query->result_array();
	}

	function getAllMatkulCek($nim) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("select count(*) from( SELECT a.periode, a.kodeunit, a.kodemk, a.kelasmk, c.namamk, d.nipdosen, 
		(select userdesc from gate.sc_user where username = d.nipdosen limit 1) as dosen, 
		(select count(*) from gate.sc_nilaidosen where kodemk = a.kodemk and nipdosen = d.nipdosen and kodeunit = a.kodeunit and periode = a.periode and nim = a.nim) as cek 
		FROM akademik.ak_krs a, akademik.ms_setting b, akademik.ak_matakuliah c, akademik.ak_mengajar d 
		where a.nim = '$nim' and a.periode = b.periodesekarang and a.kodemk = c.kodemk and a.kodemk = d.kodemk 
		and c.thnkurikulum = a.thnkurikulum and a.kelasmk = d.kelasmk) a where a.cek = '0'");
		return $query->result_array();
	}

	function getAllSet($kodefak, $periode) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("SELECT * FROM gate.sc_settingtgl where kode_fak = '$kodefak' and periode = '$periode'");
		return $query->result_array();
	}

	function addNilai($kodemk, $namamk, $nipdosen, $dosen, $kodeunit, $periode, $jawab, $jawabtext, $nim) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("insert into gate.sc_nilaidosen (kodemk, namamk, nipdosen, dosen, kodeunit, periode, jawab, jawabtext, nim) 
		values ('$kodemk','$namamk','$nipdosen','$dosen','$kodeunit','$periode','$jawab','$jawabtext', '$nim')");
		return $query;
	}

	function getAllKodeunit($kodefak) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("select kodeunit, namaunit from gate.ms_unit where kodeunitparent = '$kodefak'");
		return $query->result_array();
	}

	function getAllDosen($kodeunit, $periode) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("select nipdosen, dosen, jawab, jawabtext from gate.sc_nilaidosen where periode = '$periode' and kodeunit = '$kodeunit'");
		return $query->result_array();
	}

	function getAllFak($kodefak, $periode) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("select nipdosen, dosen, kodeunit, jawab, jawabtext from gate.sc_nilaidosen where periode = '$periode' and kodeunit 
		in (select kodeunit from gate.ms_unit where kodeunitparent = '$kodefak')");	
		return $query->result_array();
	}

	function getAllNilai($nip, $kodeunit, $periode) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("select nipdosen, dosen, jawab, jawabtext from gate.sc_nilaidosen where nipdosen = '$nip' and kodeunit = '$kodeunit' and periode = '$periode'");	
		return $query->result_array();
	}

	function getAllSetting($kodefak, $periode) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("SELECT * FROM gate.sc_settingtgl where kode_fak = '$kodefak' and periode = '$periode'");	
		return $query->result_array();
	}

	function addSetting($periode, $kodefak, $sdate, $edate) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("insert into gate.sc_settingtgl (kode_fak, periode, mulai, selesai) values ('$kodefak','$periode','$sdate','$edate')");
		return $query;
	}

	function editSetting($id, $periode, $sdate, $edate) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("update gate.sc_settingtgl set periode = '$periode', mulai = '$sdate', selesai = '$edate' where id = '$id'");
		return $query;
	}

	function delSetting($id) {
		$this->siakad = $this->load->database('siakad', TRUE);
		$query = $this->siakad->query("delete from gate.sc_settingtgl where id = '$id'");
		return $query;
	}
}
?>